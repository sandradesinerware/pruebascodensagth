--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body VERSIONADOR_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "SNW_CARGOS"."VERSIONADOR_PCK" AS
--------------------------------------------------------------------------------------------------------
  FUNCTION CONSULTAR_CARGO_ANTERIOR(i_cargo NUMBER) RETURN NUMBER IS
    i_cargo_anterior number; 

    BEGIN
      SELECT  ID_CARGO_PADRE INTO i_cargo_anterior FROM CARGO WHERE ID = i_cargo;
      return i_cargo_anterior;
  END CONSULTAR_CARGO_ANTERIOR;

--------------------------------------------------------------------------------------------------------
  PROCEDURE ACTUALIZAR_VERSION(I_CARGO NUMBER) 
    IS
    l_version_cargo NUMBER;
    l_id_cargo_anterior number;
  BEGIN
    SELECT NVL(max(version_cargo), 0)+1 INTO l_version_cargo FROM cargo 
    WHERE ID_CARGO_PADRE = (SELECT ID_CARGO_PADRE FROM cargo WHERE ID = i_cargo);
    UPDATE cargo
    SET version_cargo = l_version_cargo
    WHERE id = I_CARGO;
    if l_version_cargo = 1 then
        UPDATE CARGO SET FECHA_INICIO = trunc(sysdate),FECHA_FIN = null WHERE ID = i_cargo;
    else
        select id into l_id_cargo_anterior from cargo 
        where ID_CARGO_PADRE = (SELECT ID_CARGO_PADRE FROM cargo WHERE ID = i_cargo) and
        version_cargo = l_version_cargo-1;
        UPDATE CARGO SET FECHA_INICIO = trunc(sysdate) WHERE ID = i_cargo;
        UPDATE CARGO SET FECHA_FIN = trunc(sysdate-1) WHERE ID = l_id_cargo_anterior;
    end if;
  END ACTUALIZAR_VERSION;

--------------------------------------------------------------------------------------------------------

  PROCEDURE ACTUALIZAR_VIGENCIA(I_CARGO NUMBER) IS
  i_cargo_padre number;

  BEGIN
    --CONSULTAR SI HA SIDO APROBADO
    SELECT ID_CARGO_PADRE INTO  i_cargo_padre FROM CARGO WHERE ID = I_CARGO;
    UPDATE CARGO
    SET vigencia = 'N'
    WHERE ID_CARGO_PADRE = i_cargo_padre AND vigencia = 'S';

    UPDATE CARGO
    SET vigencia = 'S'
    WHERE ID = I_CARGO; 
    
    -- Se notifica al creador de la D.C (Sea gestor o BP) de la aceptación de esta D.C
    CODENSA_GTH.NOTIFICACION_PCK.NOTIFICAR_ACEPTACION_DC(I_CARGO);

    -- Se respaldan datos permanentes del cargo vigente
    REDUNDAR_UBICACION_JERARQUICA(I_CARGO);
    REDUNDAR_CARGOS_RESPONSABLES(I_CARGO);
    REDUNDAR_REQUISITOS(I_CARGO);
    ADMIN_UNIDAD_ORGANIZATIVA.CARGOS_REPORTAN(I_CARGO);
    ADMIN_UNIDAD_ORGANIZATIVA.GUARDAR_CARGO_SUPERIOR(I_CARGO);

  END ACTUALIZAR_VIGENCIA;
 --------------------------------------------------------------------------------------------------------- 

  FUNCTION CARGO_VIGENTE(I_ID_CARGO NUMBER) RETURN CHAR IS
    l_aux char;
  BEGIN
    select VIGENCIA into l_aux from cargo where id = I_ID_CARGO;
    return l_aux;
    exception when others then
        return 'N';
  END CARGO_VIGENTE;

--------------------------------------------------------------------------------------------------------- 

/* Grupos de SNW_AUTH:
    8 -> POC
    9 -> BP
    13 -> Administradores de aplicación
    14 -> Gestores
*/
    FUNCTION PERMITE_ACTUALIZAR(I_USER VARCHAR2,I_ID_CARGO NUMBER) RETURN VARCHAR2 IS       
        l_id_unidad_usuario number;
        l_id_unidad_cargo number;
        l_id_cargo_padre number;
        l_tipo_ubicacion number;
        l_id_trabajador number;
        l_conteo number;
        
    BEGIN
        
        -- No tiene permisos para actualizar por su grupo de usuario.
        
        /*  
            Si NO es: BP o POC o gestor o administrador de aplicación
            No permite actualizar        
        */
        if not (snw_auth.autorizacion.verificar_grupo(8,sysdate,I_USER) 
                   or snw_auth.autorizacion.verificar_grupo(9,sysdate,I_USER) 
                   or snw_auth.autorizacion.verificar_grupo(13,sysdate,I_USER) 
                   or snw_auth.autorizacion.verificar_grupo(14,sysdate,I_USER)) then
            return 'N';
        end if;
        
        
        -- El cargo tiene una actualización en proceso
        select id_cargo_padre into l_id_cargo_padre from cargo where id = I_ID_CARGO;
        
        select count(*) into l_conteo from cargo where id_cargo_padre = l_id_cargo_padre and 
        vigencia is null and estado <> snw_constantes.constante_tipo('CARGO_ANULADO');
        
        if l_conteo > 0 then
            return 'P';
        end if;
        
        -- Si es POC puede actualizar la DC
        if snw_auth.autorizacion.verificar_grupo(8,sysdate,I_USER) then
            return 'S';
        end if;
        
        -- Si es BP
        if snw_auth.autorizacion.verificar_grupo(9,sysdate,I_USER) then            
        
            -- Y la DC es Transversal, puede actualizar la DC
            select tipo_ubicacion into l_tipo_ubicacion from cargo where id = I_ID_CARGO;
            if SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') = l_tipo_ubicacion then    
                return 'S';
                
            -- Y la DC es Específica, y la UO del cargo es igual o inferior jerárquicamente a la o las UOs en que el usuario es BP.
            else                
                            
                select id into l_id_trabajador from TRABAJADOR WHERE NUMERO_IDENTIFICACION = (select NUMERO_IDENTIFICACION from usuario where USER_NAME = I_USER);
                select id_unidad_organizativa into l_id_unidad_cargo from cargo where id = I_ID_CARGO;
                
                for unidad_bp in (
                    select ID_UNIDAD_ORGANIZATIVA from BUSINESS_PARTNER where id_trabajador = l_id_trabajador)
                loop
                    
                    if ADMIN_UNIDAD_ORGANIZATIVA.PERTENECE_UNIDAD(unidad_bp.id_unidad_organizativa ,l_id_unidad_cargo) = 'S' then
                        return 'S';
                    end if;
                
                end loop;
                                
            end if; 
            
            -- return 'N'; UN BP PUEDE SER GESTOR ????
            
        end if;
  
        /*
            Si no es BP ni POC, entonces es Gestor:
            
                Primero se obtiene la UO del cargo: A
                Luego se obtiene la UO a la que está asociado el Usuario: B   
        */
        select id_unidad_organizativa into l_id_unidad_cargo from cargo where id = I_ID_CARGO;
        l_id_unidad_usuario := admin_unidad_organizativa.usuario_pertenece_unidad(I_USER);
        
        return ADMIN_UNIDAD_ORGANIZATIVA.PERTENECE_UNIDAD(l_id_unidad_usuario,l_id_unidad_cargo);        
        
    END PERMITE_ACTUALIZAR;

--------------------------------------------------------------------------------------------------------- 

    FUNCTION PERMITE_EDITAR_POC(I_USER VARCHAR2,I_ID_CARGO NUMBER) RETURN VARCHAR2 IS
        
        l_id_unidad_usuario number;
        l_id_unidad_cargo number;
        l_id_cargo_padre number;
        l_tipo_ubicacion number;
        l_id_trabajador number;
        l_conteo number;
        
    BEGIN
        
        -- No tiene permisos para actualizar por su grupo de usuario.
        
        /*  
            Si NO es: BP o POC o gestor o administrador de aplicación
            No permite actualizar        
        */
        if not (snw_auth.autorizacion.verificar_grupo(8,sysdate,I_USER) 
                   or snw_auth.autorizacion.verificar_grupo(9,sysdate,I_USER) 
                   or snw_auth.autorizacion.verificar_grupo(13,sysdate,I_USER) 
                   or snw_auth.autorizacion.verificar_grupo(14,sysdate,I_USER)) then
            return 'N';
        end if;
        
        
        -- El cargo tiene una actualización en proceso
        select id_cargo_padre into l_id_cargo_padre from cargo where id = I_ID_CARGO;
        
        select count(*) into l_conteo from cargo where id_cargo_padre = l_id_cargo_padre and 
        vigencia is null and estado <> snw_constantes.constante_tipo('CARGO_ANULADO');
        
        if l_conteo > 0 then
            return 'P';
        end if;
        
        -- Si es POC puede actualizar la DC
        if snw_auth.autorizacion.verificar_grupo(8,sysdate,I_USER) then
            return 'S';
        end if;
        
        -- Si es BP
        if snw_auth.autorizacion.verificar_grupo(9,sysdate,I_USER) then            
        
            -- Y la DC es Transversal, puede actualizar la DC
            select tipo_ubicacion into l_tipo_ubicacion from cargo where id = I_ID_CARGO;
            if SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') = l_tipo_ubicacion then    
                return 'S';
                
            -- Y la DC es Específica, y la UO del cargo es igual o inferior jerárquicamente a la o las UOs en que el usuario es BP.
            else                
                            
                select id into l_id_trabajador from TRABAJADOR WHERE NUMERO_IDENTIFICACION = (select NUMERO_IDENTIFICACION from usuario where USER_NAME = I_USER);
                select id_unidad_organizativa into l_id_unidad_cargo from cargo where id = I_ID_CARGO;
                
                for unidad_bp in (
                    select ID_UNIDAD_ORGANIZATIVA from BUSINESS_PARTNER where id_trabajador = l_id_trabajador)
                loop
                    
                    if ADMIN_UNIDAD_ORGANIZATIVA.PERTENECE_UNIDAD(unidad_bp.id_unidad_organizativa ,l_id_unidad_cargo) = 'S' then
                        return 'S';
                    end if;
                
                end loop;
                                
            end if; 
            
            -- return 'N'; UN BP PUEDE SER GESTOR ????
            
        end if;
  
        /*
            Primero se obtiene la UO del cargo: A
            Luego se obtiene la UO a la que está asociado el Usuario: B   
        */
        select id_unidad_organizativa into l_id_unidad_cargo from cargo where id = I_ID_CARGO;
        l_id_unidad_usuario := admin_unidad_organizativa.usuario_pertenece_unidad(I_USER);
        
        return ADMIN_UNIDAD_ORGANIZATIVA.PERTENECE_UNIDAD(l_id_unidad_usuario,l_id_unidad_cargo);        
        
    END PERMITE_EDITAR_POC;

--------------------------------------------------------------------------------------------------------- 

    FUNCTION ALERTA_BANDEJA(I_ID_CARGO NUMBER) RETURN CHAR IS
    l_fecha date;
    BEGIN
    select max(snw_flow.historia_proceso.fecha) into l_fecha
    from  cargo
    left join snw_flow.v_bandeja on (snw_flow.v_bandeja.id_instancia_activa = snw_flow.flujo.determinar_instancia_activa(cargo.id_instancia_proceso))
    left join snw_flow.historia_proceso on (snw_flow.historia_proceso.id_instancia_proceso = snw_flow.v_bandeja.id_instancia_activa)
    where cargo.id = i_id_cargo and snw_flow.historia_proceso.id_actividad = snw_flow.v_bandeja.id_actividad_vigente;
    if l_fecha-sysdate > 30 then
        return 'X';
    end if;
    return '';
    end ALERTA_BANDEJA;

--------------------------------------------------------------------------------------------------------- 

     PROCEDURE ANULAR_CARGO(I_ID_CARGO NUMBER) as
     begin
     null;
     end ANULAR_CARGO;

--------------------------------------------------------------------------------------------------------- 
    FUNCTION FECHA_CREACION(I_ID_CARGO NUMBER) RETURN DATE IS
        l_fecha date;
    BEGIN 
        select fecha_inicio  into l_fecha 
        from cargo where id = (select id_cargo_padre from cargo where id = I_ID_CARGO) and id = id_cargo_padre;
    RETURN l_fecha;
        exception when others then
            return null;
    END FECHA_CREACION;

--------------------------------------------------------------------------------------------------------- 
    FUNCTION FECHA_ACTUALIZACION(I_ID_CARGO NUMBER) RETURN DATE IS
        l_fecha date;
    BEGIN 
        select fecha_inicio  into l_fecha 
        from cargo where id = I_ID_CARGO and id <> id_cargo_padre;
    RETURN l_fecha;
        exception when others then
            return null;
    END FECHA_ACTUALIZACION;

--------------------------------------------------------------------------------------------------------

  PROCEDURE REDUNDAR_CARGOS_RESPONSABLES(I_CARGO NUMBER) IS

  l_conteo number;
  l_nombre varchar2(400);
  l_denominacion varchar2(400);

  BEGIN

    -- Se valida si hay datos en los campos
    select count(*) into l_conteo from cargo where id = I_CARGO and elaboro is not null;

    if l_conteo > 0 then

        select elaboro into l_nombre from cargo where id = I_CARGO;
        l_denominacion := FORMULARIO.DENOMINACION_DE_USUARIO(l_nombre);
        -- Se actualiza el campo nombre_elaboro
        UPDATE CARGO SET NOMBRE_ELABORO = l_denominacion where id = I_CARGO;
    end if;        

    select count(*) into l_conteo from cargo where id = I_CARGO and reviso is not null;

    if l_conteo > 0 then

        select reviso into l_nombre from cargo where id = I_CARGO;
        l_denominacion := FORMULARIO.DENOMINACION_DE_USUARIO(l_nombre);
        -- Se actualiza el campo nombre_reviso
        UPDATE CARGO SET NOMBRE_REVISO = l_denominacion where id = I_CARGO;        
    end if;

    select count(*) into l_conteo from cargo where id = I_CARGO and aprobo is not null;

    if l_conteo > 0 then

        select aprobo into l_nombre from cargo where id = I_CARGO;
        l_denominacion := FORMULARIO.DENOMINACION_DE_USUARIO(l_nombre);
        -- Se actualiza el campo nombre_aprobo
        UPDATE CARGO SET NOMBRE_APROBO = l_denominacion where id = I_CARGO;      
    end if;

  END REDUNDAR_CARGOS_RESPONSABLES;    

  --------------------------------------------------------------------------------------------------------

  PROCEDURE REDUNDAR_UBICACION_JERARQUICA(I_CARGO NUMBER) IS

  l_conteo number;
  l_unidad number;
  l_nombre varchar2(400);

  BEGIN

    -- Se valida si se trata de un cargo de tipo especifico (Si es transversal, no tiene ubicación jerárquica)
    select count(*) into l_conteo from cargo where id = I_CARGO and ID_UNIDAD_ORGANIZATIVA is not null;

    if l_conteo > 0 then

        -- Se obtiene la unidad del cargo
        select id_unidad_organizativa into l_unidad from cargo where id = I_CARGO;

        select GERENCIA into l_nombre from V_RAMA_UNIDAD_NOMBRE where id = l_unidad;
        -- Se actualiza el campo nombre_gerencia
        UPDATE CARGO SET NOMBRE_GERENCIA = l_nombre where id = I_CARGO;

        select SUBGERENCIA into l_nombre from V_RAMA_UNIDAD_NOMBRE where id = l_unidad;
        -- Se actualiza el campo nombre_subgerencia
        UPDATE CARGO SET NOMBRE_SUBGERENCIA = l_nombre where id = I_CARGO;               

        select DIVISION into l_nombre from V_RAMA_UNIDAD_NOMBRE where id = l_unidad;
        -- Se actualiza el campo nombre_Division
        UPDATE CARGO SET NOMBRE_DIVISION = l_nombre where id = I_CARGO;           

        select DEPARTAMENTO into l_nombre from V_RAMA_UNIDAD_NOMBRE where id = l_unidad;
        -- Se actualiza el campo nombre_departamento
        UPDATE CARGO SET NOMBRE_DEPARTAMENTO = l_nombre where id = I_CARGO;       
    end if;

  END REDUNDAR_UBICACION_JERARQUICA;  


  --------------------------------------------------------------------------------------------------------

  PROCEDURE REDUNDAR_REQUISITOS(I_CARGO NUMBER) IS

  l_conteo number;
  l_foranea number;
  l_subgrupo number;

  l_digital_transversal varchar2(400);

  BEGIN


    /* Se requiere redundar los siguientes campos, que durante el flujo de las bandejas, solo se muestran como derivados del subgrupo

    NOTA: A diferencia de los demas redundar, este procedimiento graba las foraneas, a excepción del campo DIGITAL_TRANSVERSAL.

        BASICO_NIVEL 
        NIVEL_COMPLEMENTARIO 
        IDIOMA_NIVEL
        IDIOMA_ESPECIFICACION 
        DIGITAL_TRANSVERSAL
    */

    -- Se valida si el subgrupo no es nulo
    select count(*) into l_conteo from cargo where id = I_CARGO and SUBGRUPO_PROFESIONAL is not null;

    if l_conteo > 0 then

        -- Se obtiene el subgrupo profesional del cargo
        select SUBGRUPO_PROFESIONAL into l_subgrupo from cargo where id = I_CARGO;

        select tipo into l_foranea from subgrupo_profesional where id = l_subgrupo;
        UPDATE CARGO SET BASICO_NIVEL = l_foranea where id = I_CARGO;

        select tipo_complementario into l_foranea from subgrupo_profesional where id = l_subgrupo;
        UPDATE CARGO SET NIVEL_COMPLEMENTARIO = l_foranea where id = I_CARGO;

        select ingles into l_foranea from subgrupo_profesional where id = l_subgrupo;
        UPDATE CARGO SET IDIOMA_NIVEL = l_foranea where id = I_CARGO;

        select ingles_especificacion into l_foranea from subgrupo_profesional where id = l_subgrupo;
        UPDATE CARGO SET IDIOMA_ESPECIFICACION = l_foranea where id = I_CARGO;

        select nombre into l_digital_transversal from tipo where tipo = SNW_CONSTANTES.CONSTANTE_TIPO('DIGITAL_TRANSVERSAL');
        UPDATE CARGO SET DIGITAL_TRANSVERSAL = l_digital_transversal where id = I_CARGO;

    end if;

  END REDUNDAR_REQUISITOS;  


--------------------------------------------------------------------------------------------------------
FUNCTION BUSCAR_CARGO_VIGENTE(I_ID_CARGO NUMBER) RETURN NUMBER IS
    l_id number;
BEGIN
    select id into l_id from cargo where id_cargo_padre in 
    (select id_cargo_padre from cargo where id = I_ID_CARGO) and VERSIONADOR_PCK.cargo_vigente(ID) = 'S';
    return l_id;
    exception when no_data_found then
        return null;
END BUSCAR_CARGO_VIGENTE;

--------------------------------------------------------------------------------------------------------
FUNCTION CARGO_POS_RES(I_ID_CARGO NUMBER) RETURN BOOLEAN IS
    l_conteo number;
    l_id_cargo number;
BEGIN
    select id_cargo_padre into l_id_cargo from cargo where id = i_id_cargo;
    select count(*) into l_conteo from posicion where pos_res='X' and 
    id in ( select id_posicion from posicion_cargo where id_cargo = l_id_cargo and 
    (sysdate between fecha_inicio and fecha_fin or fecha_fin is null));
    RETURN l_conteo > 0;
    exception when no_data_found then
        return false;
END CARGO_POS_RES;

--------------------------------------------------------------------------------------------------------    
FUNCTION CARGO_SUBGRUPO(I_ID_CARGO NUMBER) RETURN VARCHAR2 IS
    l_nombre varchar2(4000);
BEGIN
    select 
    upper(substr(substr(nombre,instr(nombre,'-')+2),decode(instr(substr(nombre,instr(nombre,'-')+2),'-'),0,0,instr(substr(nombre,instr(nombre,'-')+2),'-')+2))) 
    into l_nombre
    from subgrupo_profesional where id = (select subgrupo_profesional from cargo where id = i_id_cargo);
    RETURN l_nombre;
    exception  when no_data_found then
        return '';
END CARGO_SUBGRUPO;
END VERSIONADOR_PCK;

/
