--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package ADMIN_TAREAS_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "SNW_CARGOS"."ADMIN_TAREAS_PCK" AS 

  PROCEDURE GENERAR_AUD_CARGO ( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 );
  PROCEDURE GENERAR_NUEVA_VERSION ( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 );
  PROCEDURE ACTUALIZAR_CARGO ( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 );
  PROCEDURE NOTIFICAR_GESTOR_BP( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 );
  PROCEDURE NOTIFICAR_POC_BP( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 );
  PROCEDURE NOTIFICAR_BP_GESTOR( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 );
  PROCEDURE NOTIFICAR_BP_POC( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 );
END ADMIN_TAREAS_PCK;

/

  GRANT EXECUTE ON "SNW_CARGOS"."ADMIN_TAREAS_PCK" TO "SNW_FLOW";
