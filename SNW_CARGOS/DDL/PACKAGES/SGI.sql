--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package SGI
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "SNW_CARGOS"."SGI" AS 
--   PROCEDURE CREAR_NUEVA_SGIVERSION ( i_version IN NUMBER );
     PROCEDURE NOTIFICACION_RESPONSABILIDADES ( i_version IN NUMBER );
     PROCEDURE ENVIAR_RECORDATORIO ( i_version IN NUMBER );
     PROCEDURE ENVIO_CORREO_ELECTRONICO (i_destino IN NUMBER, i_version IN NUMBER, i_tipo NUMBER);
     FUNCTION  DETERMINAR_SUBGRUPO(I_SG VARCHAR2) RETURN NUMBER; 
     FUNCTION  DETERMINAR_ROLES(I_TRABAJADOR NUMBER) RETURN VARCHAR; 
     FUNCTION  DETERMINAR_CLASES(I_SGI_VERSION NUMBER) RETURN VARCHAR;
     FUNCTION  DETERMINAR_NIVELES_SUBGRUPO(I_SUBGRUPO NUMBER) RETURN VARCHAR;
     FUNCTION  DETERMINAR_SUBGRUPO_NIVELES(I_NIVEL NUMBER) RETURN VARCHAR;
     FUNCTION  VALIDAR_USUARIO_TRABAJADOR(I_USER VARCHAR2,I_TRABAJADOR NUMBER) RETURN BOOLEAN;

     ----PROCEDURE ACTUALIZAR_VIGENCIA_SGIVERSION( i_number IN NUMBER );
  /* TODO enter package declarations (types, exceptions, methods etc) here */ 

END SGI;

/
