--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package PRUEBAS_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "SNW_CARGOS"."PRUEBAS_PCK" AS 
  
  --Esta funci�n retorna el id, nombre y user_name de los BPs que pueden avalar una DC
  function bp_from_cargo(i_id_cargo CARGO.id%type) return VARCHAR2;

  --Este procedimiento asigna a un trabajador a una UO (mediante su posici�n vigente), creando la posici�n si es necesario
  procedure trabajador_a_unidad(i_id_trabajador number, i_id_unidad number);

  --funci�n para ejecutar una eliminaci�n de cargo una vez se valide que se puede eliminar
  function ejecutar_eliminacion_cargo(i_id_cargo CARGO.id%type) return BOOLEAN;

  --Este procedimiento elimina una versi�n de una descripci�n de cargo
  procedure eliminar_version_cargo(i_id_cargo CARGO.id%type);

END PRUEBAS_PCK;


/
