
  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_BUSINESS_PARTNER" 
BEFORE INSERT OR UPDATE ON BUSINESS_PARTNER FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_BUSINESS_PARTNER" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_CARGO" 
BEFORE INSERT OR UPDATE ON CARGO FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_CARGO" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_CONCEPTO_CARGO" 
BEFORE INSERT OR UPDATE ON CONCEPTO_CARGO FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_CONCEPTO_CARGO" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_DECISION" 
BEFORE INSERT OR UPDATE ON DECISION FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_DECISION" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_DIMENSION" 
BEFORE INSERT OR UPDATE ON DIMENSION FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_DIMENSION" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_LICENCIA_MATRICULA" 
BEFORE INSERT OR UPDATE ON LICENCIA_MATRICULA FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;       
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_LICENCIA_MATRICULA" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_POSICION" 
BEFORE INSERT OR UPDATE ON POSICION FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_POSICION" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_POSICION_CARGO" 
BEFORE INSERT OR UPDATE ON POSICION_CARGO FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_POSICION_CARGO" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_POSICION_TRABAJADOR" 
BEFORE INSERT OR UPDATE ON POSICION_TRABAJADOR FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_POSICION_TRABAJADOR" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_POSICION_UO" 
BEFORE INSERT OR UPDATE ON POSICION_UO FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_POSICION_UO" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_RESPON_CONTACTOS" 
BEFORE INSERT OR UPDATE ON RESPONSABILIDAD_CONTACTOS FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;


ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_RESPON_CONTACTOS" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_RESPON_ESTRATEGIA" 
BEFORE INSERT OR UPDATE ON RESPONSABILIDAD_ESTRATEGIA FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;


ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_RESPON_ESTRATEGIA" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_RESPON_PRINCIPAL" 
BEFORE INSERT OR UPDATE ON RESPONSABILIDAD_PRINCIPAL FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;


ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_RESPON_PRINCIPAL" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_SGI_ACEPTACION" 
BEFORE INSERT OR UPDATE ON SGI_ACEPTACION FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_SGI_ACEPTACION" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_SGI_RESPONSABILIDAD" 
BEFORE INSERT OR UPDATE ON SGI_RESPONSABILIDAD FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;       
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_SGI_RESPONSABILIDAD" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_SGI_VERSION" 
BEFORE INSERT OR UPDATE ON SGI_VERSION FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_SGI_VERSION" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_SOFTWARE" 
BEFORE INSERT OR UPDATE ON SOFTWARE FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;       
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_SOFTWARE" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_SUBGRUPO_PROFESIONAL" 
BEFORE INSERT OR UPDATE ON SUBGRUPO_PROFESIONAL FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_SUBGRUPO_PROFESIONAL" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_TEMP_CARGOS_INICIALES" 
BEFORE INSERT OR UPDATE ON TEMP_CARGOS_INICIALES FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;       
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_TEMP_CARGOS_INICIALES" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_TEMPO_PLANTILLA_CARGO" 
BEFORE INSERT OR UPDATE ON TEMPO_PLANTILLA_CARGO FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_TEMPO_PLANTILLA_CARGO" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_TIPO" 
BEFORE INSERT OR UPDATE ON TIPO FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_TIPO" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_UNIDAD_ORGANIZATIVA" 
BEFORE INSERT OR UPDATE ON UNIDAD_ORGANIZATIVA FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_UNIDAD_ORGANIZATIVA" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BUSINESS_PARTNER_TRG" 
BEFORE INSERT ON BUSINESS_PARTNER 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT BUSINESS_PARTNER_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;

ALTER TRIGGER "SNW_CARGOS"."BUSINESS_PARTNER_TRG" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."CARGO_COMPETENCIA_TRG" 
BEFORE INSERT ON CARGO_COMPETENCIA_ESPECIFICA 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT CARGO_COMPETENCIA_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "SNW_CARGOS"."CARGO_COMPETENCIA_TRG" ENABLE

  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."CARGOS_REPORTAN_TRG" 
BEFORE INSERT ON CARGOS_REPORTAN 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT CARGOS_REPORTAN_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;

ALTER TRIGGER "SNW_CARGOS"."CARGOS_REPORTAN_TRG" ENABLE

