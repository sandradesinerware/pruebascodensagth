
  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_BANDEJA" ("ID", "CODIGO_FUNCION", "DENOMINACION", "JUSTIFICACION", "ID_INSTANCIA_PROCESO", "ELABORO", "ESTADO", "ID_ESTADO", "TIPO_UBICACION", "FECHA_CREACION", "ID_ACTIVIDAD_VIGENTE", "ACTIVIDAD_ACTUAL", "NOMBRE_UNIDAD", "NOMBRE_GERENCIA", "NOMBRE_SUBGERENCIA", "NOMBRE_DIVISION", "NOMBRE_DEPARTAMENTO", "FECHA_ACTIVIDAD", "ALERTA", "ID_UNIDAD_ORGANIZATIVA") AS 
  select 
cargo.id,
cargo.codigo_funcion,
cargo.denominacion,
cargo.justificacion,
cargo.ID_INSTANCIA_PROCESO,
cargo.ELABORO,
(select nombre from tipo where id = CARGO.ESTADO) estado,
CARGO.ESTADO id_estado,
cargo.tipo_ubicacion,
CARGO.FECHA_INICIO FECHA_CREACION,
snw_flow.v_bandeja.id_actividad_vigente,
snw_flow.flujo.nombre_actividad(snw_flow.flujo.determinar_instancia_activa(cargo.id_instancia_proceso)) ACTIVIDAD_ACTUAL,
v_rama_unidad_nombre.unidad nombre_unidad,
v_rama_unidad_nombre.gerencia nombre_gerencia,
v_rama_unidad_nombre.subgerencia nombre_subgerencia,
v_rama_unidad_nombre.division nombre_division,
v_rama_unidad_nombre.departamento nombre_departamento,
(select max(fecha) from snw_flow.historia_proceso where id_instancia_proceso =snw_flow.v_bandeja.id_instancia_activa and id_actividad = snw_flow.v_bandeja.id_actividad_vigente) fecha_actividad,
VERSIONADOR_PCK.ALERTA_BANDEJA(cargo.id) alerta,
cargo.id_unidad_organizativa
from cargo
left join snw_flow.v_bandeja on (snw_flow.v_bandeja.id_instancia_activa = snw_flow.flujo.determinar_instancia_activa(cargo.id_instancia_proceso))
left join v_rama_unidad_nombre on (v_rama_unidad_nombre.id = cargo.id_unidad_organizativa)
where id_proceso = 141

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_CARGO_CONSOLIDADO" ("ID", "ESTADO", "FECHA_INICIO", "FECHA_FIN", "CODIGO_FUNCION", "DENOMINACION", "SUBGRUPO_PROFESIONAL", "NOMBRE_GERENCIA", "NOMBRE_SUBGERENCIA", "NOMBRE_DIVISION", "NOMBRE_DEPARTAMENTO", "SOLICITANTE") AS 
  select 
ID,
CASE
  WHEN id <> id_Cargo_padre THEN 'Actualizada'
  WHEN id = id_Cargo_padre THEN 'Creada' 
END ESTADO, 
FECHA_INICIO, -- Fecha en que ENTRO a la instancia del proceso
FECHA_FIN, -- Fecha en que SALIO a la instancia del proceso
CODIGO_FUNCION,
DENOMINACION,
(select nombre from subgrupo_profesional where id = SUBGRUPO_PROFESIONAL) SUBGRUPO_PROFESIONAL,
(select GERENCIA from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_GERENCIA,
(select SUBGERENCIA from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_SUBGERENCIA,
(select DIVISION from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_DIVISION,
(select DEPARTAMENTO from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_DEPARTAMENTO,
FORMULARIO.DENOMINACION_DE_USUARIO(elaboro) SOLICITANTE
from cargo
where (VIGENCIA = 'S' or VIGENCIA = 'N') and VERSION_CARGO is not null
order by fecha_inicio asc

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_CARGO_NO_VIGENTE" ("ID", "ID_UNIDAD_ORGANIZATIVA", "NOMBRE_UNIDAD", "SUBGRUPO_PROFESIONAL", "DENOMINACION", "CODIGO_FUNCION", "GERENCIA", "SUBGERENCIA", "DIVISION", "DEPARTAMENTO", "FECHA_INICIO", "PERSONAS_IMPACTO") AS 
  select 
cargo.id, 
cargo.id_unidad_organizativa, 
(select nombre from unidad_organizativa where cargo.id_unidad_organizativa = id) NOMBRE_UNIDAD,
cargo.subgrupo_profesional, 
cargo.denominacion, 
cargo.codigo_funcion,
v_rama_unidad_nombre.gerencia gerencia,
v_rama_unidad_nombre.subgerencia subgerencia,
v_rama_unidad_nombre.division division,
v_rama_unidad_nombre.departamento departamento,
fecha_inicio fecha_inicio,
ADMIN_UNIDAD_ORGANIZATIVA.PERSONAS_IMPACTO(cargo.id) personas_impacto
from cargo
left join v_rama_unidad_nombre on (v_rama_unidad_nombre.id = cargo.id_unidad_organizativa)
where VERSIONADOR_PCK.CARGO_VIGENTE(cargo.id) = 'N'

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_CARGO_PROCESO" ("ID", "ESTADO", "FECHA_ACTIVIDAD", "TIEMPO_ACTIVIDAD", "CODIGO_FUNCION", "DENOMINACION", "NOMBRE_GERENCIA", "NOMBRE_SUBGERENCIA", "NOMBRE_DIVISION", "NOMBRE_DEPARTAMENTO", "SOLICITANTE") AS 
  select 
ID,
ESTADO,
FECHA_ACTIVIDAD,
trunc(to_date(sysdate,'DD/MM/YYYY'))-trunc(to_date(FECHA_ACTIVIDAD,'DD/MM/YYYY')) as TIEMPO_ACTIVIDAD,
CODIGO_FUNCION,
DENOMINACION,
NOMBRE_GERENCIA,
NOMBRE_SUBGERENCIA,
NOMBRE_DIVISION,
NOMBRE_DEPARTAMENTO,
FORMULARIO.DENOMINACION_DE_USUARIO(elaboro) SOLICITANTE
from V_BANDEJA
WHERE ID_ACTIVIDAD_VIGENTE in (325, 345, 346)
order by fecha_actividad asc

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_CARGO_VIGENCIA" ("ID", "ESTADO", "FECHA_INICIO", "FECHA_FIN", "CODIGO_FUNCION", "VERSION", "DENOMINACION", "SUBGRUPO_PROFESIONAL", "NOMBRE_GERENCIA", "NOMBRE_SUBGERENCIA", "NOMBRE_DIVISION", "NOMBRE_DEPARTAMENTO", "SOLICITANTE") AS 
  select 
ID,
decode(vigencia,'S','Vigente','N','No Vigente') ESTADO, 
FECHA_INICIO, 
FECHA_FIN, 
CODIGO_FUNCION,
version_Cargo version,
DENOMINACION,
(select nombre from subgrupo_profesional where id = SUBGRUPO_PROFESIONAL) SUBGRUPO_PROFESIONAL,
(select GERENCIA from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_GERENCIA,
(select SUBGERENCIA from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_SUBGERENCIA,
(select DIVISION from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_DIVISION,
(select DEPARTAMENTO from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_DEPARTAMENTO,
FORMULARIO.DENOMINACION_DE_USUARIO(elaboro) SOLICITANTE
from cargo
where (VIGENCIA = 'S' or VIGENCIA = 'N') and VERSION_CARGO is not null
order by fecha_inicio asc

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_CARGO_VIGENTE" ("ID", "ID_UNIDAD_ORGANIZATIVA", "NOMBRE_UNIDAD", "SUBGRUPO_PROFESIONAL", "DENOMINACION", "CODIGO_FUNCION", "GERENCIA", "SUBGERENCIA", "DIVISION", "DEPARTAMENTO", "FECHA_INICIO", "ACTIVO", "PERSONAS_IMPACTO") AS 
  select 
cargo.id, 
cargo.id_unidad_organizativa, 
(select nombre from unidad_organizativa where cargo.id_unidad_organizativa = id) NOMBRE_UNIDAD,
cargo.subgrupo_profesional, 
cargo.denominacion, 
cargo.codigo_funcion,
v_rama_unidad_nombre.gerencia gerencia,
v_rama_unidad_nombre.subgerencia subgerencia,
v_rama_unidad_nombre.division division,
v_rama_unidad_nombre.departamento departamento,
fecha_inicio fecha_inicio,
activo,
ADMIN_UNIDAD_ORGANIZATIVA.PERSONAS_IMPACTO(cargo.id) personas_impacto
from cargo
left join v_rama_unidad_nombre on (v_rama_unidad_nombre.id = cargo.id_unidad_organizativa)
where VERSIONADOR_PCK.CARGO_VIGENTE(cargo.id) = 'S'

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_COMPETENCIAS_CARGO" ("ID_CARGO", "TIPO", "SKILL", "ESPECIFICACION") AS 
  'Específica' tipo,(select nombre from digital_skill where id = id_digital_skill) skill,especificacion from CARGO_COMPETENCIA_ESPECIFICA

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_POSICION" ("ID_CARGO", "ID_POSICION", "ID_UNIDAD_ORGANIZATIVA", "NOMBRE_UNIDAD", "NOMBRES", "APELLIDOS", "NUMERO_IDENTIFICACION", "FECHA_INICIO", "FECHA_FIN", "CODIGO_POSICION", "POS_RES") AS 
  select 
VERSIONADOR_PCK.BUSCAR_CARGO_VIGENTE( ADMIN_UNIDAD_ORGANIZATIVA.BUSCAR_CARGO_TRABAJADOR(posicion_trabajador.id_trabajador)) id_cargo, 
posicion.id id_posicion,
posicion.id_unidad_organizativa id_unidad_organizativa, 
(select nombre from unidad_organizativa where id = posicion.id_unidad_organizativa) nombre_unidad,
trabajador.nombres,
trabajador.apellidos,
trabajador.numero_identificacion,
posicion_trabajador.fecha_inicio fecha_inicio,
posicion_trabajador.fecha_fin fecha_fin,
posicion.codigo codigo_posicion,
posicion.pos_res pos_res
from posicion  
left join posicion_trabajador on (posicion_trabajador.id_posicion = posicion.id)
left join TRABAJADOR on (TRABAJADOR.id = posicion_trabajador.id_trabajador)
where  (sysdate between posicion_trabajador.fecha_inicio and posicion_trabajador.fecha_fin or posicion_trabajador.fecha_fin is null)

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_POSICION_CARGO" ("ID_CARGO", "ID_POSICION", "DENOMINACION", "ID_TIPO_UBICACION", "TIPO_UBICACION", "ID_UNIDAD_ORGANIZATIVA", "GERENCIA", "SUBGERENCIA", "DIVISION", "DEPARTAMENTO", "NOMBRES", "APELLIDOS", "NUMERO_IDENTIFICACION", "FECHA_INICIO", "FECHA_FIN", "CODIGO", "CODIGO_OCUPACION", "POS_RES") AS 
  select 
cargo.id id_cargo, 
posicion.id id_posicion,
cargo.denominacion,
cargo.tipo_ubicacion id_tipo_ubicacion,
(select nombre from tipo where id = cargo.tipo_ubicacion) tipo_ubicacion,
posicion.id_unidad_organizativa id_unidad_organizativa, 
v_rama_unidad_nombre.gerencia gerencia,
v_rama_unidad_nombre.subgerencia subgerencia,
v_rama_unidad_nombre.division division,
v_rama_unidad_nombre.departamento departamento,
TRABAJADOR.nombres,
TRABAJADOR.apellidos,
TRABAJADOR.numero_identificacion,
posicion_trabajador.fecha_inicio fecha_inicio,
posicion_trabajador.fecha_fin fecha_fin,
posicion.codigo,
cargo.codigo_funcion codigo_ocupacion,
posicion.pos_res pos_res
from posicion_trabajador 
left join posicion_cargo on (posicion_cargo.id_posicion = posicion_trabajador.id_posicion and 
sysdate between posicion_cargo.fecha_inicio and posicion_cargo.fecha_fin or posicion_cargo.fecha_fin is null)
left join cargo on (cargo.id = posicion_cargo.id_cargo)
left join posicion on (posicion_trabajador.id_posicion = posicion.id)
left join TRABAJADOR on (TRABAJADOR.id = posicion_trabajador.id_trabajador)
left join v_rama_unidad_nombre on (v_rama_unidad_nombre.id = posicion.id_unidad_organizativa)
where  (sysdate between posicion_trabajador.fecha_inicio and posicion_trabajador.fecha_fin or posicion_trabajador.fecha_fin is null)

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_POSICION_UO" ("NUMERO_IDENTIFICACION", "ID_POSICION", "POSICION", "ID_UNIDAD_ORGANIZATIVA", "UNIDAD_ORGANIZATIVA") AS 
  select 
    trabajador.numero_identificacion,
    posicion.id id_posicion,
    posicion.nombre posicion,
    posicion_uo.id_unidad_organizativa id_unidad_organizativa, 
    (select nombre from unidad_organizativa where id = posicion_uo.id_unidad_organizativa) unidad_organizativa
from posicion  
left join posicion_uo on (posicion_uo.id_posicion = posicion.id)
left join posicion_trabajador on (posicion_trabajador.id_posicion = posicion.id)
left join TRABAJADOR on (TRABAJADOR.id = posicion_trabajador.id_trabajador)
where ((sysdate between posicion_trabajador.fecha_inicio and posicion_trabajador.fecha_fin or posicion_trabajador.fecha_fin is null) and
(sysdate between posicion_uo.fecha_inicio and posicion_uo.fecha_fin or posicion_uo.fecha_fin is null))

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_POSICION_VIGENTE" ("POS_ID", "POS_COD", "POS_UO_TEO_VIG", "POS_UO_TEO_VIG_COD", "POS_UO_TEO_VIG_NOM", "POS_UO_VIG", "POS_UO_VIG_COD", "POS_UO_VIG_NOM", "POS_UO_LAST", "POS_UO_LAST_COD", "POS_UO_LAST_NOM", "CARGO_PADRE_VIG", "CARGO_PADRE_CODIGO_DC_VIG", "CARGO_PADRE_DENOMINACION_VIG", "CARGO_PADRE_VERSION_VIG", "CARGO_PADRE_TIPO_VIG", "CARGO_PADRE_UO_VIG", "CARGO_PADRE_UO_VIG_NOM", "ASIGNADO_VIG", "ASIGNADO_VIG_NOMBRES", "ASIGNADO_INICIO", "ASIGNADO_LAST", "ASIGNADO_LAST_NOMBRES", "ASIGNADO_LAST_FIN") AS 
  select 
p.id pos_id, p.codigo pos_cod, p.id_unidad_organizativa pos_uo_teo_vig, (select NVL(codigo_emgesa,codigo_codensa) from unidad_organizativa where id = p.id_unidad_organizativa) pos_uo_teo_vig_cod
, ADMIN_UNIDAD_ORGANIZATIVA.nombre_unidad(p.id_unidad_organizativa) pos_uo_teo_vig_nom
, puo.id_unidad_organizativa pos_uo_vig, (select NVL(codigo_emgesa,codigo_codensa) from unidad_organizativa where id = puo.id_unidad_organizativa) pos_uo_vig_cod
, ADMIN_UNIDAD_ORGANIZATIVA.nombre_unidad(puo.id_unidad_organizativa) pos_uo_vig_nom
, puo_last.id_posicion pos_uo_last, (select NVL(codigo_emgesa, codigo_codensa) from unidad_organizativa where id = puo_last.id_unidad_organizativa) pos_uo_last_cod
, ADMIN_UNIDAD_ORGANIZATIVA.nombre_unidad(puo_last.id_unidad_organizativa) pos_uo_last_nom
, c_padre.id cargo_padre_vig, c_padre.codigo_funcion cargo_padre_codigo_dc_vig, c_padre.denominacion cargo_padre_denominacion_vig
, c_padre.version_cargo cargo_padre_version_vig
, SNW_CONSTANTES.get_nombre_tipo(c_padre.tipo_ubicacion) cargo_padre_tipo_vig, c_padre.id_unidad_organizativa cargo_padre_uo_vig
, ADMIN_UNIDAD_ORGANIZATIVA.nombre_unidad(c_padre.id_unidad_organizativa) cargo_padre_uo_vig_nom
, t.numero_identificacion asignado_vig, t.nombres ||' '|| t.apellidos asignado_vig_nombres
, to_char(pt.fecha_inicio,'DD/MM/YYYY HH24:MI') asignado_inicio
, t_last.numero_identificacion asignado_last, t_last.nombres ||' '|| t_last.apellidos asignado_last_nombres
, to_char(pt_last.fecha_fin,'DD/MM/YYYY HH24:MI') asignado_last_fin
from posicion p 
left join posicion_uo puo on (p.id = puo.id_posicion and puo.fecha_fin is null)
left join (
  select id, id_posicion, id_unidad_organizativa, fecha_inicio, fecha_fin
     ,rank() over ( PARTITION BY id_posicion ORDER BY fecha_fin desc ) rango_desc
  from posicion_uo
  where fecha_fin is not null
  ) puo_last on ( puo_last.id_posicion = p.id and puo_last.rango_desc = 1 )
left join posicion_cargo pc on (p.id = pc.id_posicion and pc.fecha_fin is null)
left join cargo c_padre on (pc.id_cargo = c_padre.id)
left join posicion_trabajador pt on (p.id = pt.id_posicion and pt.fecha_fin is null)
left join trabajador t on (pt.id_trabajador = t.id)
left join (
  select id, id_posicion, id_trabajador, fecha_inicio, fecha_fin
    ,rank() over ( PARTITION BY id_posicion ORDER BY fecha_fin desc ) rango_desc
  from posicion_trabajador
  where fecha_fin is not null
  ) pt_last on (p.id = pt_last.id_posicion and pt_last.rango_desc = 1)
left join trabajador t_last on (pt_last.id_trabajador = t_last.id)

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_RAMA_UNIDAD_ID" ("ID", "ID_GERENCIA", "ID_SUB_GERENCIA", "ID_DIVISION", "ID_DEPARTAMENTO") AS 
  select 
id,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('GERENCIA')) id_gerencia,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('SUBGERENCIA')) id_sub_gerencia,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('DIVISION')) id_division,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('DEPARTAMENTO')) id_departamento
from unidad_organizativa

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_RAMA_UNIDAD_NOMBRE" ("ID", "UNIDAD", "GERENCIA", "SUBGERENCIA", "DIVISION", "DEPARTAMENTO") AS 
  select 
id, 
admin_unidad_organizativa.NOMBRE_UNIDAD(id) Unidad,
admin_unidad_organizativa.NOMBRE_UNIDAD(id_gerencia) gerencia,
admin_unidad_organizativa.NOMBRE_UNIDAD(id_sub_gerencia) subgerencia,
admin_unidad_organizativa.NOMBRE_UNIDAD(id_division) division,
admin_unidad_organizativa.NOMBRE_UNIDAD(id_departamento) departamento 
from V_RAMA_UNIDAD_ID

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."VR_DESCRIPCION_CARGO" ("ID", "ID_UNIDAD_ORGANIZATIVA", "DENOMINACION", "SUBGRUPO", "TIPO_UBICACION", "CODIGO_FUNCION", "SUBGRUPO_PROFESIONAL", "CATEGORY_ENEL", "ROLES", "FUNCTIONAL", "HOMOLOGACION", "CODIGO_HOMOLOGACION", "FECHA_CREACION", "FECHA_ACTUALIZACION", "VERSION", "GERENCIA", "SUBGERENCIA", "DIVISION", "DEPARTAMENTO", "CARGO_SUPERIOR", "PERSONAS_DIRECTAS", "PERSONAS_INDIRECTAS", "RECURSOS_EMPRESAS", "RECURSOS_PERSONAS", "TOTAL", "MISION", "BASICO_NIVEL", "BASICO_ESPECIFICACION", "EXTRA_NIVEL", "COMPLEMENTARIO_ESPECIFICACION", "EXPERIENCIA", "INGLES_NIVEL", "INGLES_ESPECIFICACION", "COMPLEMENTARIO_NIVEL", "IDIOMA_EXTRA_ESPECIFICACION", "DIGITAL_TRANSVERSAL", "DIGITAL_ESPECIFICA", "DISCAPACIDAD", "DETALLES_DISCAPACIDAD", "TELETRABAJO", "DETALLES_TELETRABAJO", "OTROS", "ELABORO", "REVISO", "APROBO", "SOFTWARE", "LICENCIAS", "SKILLS", "ECONOMICAS", "CUALITATIVAS", "TOMAR", "PROPONER", "INTERNAS", "EXTERNAS", "REPORTAN", "VIGENCIA") AS 
  select 
id,
id_unidad_organizativa,
denominacion, 
subgrupo_profesional subgrupo,
tipo_ubicacion,
nvl(TO_CHAR(codigo_funcion),'-') codigo_funcion,
nvl((select nombre from SUBGRUPO_PROFESIONAL where id = SUBGRUPO_PROFESIONAL),'-') subgrupo_profesional, 
nvl((select nombre from tipo where id = category_enel),'-') category_enel, 
nvl((select nombre from tipo where id = roles),'-') roles,
nvl((select nombre from tipo where id = functional_area),'-') functional,
nvl(homologacion,'-') homologacion,
nvl(codigo_homologacion,'-') codigo_homologacion,
nvl(TO_CHAR(VERSIONADOR_PCK.FECHA_CREACION(id)),'-') fecha_creacion,
nvl(TO_CHAR(VERSIONADOR_PCK.FECHA_ACTUALIZACION(id)),'-') fecha_actualizacion,
nvl(TO_CHAR(version_cargo),'-') version,
nvl(nombre_gerencia,'-') gerencia,
nvl(nombre_subgerencia,'-') subgerencia,
nvl(nombre_division,'-') division,
nvl(nombre_departamento,'-') departamento,
nvl(cargo_superior_jerarquico,'No aplica') cargo_superior,
CASE  
when SUBGRUPO_PROFESIONAL not in(select id from subgrupo_profesional where jefe='S') or tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') then 'No aplica' 
else nvl(TO_CHAR(personas_directas),'-') end personas_directas,
CASE  
when SUBGRUPO_PROFESIONAL not in(select id from subgrupo_profesional where jefe='S') or tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') then 'No aplica' 
else nvl(TO_CHAR(personas_indirectas),'-') end personas_indirectas,
CASE  
when SUBGRUPO_PROFESIONAL not in(select id from subgrupo_profesional where jefe='S') or tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') then 'No aplica' 
else nvl(TO_CHAR(recursos_empresas),'-') end recursos_empresas,
CASE  
when SUBGRUPO_PROFESIONAL not in(select id from subgrupo_profesional where jefe='S') or tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') then 'No aplica' 
else nvl(TO_CHAR(recursos_personas),'-') end recursos_personas, 
CASE  
when SUBGRUPO_PROFESIONAL not in(select id from subgrupo_profesional where jefe='S') or tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') then 'No aplica' 
else nvl(TO_CHAR(personas_directas + personas_indirectas),'-') end total,
nvl(mision,'-') mision,
(select nombre from tipo where id = basico_nivel) basico_nivel,
basico_Especificacion,
(select nombre from tipo where id = nivel_complementario) extra_nivel,
nvl(complementario_especificacion,'-') complementario_especificacion,
nvl(experiencia,'-') experiencia,
(select nombre from tipo where id = idioma_nivel) ingles_nivel,
(select nombre from tipo where id = idioma_especificacion) ingles_especificacion,
nvl((select nombre from tipo where id = complementario_nivel),'-') complementario_nivel,
nvl(IDIOMA_EXTRA_ESPECIFICACION,'-') IDIOMA_EXTRA_ESPECIFICACION,
digital_transversal,
nvl(digital_especifica,'-') digital_especifica,
decode(discapacidad, 'S', 'Si', 'N', 'No', null, 'Si') discapacidad,
detalles_discapacidad,
nvl((select nombre from tipo where id = teletrabajo),'-') teletrabajo,
detalles_teletrabajo,
nvl(otros,'-') otros,
nombre_elaboro elaboro,
nombre_reviso reviso,
nombre_aprobo aprobo,
(SELECT count(*) FROM SOFTWARE WHERE ID_CARGO = cargo.id) software,
(SELECT COUNT(*) FROM LICENCIA_MATRICULA WHERE ID_CARGO = cargo.id) licencias,
(SELECT COUNT(*) FROM CARGO_COMPETENCIA_ESPECIFICA WHERE ID_CARGO = cargo.id) skills,
(SELECT count(*) FROM DIMENSION WHERE TIPO_VARIABLE = SNW_CONSTANTES.CONSTANTE_TIPO('ECONOMICA') AND ID_CARGO = cargo.id) economicas,
(SELECT count(*) FROM DIMENSION WHERE TIPO_VARIABLE = SNW_CONSTANTES.CONSTANTE_TIPO('CUALITATIVA') AND ID_CARGO = cargo.id) cualitativas,
(SELECT count(*) FROM DECISION WHERE TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('TOMAR') AND ID_CARGO = cargo.id) tomar,
(SELECT count(*) FROM DECISION WHERE TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('PROPONER') AND ID_CARGO = cargo.id) proponer,
(SELECT count(*) FROM RESPONSABILIDAD_CONTACTOS WHERE TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('INTERNAS') AND ID_CARGO = cargo.id) internas,
(SELECT count(*) FROM RESPONSABILIDAD_CONTACTOS WHERE TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('EXTERNAS') AND ID_CARGO = cargo.id) externas,
(SELECT COUNT(*) FROM CARGOS_REPORTAN WHERE ID_CARGO = cargo.id) reportan,
vigencia
from cargo

  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_SGI_NIVELES" ("ID", "NOMBRE") AS 
  SELECT "ID","NOMBRE" FROM SGI_NOMBRE WHERE ID IN (
SELECT NOMBRE FROM SGI_VERSION WHERE TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('GENERALES'))

