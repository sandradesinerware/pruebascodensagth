--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package DEPLOY_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."DEPLOY_PCK" AS 
--------------------------------------------------------------------------------
 -- CUIDADO CONSTANTE DE PRODUCCIÓN !!!!
 --SERVER     CONSTANT VARCHAR2(50) := 'https://grupoenelcolombia.com';
 SERVER   CONSTANT VARCHAR2(50) := 'http://54.164.83.200';
-------------------------------------------------------------------------------- 
 reports_internal constant varchar2(200) := 'http://54.164.83.200/AutomaticReports2/ReportsServlet';
 tomcat_user constant varchar2(200) := 'snw_user';
 tomcat_password  constant varchar2(200) := 'yENk.keP8sVtPh(ej}6.w=/J8#tSP5'; 
--------------------------------------------------------------------------------
 SERVER_ORDS_CONTEXT CONSTANT VARCHAR2(20) := '/ords/co/'; 
--------------------------------------------------------------------------------
 FOLDER_REPORT CONSTANT VARCHAR2(20) := 'codensa'; 
--------------------------------------------------------------------------------
 FUNCTION OBTENER_SERVER RETURN VARCHAR2;
--------------------------------------------------------------------------------
END DEPLOY_PCK;

/

  GRANT EXECUTE ON "CODENSA_GTH"."DEPLOY_PCK" TO "SNW_CARGOS";
