--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package EMAIL_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."EMAIL_PCK" AS 
  
    procedure enviar_correo(
            receiver    IN VARCHAR2,
      msg         IN VARCHAR2,
      subject     IN VARCHAR2 );
     
END EMAIL_PCK;

/
