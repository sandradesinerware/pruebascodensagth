--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body ORDS_MODULE_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."ORDS_MODULE_PCK" AS
--------------------------------------------------------
procedure printer_module AS
l_blob blob;
l_query varchar2(4000);
l_response restful_pck.response;
l_url varchar2(10000);
l_exception boolean:=false;

begin
 l_query :=  UTL_URL.ESCAPE(owa_util.get_cgi_env('QUERY_STRING'));
 if length(l_query) > 0 then
    l_url:= SNW_REPORTES.INTERNAL_URL||'?'||l_query;
 else
    l_url:= SNW_REPORTES.INTERNAL_URL;
 end if;

    begin
        l_response := restful_pck.make_request(i_url => l_url, i_fetch_blob=> true);
    exception when others then
        htp.p(SQLCODE||' -ERROR- '||SQLERRM);
        l_exception:= true;
    end;

 if not l_exception then
    owa_util.status_line(nstatus=>l_response.status_code, bclose_header=>false);
    if l_response.headers.count>0 then
        for i in l_response.headers.first..l_response.headers.last loop
        if lower(l_response.headers(i).name) = 'content-type' then
            OWA_UTIL.mime_header(l_response.headers(i).value, false);
        elsif lower(l_response.headers(i).name) = 'content-disposition' then
            htp.p(l_response.headers(i).name||': '||l_response.headers(i).value);
        end if;
        end loop;

    end if;
    owa_util.http_header_close;
    wpg_docload.download_file(l_response.blob_body);
 end if;
 exception when others then
         htp.p(SQLCODE||' -ERROR2- '||SQLERRM);
end;
--------------------------------------------------------
END ORDS_MODULE_PCK;

/
