--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package ADMIN_ASISTENCIA_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."ADMIN_ASISTENCIA_PCK" AS 

--------------------------------------------
FUNCTION ASISTENCIAS_SESIONES (i_id_pdi IN NUMBER) RETURN NUMBER;
--------------------------------------------
PROCEDURE CREAR_ASISTENCIA(i_id_pdi_sesion in number);
--------------------------------------------
PROCEDURE DEFINIR_ASISTENCIA(i_id_bars_pdi in number,i_id_pdi_sesion in number,i_asistio in varchar2);

END ADMIN_ASISTENCIA_PCK;


/
