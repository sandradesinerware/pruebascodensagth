--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package UTILS_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."UTILS_PCK" AS 

--------------------------------------------
FUNCTION CREAR_SELECT_LIST_ESTADO(i_id_pdi IN NUMBER) RETURN CLOB;

FUNCTION SET_PARENT_NAME(I_ID_PAGE IN NUMBER) RETURN VARCHAR2;

FUNCTION BUILD_URL_GROUP(I_ID_GROUP IN NUMBER) RETURN VARCHAR2;

FUNCTION Current_Time_Stamp_To_Secs RETURN NUMBER;

FUNCTION Current_x_amz_date RETURN VARCHAR2;
--------------------------------------------

END UTILS_PCK;

/
