--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body PARAMETRIZACION_CLIENTE
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."PARAMETRIZACION_CLIENTE" AS

  procedure sincronizar_agentes AS
  l_id number;
  BEGIN
    for i in (select identificacion from trabajador) loop
    begin
        insert into agente (numero_documento) values (i.identificacion);
       EXCEPTION when OTHERS then
          DBMS_OUTPUT.PUT_LINE(SQLERRM||'  -  :'||i.identificacion);
      end;
    end loop;
    for i in (select nit from contratista) loop
    begin
        insert into agente (numero_documento) values (i.nit);
     EXCEPTION when OTHERS then
          DBMS_OUTPUT.PUT_LINE(SQLERRM||'  -   :'||i.nit);
      end;
    end loop;
  END sincronizar_agentes;
    ------------------------------------------------------------------------
  procedure parametrizacion_servicios as
  l_conteo number;
  l_tipo number;
  l_division number;
  l_log varchar2(4000);
  begin
    --Actualización de nuevas gerencias
    for i in (select * from temp_servicio) loop
      begin
        select count(*) into l_conteo from dependencia where upper(nombre) = upper(i.gerencia) 
        and nivel = SNW_CONSTANTES.constante_tipo('DEP_GERENCIA');
        if l_conteo = 0 then
            insert into dependencia (nombre,estado,tipo,nivel) values
            (i.gerencia,'A',1,SNW_CONSTANTES.constante_tipo('DEP_GERENCIA'));
        end if;
        l_conteo := -1;
              EXCEPTION when OTHERS then
                 l_log := SQLERRM||'  - gerencia  :'||i.gerencia;
                 UPDATE temp_servicio SET LOG=LOG||' - '||l_log where gerencia = i.gerencia;
      end;
    end loop;
    --Actualización de nuevas subgerencias
    for i in (select * from temp_servicio) loop
    begin
        l_tipo := PARAMETRIZACION_CLIENTE.buscar_tipo
        (i.gerencia,i.subgerencia,i.division,'',SNW_CONSTANTES.constante_tipo('DEP_GERENCIA'));
        select count(*) into l_conteo from dependencia where upper(nombre) like upper(i.subgerencia) 
        and nivel = SNW_CONSTANTES.constante_tipo('DEP_SUBGERENCIA') and tipo = l_tipo;
        if l_conteo = 0 then
            insert into dependencia (nombre,estado,tipo,nivel) values
            (i.subgerencia,'A',l_tipo,SNW_CONSTANTES.constante_tipo('DEP_SUBGERENCIA'));
        end if;
        l_conteo := -1;
        EXCEPTION when OTHERS then
            l_log := SQLERRM||'  - subgerencia  :'||i.subgerencia;
            UPDATE temp_servicio SET LOG=LOG||' - '||l_log where subgerencia = i.subgerencia;
      end;
    end loop;
    --Actualización de nuevas divisiones
    for i in (select * from temp_servicio) loop
    begin
        l_tipo := PARAMETRIZACION_CLIENTE.buscar_tipo
        (i.gerencia,i.subgerencia,i.division,'',SNW_CONSTANTES.constante_tipo('DEP_SUBGERENCIA'));
        select count(*) into l_conteo from dependencia where upper(nombre) = upper(i.division) and 
        nivel = SNW_CONSTANTES.constante_tipo('DEP_DIVISION') and tipo = l_tipo;
        if l_conteo = 0 then
            insert into dependencia (nombre,estado,tipo,nivel) values
            (i.division,'A',l_tipo,SNW_CONSTANTES.constante_tipo('DEP_DIVISION'));
        end if;
        l_conteo := -1;
        EXCEPTION when OTHERS then
          l_log := SQLERRM||'  - division  :'||i.division;
          UPDATE temp_servicio SET LOG=LOG||' - '||l_log where division = i.division;
      end;
    end loop;
    --Actualizcion servicios
    for i in (select * from temp_servicio) loop
    begin
        l_tipo := PARAMETRIZACION_CLIENTE.buscar_tipo
        (i.gerencia,i.subgerencia,i.division,'',SNW_CONSTANTES.constante_tipo('DEP_DIVISION'));
        select count(*) into l_conteo from servicio where upper(codigo) = upper(i.codigo);
        if l_conteo = 0 then
            insert into servicio (nombre,division,codigo) values (i.servicio,l_tipo,i.codigo);
        else
            update servicio set nombre = i.servicio, division=l_tipo where upper(codigo) like upper(i.codigo);
        end if;
        l_conteo := -1;
        EXCEPTION when OTHERS then
          l_log := SQLERRM||'  - servicio  :'||i.servicio;
          UPDATE temp_servicio SET LOG=LOG||' - '||l_log where servicio = i.servicio;
      end;
    end loop;
  end parametrizacion_servicios;
      ------------------------------------------------------------------------
    procedure parametrizacion_dependencias AS
    l_conteo number;
    l_tipo number;
    l_nombre_tipo varchar(4000);
    l_log varchar2(4000);
    BEGIN
    --Actualización de nuevas gerencias
    for i in (select distinct(gerencia) from temp_cliente) loop
    begin
        select count(*) into l_conteo from dependencia where (nombre) like (i.gerencia) and nivel = SNW_CONSTANTES.constante_tipo('DEP_GERENCIA');
        if l_conteo = 0 then
            insert into dependencia (nombre,estado,tipo,nivel) values
            (i.gerencia,'A',1,SNW_CONSTANTES.constante_tipo('DEP_GERENCIA'));
        end if;
        l_conteo := -1;
        EXCEPTION when OTHERS then
          l_log := SQLERRM||'  - gerencia  :'||i.gerencia;
          UPDATE temp_cliente SET LOG=LOG||' - '||l_log where gerencia = i.gerencia;
      end;
    end loop;
    --Actualización de nuevas subgerencias
    l_conteo := -1;
    for i in (select * from temp_cliente) loop
        begin
        l_tipo := PARAMETRIZACION_CLIENTE.buscar_tipo
        (i.gerencia,i.subgerencia,i.division,i.departamento,SNW_CONSTANTES.constante_tipo('DEP_GERENCIA'));
        select count(*) into l_conteo from dependencia 
        where (nombre) like (i.subgerencia) and nivel = SNW_CONSTANTES.constante_tipo('DEP_SUBGERENCIA') and tipo = l_tipo;
        if l_conteo = 0 then 
            insert into dependencia (nombre,estado,tipo,nivel) values
            (i.subgerencia,'A',l_tipo,SNW_CONSTANTES.constante_tipo('DEP_SUBGERENCIA'));
        end if;
      l_conteo := -1;
        EXCEPTION when OTHERS then
            l_log := SQLERRM||'  - subgerencia  :'||i.subgerencia;
            UPDATE temp_cliente SET LOG=LOG||' - '||l_log where subgerencia = i.subgerencia;
      end;
    end loop;
    --Actualización de nuevas division
    l_conteo := -1;
    for i in (select * from temp_cliente) loop
        begin
        l_tipo := PARAMETRIZACION_CLIENTE.buscar_tipo
        (i.gerencia,i.subgerencia,i.division,i.departamento,SNW_CONSTANTES.constante_tipo('DEP_SUBGERENCIA')); 
        select count(*) into l_conteo from dependencia 
        where upper(nombre) like upper(i.division) and 
        nivel = SNW_CONSTANTES.constante_tipo('DEP_DIVISION') and tipo = l_tipo;
        if l_conteo = 0 then
            insert into dependencia (nombre,estado,tipo,nivel) values
            (i.division,'A',l_tipo,SNW_CONSTANTES.constante_tipo('DEP_DIVISION'));
        end if;
      l_conteo := -1;
        EXCEPTION when OTHERS then
          l_log := SQLERRM||'  - division  :'||i.division;
          UPDATE temp_cliente SET LOG=LOG||' - '||l_log where division = i.division;
      end;
    end loop;
    --Actualización de nuevas departamento
    l_conteo := -1;
    for i in (select * from temp_cliente) loop
    begin
        l_tipo := PARAMETRIZACION_CLIENTE.buscar_tipo
        (i.gerencia,i.subgerencia,i.division,i.departamento,SNW_CONSTANTES.constante_tipo('DEP_DIVISION')); 
        select count(*) into l_conteo from dependencia where upper(nombre) = upper(i.departamento) and 
        nivel = SNW_CONSTANTES.constante_tipo('DEP_DEPARTAMENTO') and tipo = l_tipo;
        if l_conteo = 0 then
            insert into dependencia (nombre,estado,tipo,nivel) values
            (i.departamento,'A',l_tipo,SNW_CONSTANTES.constante_tipo('DEP_DEPARTAMENTO'));
        end if;
        l_conteo := -1;
      EXCEPTION when OTHERS then
          l_log := SQLERRM||'  - departamento  :'||i.departamento;
          UPDATE temp_cliente SET LOG=LOG||' - '||l_log where departamento = i.departamento;
      end;
    end loop;
    END parametrizacion_dependencias;
    ------------------------------------------------------------------------
    procedure parametrizacion_contratistas AS
    l_conteo number;
    l_departamento number;
    l_clase number;
    l_error varchar2(2000);
    l_tipo number;
    l_id_usuario number;
    BEGIN
     for i in (select * from temp_cliente where upper(empresa_interna) = 'CONTRATISTA') loop
     begin
        select count(*) into l_conteo from contratista where nit = i.numero_identificacion;
        l_tipo := PARAMETRIZACION_CLIENTE.buscar_tipo
        (i.gerencia,i.subgerencia,i.division,i.departamento,SNW_CONSTANTES.constante_tipo('DEP_DIVISION'));
        select max(id) into l_clase from tipo 
        where upper(nombre) = upper(i.clase);
        select id into l_departamento from dependencia 
        where upper(nombre) = upper(i.departamento) and nivel = SNW_CONSTANTES.constante_tipo('DEP_DEPARTAMENTO') and
        tipo = l_tipo;
        if l_conteo = 0 then
            insert into contratista 
            (nit,nombre,email,clase,id_departamento,ubicacion,nombre_superior,empresa) 
            values (i.numero_identificacion,nvl(i.nombre_cliente,''),nvl(i.correo,''),nvl(l_clase,''),
            nvl(l_departamento,''),nvl(i.ubicacion,''),nvl(i.nombre_superior,''),nvl(i.empresa,''));
            SNW_GESTION_USRS.snw_create_user_contratista(to_number(i.numero_identificacion));
            select id into l_id_usuario from usuario where numero_identificacion = i.numero_identificacion;
            AUTORIZACION.ASIGNAR_GRUPO(l_id_usuario,17);
        else
            update contratista 
            set 
                nombre = i.nombre_cliente,
                email = i.correo,
                clase = l_clase,
                id_departamento= l_departamento,
                ubicacion = i.ubicacion,
                nombre_superior = i.nombre_superior,
                empresa = i.empresa            
            where nit = i.numero_identificacion;
        end if;
        l_conteo := -1;
        EXCEPTION when OTHERS then
          l_error := SQLERRM || ' contratista ';
          UPDATE temp_cliente SET LOG=LOG||' - '||l_error where numero_identificacion = i.numero_identificacion;
      end;
     end loop;
    END parametrizacion_contratistas;
    ------------------------------------------------------------------------
  procedure parametrizacion_trabajadores AS
    l_conteo number;
    l_departamento number;
    l_tipo number;
    l_error varchar(4000);
    BEGIN
     for i in (select * from temp_cliente where upper(empresa_interna) <> 'CONTRATISTA') loop
        begin
            select count(*) into l_conteo from trabajador where identificacion = i.numero_identificacion;
            if l_conteo > 0 then
              l_tipo := PARAMETRIZACION_CLIENTE.buscar_tipo
              (i.gerencia,i.subgerencia,i.division,i.departamento,SNW_CONSTANTES.constante_tipo('DEP_DIVISION'));
              select id into l_departamento from dependencia 
              where upper(nombre) = upper(i.departamento) and nivel = SNW_CONSTANTES.constante_tipo('DEP_DEPARTAMENTO')
              and tipo = l_tipo;
              if l_departamento is null then
                update temp_cliente set log = 'No se encontró el dept. '||i.departamento 
                where numero_identificacion = i.numero_identificacion;
              end if;
              update trabajador set id_departamento =  l_departamento where identificacion = i.numero_identificacion;
            end if;
            l_conteo := -1;
            EXCEPTION when OTHERS then
              l_error := SQLERRM || ' trabajador ';
              UPDATE temp_cliente SET LOG=LOG||' - '||l_error where numero_identificacion = i.numero_identificacion;
        end;
     end loop;
  END parametrizacion_trabajadores;
    ------------------------------------------------------------------------
  procedure parametrizacion_evaluaciones(i_id_periodo in number) AS
  l_evaluador number;
  l_servicio number;
  l_error varchar2(2000);
  l_conteo number;
  BEGIN
    for i in (select * from temp_evaluacion) loop
    begin
        select id into l_servicio from servicio where upper(codigo) = upper(i.codigo);
        select numero_documento into l_evaluador from agente where numero_documento = i.identificacion;
        select count(*) into l_conteo from evaluacion where servicio = l_servicio and evaluador = l_evaluador and periodo= i_id_periodo;
        if l_evaluador is not null and l_servicio is not null and l_conteo = 0 then
            insert into evaluacion (servicio,evaluador,estado,aplica,periodo,tipo,porcentaje) 
            values (l_servicio,l_evaluador,35,1,i_id_periodo,SNW_CONSTANTES.constante_tipo('PLANEADO'),0);
        end if;
      EXCEPTION when OTHERS then
           l_error := SQLERRM;
           update temp_evaluacion set log = l_error where upper(codigo) = upper(i.codigo) and identificacion =i.identificacion;

      end;
    end loop;
  END parametrizacion_evaluaciones;
  ------------------------------------------------------------------------
  function buscar_tipo(i_gerencia in varchar2, 
                        i_subgerencia in varchar2, 
                        i_division in varchar2, 
                        i_departamento in varchar2, 
                        i_nivel in number) return number is
    l_tipo number;
  begin
    if i_nivel = SNW_CONSTANTES.constante_tipo('DEP_GERENCIA') then
        select id into l_tipo from dependencia 
        where upper(nombre) = upper(i_gerencia) and nivel = i_nivel and tipo= 1;
        return l_tipo;
    end if;
    if i_nivel = SNW_CONSTANTES.constante_tipo('DEP_SUBGERENCIA') then
        select id into l_tipo from dependencia 
        where upper(nombre) = upper(i_subgerencia) and nivel = i_nivel 
        and tipo = (select id from dependencia where upper(nombre) = upper(i_gerencia) and tipo= 1);
        return l_tipo;
    end if;
    if i_nivel = SNW_CONSTANTES.constante_tipo('DEP_DIVISION') then
        select id into l_tipo from dependencia 
        where upper(nombre) = upper(i_division) and nivel = i_nivel and tipo = (
        select  id from dependencia where upper(nombre) = upper(i_subgerencia)  
        and tipo = (select id from dependencia where upper(nombre) = upper(i_gerencia) and tipo= 1));
        return l_tipo;
    end if;
    if i_nivel = SNW_CONSTANTES.constante_tipo('DEP_DEPARTAMENTO') then
        select id into l_tipo from dependencia 
        where upper(nombre) = upper(i_departamento) and nivel = i_nivel and
        tipo = (select id from dependencia where upper(nombre) = upper(i_division) 
        and nivel = i_nivel and tipo = (
        select  id from dependencia where upper(nombre) = upper(i_subgerencia)  
        and tipo = (select id from dependencia where upper(nombre) = upper(i_gerencia) and tipo= 1)));
        return l_tipo;
    end if;
    return 0;
    end;
END PARAMETRIZACION_CLIENTE;


/
