--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package OBJETIVOS_FLUJO_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."OBJETIVOS_FLUJO_PCK" AS 

--Definición de constantes que describen los casos de uso a validar (directamente relacionados con los requests en forms)
  k_grabable            CONSTANT VARCHAR2(30) := 'GRABABLE';--Asociado al grabar objetivos que los crea y actualiza
  k_grabable_validable  CONSTANT VARCHAR2(30) := 'GRABABLE_VALIDABLE';--Asociado al grabar y validar objetivos
  k_grabable_aprobable  CONSTANT VARCHAR2(30) := 'GRABABLE_APROBABLE';--Asociado al grabar y aprobar objetivos
  k_grabable_resultados  CONSTANT VARCHAR2(30) := 'GRABABLE_RESULTADOS';--Asociado al grabar los resultados de los objetivos
  k_grabable_res_evaluable  CONSTANT VARCHAR2(30) := 'GRABABLE_RES_EVALUABLE';--Asociado al grabar y aprobar los resultados de los objetivos  
  

--Definición de constantes con los nombres de los estados de objetivos
  k_nuevo       CONSTANT VARCHAR2(30) := 'NUEVO';
  k_et          CONSTANT VARCHAR2(30) := 'EN TRATAMIENTO';
  k_conval      CONSTANT VARCHAR2(30) := 'CONCERTACION VALIDADA';
  k_conap       CONSTANT VARCHAR2(30) := 'CONCERTACION APROBADA';
  k_peraj       CONSTANT VARCHAR2(30) := 'PERIODO AJUSTE';
  k_resval      CONSTANT VARCHAR2(30) := 'RESULTADO VALIDADO';
  k_finalizado  CONSTANT VARCHAR2(30) := 'FINALIZADO';
  k_anulado     CONSTANT VARCHAR2(30) := 'ANULADO';

--Definición de constantes para consulta de la autorización
  k_funcion_admin   CONSTANT VARCHAR2(30) := 'Administrar OPR';
  k_aplicacion_auth CONSTANT VARCHAR2(30) := 'GTH';

--Condicionales de elementos de aplicación de acuerdo al estado, usuario, fecha y periodo
FUNCTION ES_GRABABLE(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN;
FUNCTION ES_GRABABLE_VALIDABLE(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN;
FUNCTION ES_GRABABLE_APROBABLE(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN;
FUNCTION ES_GRABABLE_RES(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN;
FUNCTION ES_GRABABLE_RES_EVALUABLE(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN;
FUNCTION ES_FINALIZABLE(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN;
  
--Subprogramas encargados de la gestión del flujo
PROCEDURE GRABAR_OBJETIVOS (i_formevaobj_id IN NUMBER, o_msj_error OUT VARCHAR2, o_msj_exito OUT VARCHAR2);
PROCEDURE GRABAR_RESULTADOS (i_formevaobj_id IN NUMBER, o_msj_error OUT VARCHAR2, o_msj_exito OUT VARCHAR2);
PROCEDURE VALIDAR_EVALUACION (i_formevaobj_id IN NUMBER, o_msj_error OUT VARCHAR2, o_msj_exito OUT VARCHAR2);

--Utilitarios
FUNCTION es_periodo_activo(i_formevaobj_id IN NUMBER) RETURN BOOLEAN;
FUNCTION es_valido_resultados(i_objetivo IN BARS_OBJETIVO.id%type, i_resultado IN BARS_OBJETIVO.id%type) RETURN VARCHAR2;

-- Getters (Obtener data)
FUNCTION get_asignado_jefe(i_asignado_trabajador IN NUMBER) RETURN NUMBER;
FUNCTION get_asignado_username(i_user_name IN VARCHAR2) RETURN NUMBER;
FUNCTION get_estado (i_formevaobj_id IN NUMBER) RETURN VARCHAR2;

-- Setters (Actualizar data)
PROCEDURE set_estado (i_formevaobj_id IN NUMBER, i_estado IN VARCHAR2);

END OBJETIVOS_FLUJO_PCK;

/
