--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body ADMIN_OBJETIVOS_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."ADMIN_OBJETIVOS_PCK" AS

function periodo_activo_default return number AS
    L_ULTIMO_PERIODO BARS_PEROBJ.ano%TYPE;
  BEGIN

    select  BARS_PEROBJ.ano INTO L_ULTIMO_PERIODO from BARS_PEROBJ 
    WHERE ESTADO = 1 AND ROWNUM = 1 ORDER BY ANO DESC;
        RETURN L_ULTIMO_PERIODO;
    exception when NO_DATA_FOUND then 
         return NULL; 

END periodo_activo_default;

procedure calidatos_corregir_aprobadores(i_periodo BARS_FORMEVAOBJ.periodo%type,
    i_aprobador_incorrecto TRABAJADOR.numero_identificacion%type) AS
  CURSOR l_formularios_corregir IS
    select id, estado, evaluado, evaluador, evaluador_resultado from bars_formevaobj 
      where periodo = i_periodo AND (evaluador = i_aprobador_incorrecto OR 
      evaluador_resultado = i_aprobador_incorrecto);
  l_gestor_vigente TRABAJADOR.numero_identificacion%type;
  l_new_evaluador_resultado TRABAJADOR.numero_identificacion%type := '';
  l_new_evaluador TRABAJADOR.numero_identificacion%type := '';

  BEGIN
    -- Este procedimiento corrige el registro expl�cito de gestor concertador (evaluador)
    -- o gestor evaluador del resultado, cuando �ste es realizado por el administrador o soporte,
    -- registrando el gestor vigente, o cuando qued� nulo err�neamente.
    -- Tener presente que no corrige auditor�a por tanto se actualizar� la columna de actualizaci�n de la fila
    FOR i in l_formularios_corregir LOOP
        select jefe into l_gestor_vigente from TRABAJADOR where numero_identificacion = i.evaluado;
        --Determinar evaluador resultado a corregir
        if ( i.evaluador_resultado = i_aprobador_incorrecto OR
            (i.evaluador_resultado is null and i.estado in (12/*resultado validado*/, 14/*finalizado*/)) ) then 
            l_new_evaluador_resultado := l_gestor_vigente;
        end if;
        --Determinar evaluador (concertador) a corregir
        if ( i.evaluador = i_aprobador_incorrecto OR
            (i.evaluador is null and i.estado in (11/*concertaci�n aprobada*/, 12/*resultado validado*/, 14/*finalizado*/)) ) then 
            l_new_evaluador := l_gestor_vigente;
        end if;
        --Persistir
        if ( l_new_evaluador IS NOT NULL and l_new_evaluador_resultado IS NOT NULL ) then
            UPDATE BARS_FORMEVAOBJ SET evaluador_resultado=l_new_evaluador_resultado, 
                evaluador=l_new_evaluador WHERE id=i.id;
        elsif ( l_new_evaluador_resultado IS NOT NULL ) then
            UPDATE BARS_FORMEVAOBJ SET evaluador_resultado=l_new_evaluador_resultado WHERE id=i.id;
        elsif ( l_new_evaluador IS NOT NULL ) then
            UPDATE BARS_FORMEVAOBJ SET evaluador=l_new_evaluador WHERE id=i.id;
        end if;
    END LOOP;
END calidatos_corregir_aprobadores;

procedure calidatos_corregir_concertador(i_periodo BARS_FORMEVAOBJ.periodo%type) AS
  CURSOR l_formularios_corregir IS
    select id, estado, evaluado, evaluador from bars_formevaobj
      where periodo = i_periodo AND estado in (6/*NUEVO*/,7/*EN TRATAMIENTO*/,8/*CONCERTACION VALIDADA*/);
  l_gestor_vigente TRABAJADOR.jefe%type;

BEGIN
  --Este procedimiento actualiza el gestor concertador cuando el formulario de objetivos
  --se crea antes de tener el gestor actualizado
  FOR i in l_formularios_corregir LOOP
    --Corregir el evaluador (concertador) del formulario de objetivos porque se cre� con el desactualizado
    select jefe into l_gestor_vigente from TRABAJADOR where numero_identificacion = i.evaluado;
    if ( i.evaluador != l_gestor_vigente ) then
      UPDATE BARS_FORMEVAOBJ SET evaluador=l_gestor_vigente WHERE id=i.id;
    end if;
  END LOOP;

END calidatos_corregir_concertador;


function eliminar_concertacion_obj(i_periodo BARS_FORMEVAOBJ.periodo%type,
  i_evaluado BARS_FORMEVAOBJ.evaluado%type, o_mensaje_error OUT VARCHAR2) return boolean AS

l_formulario BARS_FORMEVAOBJ.id%type;

BEGIN
  delete from BARS_TRAEVAOBJ
    where trabajador = i_evaluado and bars_perobj = i_periodo;
  select id into l_formulario
    from BARS_FORMEVAOBJ
    where evaluado = i_evaluado and periodo = i_periodo;
  delete from BARS_OBJETIVO 
    where formevaobj = l_formulario;
  delete from BARS_COMEVAOBJ 
    where formeva_obj = l_formulario;
  delete from BARS_FORMEVAOBJ
    where id = l_formulario;
  return true;

  EXCEPTION
    WHEN TOO_MANY_ROWS THEN
      o_mensaje_error := 'No se puede eliminar. El evaluado tiene m�s de un formulario en el periodo.';
      return false;
    WHEN NO_DATA_FOUND THEN
      o_mensaje_error := 'No se puede eliminar. El evaluado no tiene formulario en el periodo.';
      return false;
    WHEN OTHERS THEN
      o_mensaje_error := 'No se puede eliminar. Ocurri� un problema no manejado en la eliminaci�n.';
      return false;
END eliminar_concertacion_obj;


procedure autorizacion_masiva_objetivos(i_periodo BARS_FORMEVAOBJ.periodo%type,
  i_target_opr TRABAJADOR.id_target_opr%type) AS
  BEGIN
    --Este procedimiento adiciona a los trabajadores autorizados a tener objetivos en un periodo dado
    --el conjunto de trabajadores que cumplen el target recibido
    INSERT INTO BARS_TRAEVAOBJ(bars_perobj, trabajador)
      select i_periodo, numero_identificacion
        from TRABAJADOR
        where id_target_opr = i_target_opr AND id_estado = 1/*ACTIVO*/
      minus
        select i_periodo, trabajador
          from BARS_TRAEVAOBJ
          where bars_perobj = i_periodo;

END autorizacion_masiva_objetivos;

procedure duplicar_param_periodo(i_periodo_destino BARS_PEROBJ.ano%type,
  i_periodo_origen BARS_PEROBJ.ano%type) AS
  BEGIN
    --Este procedimiento duplica las parametrizaciones que controlan la concertaci�n y evaluaci�n de objetivos.
    --Parte de la premisa que el trigger BI de las tablas crean la primaria

    --Duplicar BARS_POSICION
    insert into BARS_POSICION(nombre, pct_inf, pct_sup, periodo, tipo_id)
        select nombre, pct_inf, pct_sup, i_periodo_destino, tipo_id
          from BARS_POSICION
          where periodo = i_periodo_origen
          minus
            select nombre, pct_inf, pct_sup, periodo, tipo_id
              from BARS_POSICION
              where periodo = i_periodo_destino;

    --Duplicar RESTRICCION_TARGET
    insert into RESTRICCION_TARGET(id_target, id_tipo_evaluacion, porcentaje_total, pct_ind_min, pct_ind_max, periodo)
        select id_target, id_tipo_evaluacion, porcentaje_total, pct_ind_min, pct_ind_max, i_periodo_destino
          from RESTRICCION_TARGET
          where periodo = i_periodo_origen
          minus
            select id_target, id_tipo_evaluacion, porcentaje_total, pct_ind_min, pct_ind_max, periodo
              from RESTRICCION_TARGET
              where periodo = i_periodo_destino;

    /* ! ! ! Se elimina: https://sinerware.atlassian.net/browse/GTH2019-81 ! ! ! 
    
    --Duplicar PCT_CONSECUCION_BARS
    insert into PCT_CONSECUCION_BARS(pct_consecucion, media_comp_inf, media_comp_sup, periodo, tipo_id)
        select pct_consecucion, media_comp_inf, media_comp_sup, i_periodo_destino, tipo_id
          from PCT_CONSECUCION_BARS
          where periodo = i_periodo_origen
          minus
            select pct_consecucion, media_comp_inf, media_comp_sup, periodo, tipo_id
              from PCT_CONSECUCION_BARS
              where periodo = i_periodo_destino;
              
    */

END duplicar_param_periodo;

procedure alistar_periodo(i_periodo_destino BARS_PEROBJ.ano%type,
  i_periodo_origen BARS_PEROBJ.ano%type) AS

  cursor trabajadores_sin_usuario IS
    select numero_identificacion, identificacion
      from TRABAJADOR
      where id_estado = 1/*ACTIVO*/ and numero_identificacion not in
        (select numero_identificacion from USUARIO);

  cursor usuarios_activos IS
    select TRABAJADOR.numero_identificacion, CASE WHEN JEFES.numero_identificacion IS NOT NULL THEN 'S' ELSE 'N' END es_jefe, USUARIO.id usuario_id
      from TRABAJADOR left join 
        (select distinct(jefe) numero_identificacion 
           from trabajador where jefe in (select numero_identificacion from TRABAJADOR where id_estado = 1/*ACTIVO*/)
        )JEFES on TRABAJADOR.numero_identificacion = JEFES.numero_identificacion
        left join USUARIO on (TRABAJADOR.numero_identificacion = USUARIO.numero_identificacion)
      where TRABAJADOR.id_estado = 1/*ACTIVO*/ and USUARIO.numero_identificacion is not null;

    l_grupo_evaluado NUMBER := 15;
    l_grupo_gestor NUMBER := 14;

  BEGIN
    --Este procedimiento alista el periodo para objetivos de la siguiente manera:
    --Crea usuarios para los trabajadores activos que no lo tienen (SNW_GESTION_USUARIOS.snw_create_user)
    --Crea permisos como evaluado y gestor  (SNW_AUTORIZACION.ASIGNAR_GRUPO(I_USER_NRO_IDENTIFICACION NUMBER,I_GRUPO NUMBER)
    --Si concertaci�n existe y est� antes de concertaci�n aprobada, actualizar el concertador del formulario
    --Autorizar masivamente traevaobj (id_target_opr)
    --Parametrizaciones del periodo basadas en periodo de referencia

    --Crear usuarios a trabajadores activos que no lo tienen
    FOR c in trabajadores_sin_usuario LOOP
        BEGIN
            AUTH_SNW_GESTION_USUARIOS.snw_create_user(c.numero_identificacion); --TODO: Manejar excepciones de snw_create_user (uniques de numero_identificacion, username, y caso contratista-trabajador)
        EXCEPTION
            WHEN OTHERS THEN --fall� snw_gestion_usuarios.snw_create_user
                NULL;
        END;
    END LOOP;

    --Crea permisos evaluado y gestor
    FOR c in usuarios_activos LOOP
        BEGIN
            if ( NOT AUTH_SNW_AUTORIZACION.pertenece_usu_grupo(i_usuario=>c.usuario_id, i_grupo=>l_grupo_evaluado) ) then
              AUTH_SNW_AUTORIZACION.ASIGNAR_GRUPO(i_user_nro_identificacion=>c.usuario_id, i_grupo=>l_grupo_evaluado);
            end if;
            if ( c.es_jefe = 'S' and NOT AUTH_SNW_AUTORIZACION.pertenece_usu_grupo(i_usuario=>c.usuario_id, i_grupo=>l_grupo_gestor) ) then
              AUTH_SNW_AUTORIZACION.ASIGNAR_GRUPO(i_user_nro_identificacion=>c.usuario_id, i_grupo=>l_grupo_gestor);
            end if;
        EXCEPTION
            WHEN OTHERS THEN --Podr�a fallar AUTH_SNW_GESTION_AUTORIZACION
                NULL;
        END;
    END LOOP;

    --Si concertaci�n existe y est� antes de concertaci�n aprobada, actualizar el concertador del formulario
    calidatos_corregir_concertador(i_periodo=>i_periodo_destino);

    --Autorizar masivamente traevaobj (id_target_opr) TODO: hacer din�mico la elecci�n del target
    autorizacion_masiva_objetivos(i_periodo=>i_periodo_destino, i_target_opr=>1044/*CONV. EMGESA*/);

    --Parametrizaciones del periodo basadas en periodo de referencia
    duplicar_param_periodo(i_periodo_destino=>i_periodo_destino, i_periodo_origen=>i_periodo_origen);

END alistar_periodo;

function val_traevaobj(i_periodo IN BARS_PEROBJ.ano%type, i_trabajador IN TRABAJADOR.numero_identificacion%type,
  o_mensaje OUT VARCHAR2) RETURN BOOLEAN AS
  l_conteo NUMBER;
  BEGIN
    select count(*) into l_conteo
      from BARS_TRAEVAOBJ
      where trabajador = i_trabajador and bars_perobj = i_periodo;
    if l_conteo = 0 then 
       o_mensaje := 'El trabajador ' || i_trabajador || ' no est� habilitado para concertar objetivos en el periodo ' || i_periodo || '.';
       RETURN FALSE;
     else
       o_mensaje := 'El trabajador ' || i_trabajador || ' s� est� habilitado para concertar objetivos en el periodo ' || i_periodo || '.';
       RETURN TRUE;
     end if; 

END val_traevaobj;



function validar_resultados(i_objetivo IN BARS_OBJETIVO.id%type, i_resultado IN BARS_OBJETIVO.id%type) RETURN VARCHAR2 AS
  
  l_target NUMBER;
  l_periodo NUMBER;
  l_curva NUMBER;
  l_EVALUADO NUMBER;
  
  l_min NUMBER;
  l_max NUMBER;
  
  l_conteo NUMBER;
  
  BEGIN
  
    -- Se busca el registro en RESTRICCION_RESULTADOS_OPR que contenga la terna (target,curva,periodo) dado el objetivo i_objetivo
    select id_tipo_curva into l_curva from BARS_OBJETIVO where id = i_objetivo;
    
    select periodo into l_periodo from BARS_FORMEVAOBJ where id = (select FORMEVAOBJ from BARS_OBJETIVO where id = i_objetivo);
    
     -- Se obtiene el TARGET_OPR del trabajador (ya no del formulario)
    select EVALUADO into l_evaluado from bars_formevaobj where id = (select FORMEVAOBJ from BARS_OBJETIVO where id = i_objetivo);
    select ID_TARGET_OPR into l_target from trabajador where NUMERO_IDENTIFICACION = l_evaluado;
        
    
    -- Se verifica cuantos registros en la tabla param�trica
    select count(*) into l_conteo from RESTRICCION_RESULTADOS_OPR where periodo = l_periodo and tipo_target = l_target and tipo_curva = l_curva;
    
    
        
    -- Se encuentran dos o mas parametrizaciones para la misma terna (curva,target,periodo)  
    if l_conteo > 1  then
        return 'Existen dos o m�s parametrizaciones para este objetivo, por favor comunicarse con el administrador OPR';
    
    -- No se encontr� ninguna parametrizaci�n para la terna (curva,target,periodo)      
    elsif l_conteo = 0 then
        return 'No existe ninguna parametrizaci�n para este objetivo, por favor comunicarse con el administrador OPR';
        
    -- Existe una sola parametrizaci�n, se procede con validar el rango minimo y maximo de resultados    
    else 
    
        select RESULTADO_MINIMO into l_min from RESTRICCION_RESULTADOS_OPR where periodo = l_periodo and tipo_target = l_target and tipo_curva = l_curva;
        select RESULTADO_MAXIMO into l_max from RESTRICCION_RESULTADOS_OPR where periodo = l_periodo and tipo_target = l_target and tipo_curva = l_curva;
        
        if i_resultado < l_min or i_resultado > l_max then
            return 'El resultado no es v�lido. Debe estar en el rango: ' || l_min || ' - ' || l_max;
        end if;
        
        return null;
    end if;
    
END validar_resultados;


function eliminar_formevaobj(i_formulario IN BARS_FORMEVAOBJ.id%type, o_mensaje OUT VARCHAR2) RETURN BOOLEAN AS
  /*Eliminar un formulario de evaluaci�n de objetivos y su informaci�n relacionada.*/
  l_retorno number;
  l_mensaje VARCHAR2(4000);
  
BEGIN
  --Validar existencia de la concertaci�n  
  select count(*) into l_retorno from BARS_FORMEVAOBJ where id = i_formulario;
  if l_retorno = 0 then
    o_mensaje := 'No se puede eliminar la concertaci�n de objetivos porque no existe.';
    return FALSE;
  else
    --Validar estado de la concertaci�n
    BARS_VALESTFORMOBJ (i_formulario, 'FINALIZADO', l_retorno, l_mensaje);
    if l_retorno = 0 then
      DELETE BARS_OBJETIVO where formevaobj = i_formulario;
      DELETE BARS_COMEVAOBJ where formeva_obj = i_formulario;
      DELETE BARS_FORTEVAOBJ where formevaobj = i_formulario;
      DELETE BARS_FORMEVAOBJ where id = i_formulario;
      o_mensaje := 'Concertaci�n de objetivos eliminada.';
      RETURN TRUE;
    else
      o_mensaje := 'No se puede eliminar la concertaci�n de objetivos. '||l_mensaje;
      RETURN FALSE;
    end if;
  end if;

END eliminar_formevaobj;

function CALCULAR_PCTCONSABI(i_formevaobj IN BARS_FORMEVAOBJ.id%type) RETURN BOOLEAN AS
/*Calcular el porcentaje de consecuci�n promedio ponderado para los objetivos 
abiertos de un FORMEVAOBJ, transform�ndolo en base 100%, 
y actualizar el valor en la tabla.*/
l_pctconsabi_crudo NUMBER(5,2); --Porcentaje de consecuci�n promedio para objetivos abiertos seg�n el peso original
l_pctconsabi_base100 NUMBER(5,2); --Porcentaje de consecuci�n promedio para objetivos abiertos en base 100%
l_peso_abi NUMBER(5,2); --Peso en proporci�n de los objetivos abiertos en la evaluaci�n integral
l_periodo BARS_PEROBJ.ano%type;
l_evaluado BARS_FORMEVAOBJ.evaluado%type;
l_target_opr TIPO.id%type;

BEGIN
  --Calcular porcantaje consecuci�n ponderado para objetivos abiertos sin interpolaci�n
  select sum( (ponderacion/100)*pctconspond ) into l_pctconsabi_crudo
    from BARS_OBJETIVO
    where formevaobj = i_formevaobj
      and id_tipo = SNW_CONSTANTES.get_id_tipo('ABIERTO', 'TIPO_OBJETIVO');
  
  --Calcular peso de objetivos abiertos en la evaluaci�n del trabajador con su target actual
  select periodo, evaluado into l_periodo, l_evaluado
    from BARS_FORMEVAOBJ
    where id = i_formevaobj;
  select id_target_opr into l_target_opr
    from TRABAJADOR
    where numero_identificacion = l_evaluado;
  l_peso_abi := VALIDACIONES_GUI.calc_peso_requerido_evaluacion( l_periodo, l_target_opr,
    SNW_CONSTANTES.get_id_tipo('OPR_ABI', 'TIPO_EVALUACION_TARGET') );

  --Interpolaci�n de la base de peso objetivos abiertos a 100
  l_pctconsabi_base100 := l_pctconsabi_crudo * (1/l_peso_abi);
  UPDATE BARS_FORMEVAOBJ set pctconsabi = l_pctconsabi_base100
    where id = i_formevaobj;
  return TRUE;

EXCEPTION
  WHEN OTHERS THEN
    return FALSE;

END CALCULAR_PCTCONSABI;

END ADMIN_OBJETIVOS_PCK;

/
