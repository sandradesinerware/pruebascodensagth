--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body UTILS_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."UTILS_PCK" AS
  
  FUNCTION CREAR_SELECT_LIST_ESTADO(i_id_pdi IN NUMBER) RETURN CLOB AS
    L_SELECT_LIST         CLOB;
    L_SELECT_LIST_DEFAULT CLOB;
  BEGIN    

    SELECT APEX_ITEM.SELECT_LIST_FROM_QUERY(8,ID_ESTADO_PDI,'SELECT NOMBRE, ID FROM ESTADO_BARS WHERE ID IN (50,51)',NULL,'YES', 0, '-') 
      INTO L_SELECT_LIST FROM PDI_CONCEPTO where id_pdi = i_id_pdi and id_estado_pdi in(50,51);

    RETURN L_SELECT_LIST;

    EXCEPTION WHEN OTHERS THEN
      SELECT APEX_ITEM.SELECT_LIST(8,0,'-;0, No Aprobado;51, Aprobado;50') INTO L_SELECT_LIST_DEFAULT FROM DUAL;
      RETURN L_SELECT_LIST_DEFAULT;

  END CREAR_SELECT_LIST_ESTADO;  


  FUNCTION SET_PARENT_NAME(I_ID_PAGE IN NUMBER) RETURN VARCHAR2 AS
    BEGIN    
      CASE I_ID_PAGE
        WHEN 143 THEN RETURN 'Mis acciones';
        WHEN 144 THEN RETURN 'Acci�n de PDI';
        WHEN 145 THEN RETURN 'Concertar acciones de mis colaboradores';
        WHEN 149 THEN RETURN 'Acciones evaluadas';
        WHEN 155 THEN RETURN 'PDI';
        WHEN 1551 THEN RETURN 'Acciones concertadas/anuladas';
        WHEN 161 THEN RETURN 'Aprobar acciones de proyecto como l�der';
        WHEN 163 THEN RETURN 'Aprobar acciones de proyecto como miembro';        
        WHEN 164 THEN RETURN 'Aprobar acciones de formaci�n';        
        WHEN 165 THEN RETURN 'Aprobar acciones de estancias';
        WHEN 166 THEN RETURN 'Aprobar acciones de autogesti�n';
        WHEN 167 THEN RETURN 'Aprobar acciones de mentoring';
        ELSE RETURN 'UNKNOWN PARENT NAME';
      END CASE;
      RETURN 'UNKNOWN PARENT NAME';
      EXCEPTION WHEN OTHERS THEN
        RETURN 'UNKNOWN PARENT NAME';        
    END SET_PARENT_NAME;

  FUNCTION BUILD_URL_GROUP(I_ID_GROUP IN NUMBER) RETURN VARCHAR2 AS
    l_url_group         VARCHAR2(1000);
    l_default_url_group VARCHAR2(1000);
    l_id_periodo number;
    BEGIN    
    select id_periodo into l_id_periodo from pdi_grupo where id = I_ID_GROUP;
      l_url_group := DEPLOY_PCK.OBTENER_SERVER || CODENSA_GTH.DEPLOY_PCK.SERVER_ORDS_CONTEXT || 
                     'f?p=101:144:'||V('SESSION')||'::NO::P144_ID_PERIODO,P144_BANDERA,' 
                     || 'AI_PARENT_BRANCH,P144_ACCION,P144_ID_GRUPO:' ||l_id_periodo||',MI_PDI,143,57,' || I_ID_GROUP;

      /*
      l_url_group := deploy_pck.SERVER || deploy_pck.SERVER_ORDS_CONTEXT ||
                        APEX_UTIL.PREPARE_URL( 
                          p_url => 'f?p=101:144:::NO:144:P144_ID_PERIODO,P144_BANDERA,' ||
                                    'AI_PARENT_BRANCH,P144_ACCION,P144_ID_GRUPO:' ||
                                    '22,MI_PDI,143,57,' || I_ID_GROUP );
       */ 


      RETURN l_url_group;

      EXCEPTION WHEN OTHERS THEN
        l_default_url_group := 'http://34.201.51.0:8081/ords/' || 
                                  APEX_UTIL.PREPARE_URL(                                   
                                    p_url => 'f?p=101:155:::NO:::' );
        RETURN l_default_url_group;

    END BUILD_URL_GROUP;

    FUNCTION Current_Time_Stamp_To_Secs RETURN NUMBER AS
    BEGIN
          RETURN round((SYSDATE - date '1970-01-01')*24*60*60);
    END Current_Time_Stamp_To_Secs;

    FUNCTION Current_x_amz_date RETURN VARCHAR2 AS
      L_DATE_X_AMZ VARCHAR2(1000);
    BEGIN

      SELECT to_char(current_timestamp, 'YYYYMMDD')||'T' 
          || to_char(current_timestamp, 'HH24MISS')||'Z' 
      INTO L_DATE_X_AMZ 
      FROM DUAL;

      RETURN L_DATE_X_AMZ;

    END Current_x_amz_date;    



END UTILS_PCK;

/
