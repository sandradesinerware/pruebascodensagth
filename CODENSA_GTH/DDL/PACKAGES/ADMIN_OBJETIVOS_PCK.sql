--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package ADMIN_OBJETIVOS_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."ADMIN_OBJETIVOS_PCK" AS 

function periodo_activo_default return number;

procedure calidatos_corregir_aprobadores(i_periodo BARS_FORMEVAOBJ.periodo%type, 
  i_aprobador_incorrecto TRABAJADOR.numero_identificacion%type);

procedure calidatos_corregir_concertador(i_periodo BARS_FORMEVAOBJ.periodo%type);

function eliminar_concertacion_obj(i_periodo BARS_FORMEVAOBJ.periodo%type,
  i_evaluado BARS_FORMEVAOBJ.evaluado%type, o_mensaje_error OUT VARCHAR2) return boolean;

procedure autorizacion_masiva_objetivos(i_periodo BARS_FORMEVAOBJ.periodo%type,
  i_target_opr TRABAJADOR.id_target_opr%type);

procedure duplicar_param_periodo(i_periodo_destino BARS_PEROBJ.ano%type,
  i_periodo_origen BARS_PEROBJ.ano%type);

procedure alistar_periodo(i_periodo_destino BARS_PEROBJ.ano%type,
  i_periodo_origen BARS_PEROBJ.ano%type);

function val_traevaobj(i_periodo IN BARS_PEROBJ.ano%type, i_trabajador IN TRABAJADOR.numero_identificacion%type,
  o_mensaje OUT VARCHAR2) RETURN BOOLEAN;
  
function validar_resultados(i_objetivo IN BARS_OBJETIVO.id%type, i_resultado IN BARS_OBJETIVO.id%type) RETURN VARCHAR2;

function eliminar_formevaobj(i_formulario IN BARS_FORMEVAOBJ.id%type, o_mensaje OUT VARCHAR2) RETURN BOOLEAN;

function CALCULAR_PCTCONSABI(i_formevaobj IN BARS_FORMEVAOBJ.id%type) RETURN BOOLEAN;

END ADMIN_OBJETIVOS_PCK;

/
