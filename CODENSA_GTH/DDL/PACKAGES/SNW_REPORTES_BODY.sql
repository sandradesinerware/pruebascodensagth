--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body SNW_REPORTES
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."SNW_REPORTES" AS

  FUNCTION CONSTRUIR_URL(I_TIPO_REPORTE VARCHAR2,I_PARAMETROS VARCHAR2) RETURN VARCHAR2 AS
      l_reporte varchar2(4000);
  BEGIN
    if I_TIPO_REPORTE = 'ENCABEZADO_PDI' then 
        l_reporte := 'ENCABEZADO_PDI';
    end if;
    if I_TIPO_REPORTE = 'ENCABEZADO_OPR' then 
        l_reporte := 'encabezado_opr';
    end if;
    if I_TIPO_REPORTE = 'PDI_COLABORADOR' then 
        l_reporte := 'Avance_PDI_colaborador';
    end if;
    if I_TIPO_REPORTE = 'PDI_JEFE' then 
        l_reporte := 'Avance_PDI_jefe';
    end if;
    if I_TIPO_REPORTE = 'REPORTE_FORMACION' then 
        l_reporte := 'Reporte_formacion';
    end if;
    if I_TIPO_REPORTE = 'REPORTE_CARGO' then
        l_reporte := 'descripcion_cargo';
        RETURN L_URL||'?subfolder='||L_SUBFOLDER||'&reportName='||l_reporte||'&jndi='||'cargo'||'&'||I_PARAMETROS;
    end if;
    RETURN L_URL||'?subfolder='||L_SUBFOLDER||'&reportName='||l_reporte||'&jndi='||L_JNDI||'&'||I_PARAMETROS;
  END CONSTRUIR_URL;

END SNW_REPORTES;

/

  GRANT EXECUTE ON "CODENSA_GTH"."SNW_REPORTES" TO "SNW_CARGOS";
