--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package PERIODO_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."PERIODO_PCK" AS 

  function periodo_activo_default return number;
  
  function periodo_maximo_default return number;

  Procedure val_estado_periodo (i_ano      IN PERIODO_BARS.ano%type,
                                i_estado   IN ESTADO_BARS.NOMBRE%type,
                                o_return   OUT NUMBER, 
                                o_mensaje  OUT VARCHAR2);

  Procedure val_estado_periodo_feed (i_id_periodo      IN NUMBER,
                                i_estado   IN NUMBER,
                                o_return   OUT NUMBER, 
                                o_mensaje  OUT VARCHAR2);

END PERIODO_PCK;

/
