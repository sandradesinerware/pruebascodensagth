--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body DEPLOY_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."DEPLOY_PCK" as
--------------------------------------------------------------------------------
 function obtener_server return varchar2 as
 begin
  return server;
 end obtener_server;
--------------------------------------------------------------------------------
end deploy_pck;

/

  GRANT EXECUTE ON "CODENSA_GTH"."DEPLOY_PCK" TO "SNW_CARGOS";
