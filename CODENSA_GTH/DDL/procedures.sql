
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_ACTUALIZAR_CONSEC_CALIB" (idForm IN NUMBER) IS
    vAjusteCalibrado NUMBER;
    vTrabajador      NUMBER;
    vPeriodo         VARCHAR2(30);
    vPct             NUMBER;
    vIdMesa          NUMBER;
    vAjuste          NUMBER;
    vconteo_filas 	 NUMBER;
BEGIN

 select evaluado, periodo, pct_consecucion into vTrabajador,
vPeriodo, vPct from FORMULARIO_BARS where ID = idForm;

 --Verificar si el trabajador ya tiene mesa de homogeneizaci�n
 select count(*) into vconteo_filas
   from BARS_MESAHOM_TRABAJADOR bmt join BARS_MESAHOM bm on bmt.MESA = bm.ID
   where trabajador = vTrabajador and periodo = vPeriodo;
 if vconteo_filas != 0 then -- El trabajador tiene mesa de homogeneizaci�n
   select bm.ID, bmt.AJUSTE into vIdMesa, vAjuste from
     BARS_MESAHOM_TRABAJADOR bmt join BARS_MESAHOM bm on bmt.MESA = bm.ID
     where PERIODO = vPeriodo and TRABAJADOR = vTrabajador;

   update BARS_MESAHOM_TRABAJADOR set PCT_CALIBRADO =
     BARS_GET_NIVELES(vPct,vAjuste,vPeriodo) where TRABAJADOR = vTrabajador and MESA
     = vIdMesa and AJUSTE IS NOT NULL;
 end if;

EXCEPTION
WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en BARS_ACTUALIZAR_CONSEC_CALIB. ' || SQLCODE || ' - ' 
     || SQLERRM || ' ');
END;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_CAMESTFORMPRE" (pformulario IN BARS_FORMEVAPRE.ID%TYPE, pestado IN ESTADO_BARS.NOMBRE%TYPE)
is
 vconteo_filas NUMBER;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 vestado_anterior ESTADO_BARS.NOMBRE%TYPE;
 ktipo_estado_predictores CONSTANT VARCHAR2(30) := 'PREDICTORES_POTENCIAL';
BEGIN
  --Verificar que el formulario exista
  select count(*) into vconteo_filas
  from BARS_FORMEVAPRE
  where id = pformulario;
  if vconteo_filas = 0 then --formulario inexistente
    null;
  else
    --Verificar tipo_estado_bars = PREDICTORES_POTENCIAL
    select count(*) into vconteo_filas 
    from TIPO_ESTADO_BARS
    where upper(nombre) = ktipo_estado_predictores;
    if vconteo_filas >= 1 then --El tipo_estado_bars s� existe
      SELECT id into vtipo_estado
        from TIPO_ESTADO_BARS
        where upper(nombre) = ktipo_estado_predictores and rownum = 1;
    else --El tipo_estado_bars no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into vtipo_estado
        from TIPO_ESTADO_BARS;
      INSERT INTO TIPO_ESTADO_BARS (id, nombre)
           values ( vtipo_estado, ktipo_estado_predictores );
    end if;
    --Verificar estado deseado
    SELECT count(*) into vconteo_filas
      from ESTADO_BARS
      where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado);
    if vconteo_filas >= 1 then --El estado deseado existe
      SELECT id into vestado
        from ESTADO_BARS
        where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado) and rownum = 1;
    else --El estado deseado no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into vestado
        from ESTADO_BARS;
      INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
        values ( vestado, upper(pestado), vtipo_estado );
    end if;
    --Actualizar el estado del formulario  
    BEGIN
    SELECT nombre into vestado_anterior
      from ESTADO_BARS
      where id = (select estado from BARS_FORMEVAPRE where id = pformulario);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN --El formulario no existe luego no hay estado anterior
        vestado_anterior := '';
    END;
    UPDATE BARS_FORMEVAPRE SET estado = vestado, fecha_actualizacion = trunc(sysdate) where id = pformulario;
  end if;
EXCEPTION
  when others then
    raise_application_error(-20000,'Un error inesperado ocurri� en BARS_CAMESTFORMPRE. 
      Favor contactar al administrador del sistema. ' ||SQLCODE||' - '||SQLERRM || ' ');
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_CAMESTOBJ" (pformulario IN BARS_FORMEVAOBJ.ID%TYPE, pestado IN ESTADO_BARS.NOMBRE%TYPE)
is

    vconteo_filas NUMBER;
    l_evaluado NUMBER;
    l_evaluador NUMBER;
    l_target_opr NUMBER;
    vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
    vestado ESTADO_BARS.ID%TYPE;
    faltan_parametros EXCEPTION;
    formulario_inexistente EXCEPTION;
 
BEGIN

  --Verificar que el formulario exista
  select count(*) into vconteo_filas
  from BARS_FORMEVAOBJ
  where id = pformulario;
  if vconteo_filas = 0 then --formulario inexistente
    RAISE formulario_inexistente;
  else
    --Verificar tipo_estado_bars = 'OBJETIVOS_BARS'
    select count(*) into vconteo_filas 
    from TIPO_ESTADO_BARS
    where upper(nombre) = 'OBJETIVOS_BARS';
    if vconteo_filas >= 1 then --El tipo_estado_bars s� existe
      SELECT id into vtipo_estado
        from TIPO_ESTADO_BARS
        where upper(nombre) = 'OBJETIVOS_BARS' and rownum = 1;
    else --El tipo_estado_bars no existe. Generar excepci�n
      RAISE faltan_parametros;
    end if;
    --Verificar estado deseado
    SELECT count(*) into vconteo_filas
      from ESTADO_BARS
      where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado);
    if vconteo_filas >= 1 then --El estado deseado existe
      SELECT id into vestado
        from ESTADO_BARS
        where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado) and rownum = 1;
    else --El estado deseado no existe. Generar excepci�n
      RAISE faltan_parametros;
    end if;
    --Actualizar el estado del formulario
    --atroncoso 20150304 se agrega condici�n 
    
    if pestado = 'CONCERTACION APROBADA' then
        
        -- Se persiste en el formulario el TARGET del Trabajador
        select EVALUADO into l_evaluado from bars_formevaobj where id = pformulario;
        select ID_TARGET_OPR, JEFE into l_target_opr, l_evaluador from trabajador where NUMERO_IDENTIFICACION = l_evaluado;  
    
        UPDATE BARS_FORMEVAOBJ SET estado = vestado, fecheva = trunc(sysdate), evaluador = l_evaluador,
        PCTCONSFORM = null, RESULTADO_PONDERADO = null,POSICION = null, EVALUADOR_RESULTADO = null, ID_TARGET_OPR = l_target_opr
        where id = pformulario;
    else
        UPDATE BARS_FORMEVAOBJ SET estado = vestado, fecheva = trunc(sysdate) 
        where id = pformulario;
    end if;
  end if;
EXCEPTION
  when faltan_parametros then
    raise_application_error(-20000,'Falta el estado deseado para el cambio de estado en BARS_CAMESTOBJ. 
      Favor contactar al administrador del sistema. ');
  when formulario_inexistente then
    raise_application_error(-20000,'Falta el formulario para el cambio de estado en BARS_CAMESTOBJ. 
      Favor contactar al administrador del sistema. ');
  when others then
    raise_application_error(-20000,'Un error inesperado ocurri� en BARS_CAMESTOBJ. 
      Favor contactar al administrador del sistema. ' ||SQLCODE||' - '||SQLERRM || ' ');
END;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_CAMESTPDI" (pformulario IN BARS_PDI.ID%TYPE, pestado IN ESTADO_BARS.NOMBRE%TYPE)
is
 vconteo_filas NUMBER;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 faltan_parametros EXCEPTION;
 formulario_inexistente EXCEPTION;
 ktipoestadopdi CONSTANT VARCHAR2(30) := 'PDI';
BEGIN
  --Verificar que el formulario exista
  select count(*) into vconteo_filas
  from BARS_PDI
  where id = pformulario;
  if vconteo_filas = 0 then --formulario inexistente
    RAISE formulario_inexistente;
  else
    --Verificar tipo_estado_bars = 'PDI'
    select count(*) into vconteo_filas 
    from TIPO_ESTADO_BARS
    where upper(nombre) = ktipoestadopdi;
    if vconteo_filas >= 1 then --El tipo_estado_bars s� existe
      SELECT id into vtipo_estado
        from TIPO_ESTADO_BARS
        where upper(nombre) =  ktipoestadopdi and rownum = 1;
    else --El tipo_estado_bars no existe. Generar excepci�n
      RAISE faltan_parametros;
    end if;
    --Verificar estado deseado
    SELECT count(*) into vconteo_filas
      from ESTADO_BARS
      where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado);
    if vconteo_filas >= 1 then --El estado deseado existe
      SELECT id into vestado
        from ESTADO_BARS
        where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado) and rownum = 1;
    else --El estado deseado no existe. Generar excepci�n
      RAISE faltan_parametros;
    end if;
    --Actualizar el estado del formulario  
    UPDATE BARS_PDI SET ID_estado = vestado where id = pformulario;
  end if;
EXCEPTION
  when faltan_parametros then
    raise_application_error(-20000,'Falta el estado deseado para el cambio de estado en BARS_CAMESTPDI. 
      Favor contactar al administrador del sistema. ');
  when formulario_inexistente then
    raise_application_error(-20000,'Falta el formulario para el cambio de estado en BARS_CAMESTPDI. 
      Favor contactar al administrador del sistema. ');
  when others then
    raise_application_error(-20000,'Un error inesperado ocurri� en BARS_CAMESTPDI. 
      Favor contactar al administrador del sistema. ' ||SQLCODE||' - '||SQLERRM || ' ');
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_CREAMESAHOM" (
    pperiodo IN NUMBER)
IS
  /*Este procedimiento crea las mesas de homogeneizacion de evaluciones de
  actuaci�n (BARS) para un periodo dado,
  con base en una tabla temporal denominada BARS_TEMPCARGUEMESA con los datos,
  siguiendo la siguiente plantilla:
  consecutivo|id_trabajador(NUMBER)|nombres|apellidos|cargo|tipo_trabajador|
  area|id_gestor(NUMBER)|mesa. La tabla tendr� adicionalmente
  un campo resultado*/
  CURSOR ccargue
  IS
    SELECT
      *
    FROM
      BARS_TEMPCARGUEMESA
    ORDER BY
      CONSECUTIVO;
  vconteo                NUMBER;
  vidmesa                NUMBER;
  vidcargo               NUMBER;
  vidcargo_anterior      NUMBER;
  vidtipo_trabajador     NUMBER;
  vidarea                NUMBER;
  vidarea_anterior       NUMBER;
  vidgestor_anterior     NUMBER;
  trabajador_inexistente EXCEPTION;
BEGIN
  FOR reccargue IN ccargue
  LOOP
    BEGIN --Se necesita un bloque adicional, para que al encontrar que el
      -- trabajador no existe, se contin�e con el siguiente registro sin
      -- problema
      --Verificar si la mesa existe
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        BARS_MESAHOM
      WHERE
        periodo                    = pperiodo
      AND trim(upper(descripcion)) = trim(upper(reccargue.mesa));
      IF vconteo                   = 0 THEN --la mesa no existe as� que debe
        -- crearse
        SELECT
          NVL(MAX(id),0)+1
        INTO
          vidmesa
        FROM
          BARS_MESAHOM;
        INSERT
        INTO
          BARS_MESAHOM
          (
            id,
            periodo,
            descripcion
          )
          VALUES
          (
            vidmesa,
            pperiodo,
            trim(initcap(reccargue.mesa))
          );
      ELSE --la mesa s� existe, consultar su id
        SELECT
          id
        INTO
          vidmesa
        FROM
          BARS_MESAHOM
        WHERE
          periodo                    = pperiodo
        AND trim(upper(descripcion)) = trim(upper(reccargue.mesa));
      END IF;
      --Asociar el trabajador a la mesa para el periodo
      --Verificar si el trabajador existe
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        TRABAJADOR
      WHERE
        numero_identificacion = reccargue.id_trabajador;
      IF vconteo              = 0 THEN --el trabajador no existe. Se reporta en
        -- el log y se contin�a con el siguiente registro.
        raise trabajador_inexistente;
      END IF;
      --Asociar el trabajador a la mesa
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        BARS_MESAHOM_TRABAJADOR
      WHERE
        trabajador = reccargue.id_trabajador
      AND mesa     = vidmesa;
      IF vconteo   = 0 THEN --el trabajador no ha sido asociado as� que debe
        -- asociarse
        INSERT
        INTO
          BARS_MESAHOM_TRABAJADOR
          (
            trabajador,
            mesa
          )
          VALUES
          (
            reccargue.id_trabajador,
            vidmesa
          );
        UPDATE
          BARS_TEMPCARGUEMESA
        SET
          resultado = resultado
          || ' - '
          || 'Trabajador cargado'
        WHERE
          consecutivo = reccargue.consecutivo;
      END IF;
/*      --Actualizar datos del trabajador
      --Cargo
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        CARGO
      WHERE
        trim(upper(nombre)) = trim(upper(reccargue.cargo));
      IF vconteo            = 0 THEN --el cargo no existe. No se crea
        -- autom�ticamente sino se reporta en el log la no coincidencia.
        UPDATE
          BARS_TEMPCARGUEMESA
        SET
          resultado = resultado
          || ' - '
          || 'El cargo reportado no existe.'
        WHERE
          consecutivo = reccargue.consecutivo;
      ELSE --el cargo s� existe. Se procede a compararlo con el que tiene el
        -- trabajador (se toma el primero en caso que est� repetido)
        SELECT
          id
        INTO
          vidcargo
        FROM
          CARGO
        WHERE
          trim(upper(nombre)) = trim(upper(reccargue.cargo))
        AND rownum            = 1;
        SELECT
          cargo
        INTO
          vidcargo_anterior
        FROM
          TRABAJADOR
        WHERE
          numero_identificacion = reccargue.id_trabajador;
        IF vidcargo            != vidcargo_anterior THEN --Se debe actualizar
          -- el cargo
          UPDATE
            TRABAJADOR
          SET
            cargo = vidcargo
          WHERE
            numero_identificacion = reccargue.id_trabajador;
          UPDATE
            BARS_TEMPCARGUEMESA
          SET
            resultado = resultado
            || ' - '
            || 'Cargo actualizado. El anterior fue el id '
            || vidcargo_anterior
            || '.'
          WHERE
            consecutivo = reccargue.consecutivo;
        END IF;
      END IF;
      --�rea
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        AREA
      WHERE
        trim(upper(nombre)) = trim(upper(reccargue.area));
      IF vconteo            = 0 THEN --el �rea no existe. No se crea
        -- autom�ticamente sino se reporta en el log la no coincidencia.
        UPDATE
          BARS_TEMPCARGUEMESA
        SET
          resultado = resultado
          || ' - '
          || 'El �rea reportado no existe.'
        WHERE
          consecutivo = reccargue.consecutivo;
      ELSE --el �rea s� existe. Se procede a compararla con el que tiene el
        -- trabajador
        SELECT
          id
        INTO
          vidarea
        FROM
          AREA
        WHERE
          trim(upper(nombre)) = trim(upper(reccargue.area));
        SELECT
          area
        INTO
          vidarea_anterior
        FROM
          TRABAJADOR
        WHERE
          numero_identificacion = reccargue.id_trabajador;
        IF vidarea             != vidarea_anterior THEN --Se debe actualizar el
          -- �rea
          UPDATE
            TRABAJADOR
          SET
            area = vidarea
          WHERE
            numero_identificacion = reccargue.id_trabajador;
          UPDATE
            BARS_TEMPCARGUEMESA
          SET
            resultado = resultado
            || ' - '
            || '�rea actualizada. La anterior fue el id '
            || vidarea_anterior
            || '.'
          WHERE
            consecutivo = reccargue.consecutivo;
        END IF;
      END IF;
      --tipo_trabajador
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        BARS_TIPO_TRABAJADOR
      WHERE
        trim(upper(nombre)) = trim(upper(reccargue.tipo_trabajador));
      IF vconteo            = 0 THEN --el tipo no existe. No se crea
        -- autom�ticamente sino se reporta en el log la no coincidencia.
        UPDATE
          BARS_TEMPCARGUEMESA
        SET
          resultado = resultado
          || ' - '
          || 'El tipo_trabajador reportado no existe.'
        WHERE
          consecutivo = reccargue.consecutivo;
      ELSE --el tipo s� existe. Se procede a actualizarlo independiente de su
        -- correspondencia
        SELECT
          id_tipo_trabajador
        INTO
          vidtipo_trabajador
        FROM
          BARS_TIPO_TRABAJADOR
        WHERE
          trim(upper(nombre)) = trim(upper(reccargue.tipo_trabajador));
        UPDATE
          TRABAJADOR
        SET
          fk_tipo_trabajador = vidtipo_trabajador
        WHERE
          numero_identificacion = reccargue.id_trabajador;
      END IF;*/
      --jefe
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        TRABAJADOR
      WHERE
        numero_identificacion = reccargue.id_gestor;
      IF vconteo              = 0 THEN --el jefe no existe. Se reporta en el
        -- log.
        UPDATE
          BARS_TEMPCARGUEMESA
        SET
          resultado = resultado
          || ' - '
          || 'No existe el jefe.'
        WHERE
          consecutivo = reccargue.consecutivo;
      ELSE --el jefe si existe. Se verifica su correspondencia.
        SELECT
          jefe
        INTO
          vidgestor_anterior
        FROM
          TRABAJADOR
        WHERE
          numero_identificacion = reccargue.id_trabajador;
        IF reccargue.id_gestor != vidgestor_anterior THEN
          UPDATE
            BARS_TEMPCARGUEMESA
          SET
            resultado = resultado
            || ' - '
            || 'El jefe es diferente. El reportado es el id '
            || vidgestor_anterior
            || '.'
          WHERE
            consecutivo = reccargue.consecutivo;
        END IF;
      END IF;
    EXCEPTION --Para el manejo del trabajador inexistente sin terminar el loop
    WHEN TRABAJADOR_INEXISTENTE THEN
      UPDATE
        BARS_TEMPCARGUEMESA
      SET
        resultado = resultado
        || ' - '
        || 'No existe el trabajador.'
      WHERE
        consecutivo = reccargue.consecutivo;
    END;
  END LOOP;
EXCEPTION
WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20000,
  'Un error inesperado ocurri� en BARS_CREAMESAHOM. Por favor, inf�rmelo al administrador de la aplicaci�n. '
  || SQLCODE || ' - ' || SQLERRM);
END BARS_CREAMESAHOM;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_DETFORMPRE" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN BARS_PERPRE.ANO%type, ptipo IN BARS_FORMEVAPRE.TIPO%type, pformulario OUT BARS_FORMEVAPRE.ID%type)
IS
 vconteo_filas NUMBER;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vevaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO';
 k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO';
 k_tipo_estado_predictores CONSTANT VARCHAR2(30) := 'PREDICTORES_POTENCIAL';
BEGIN
  BARS_VALESTPERPRE(pano, k_estado_activo, vconteo_filas, vmensaje);
 if (vconteo_filas = 1) then --El periodo es el activo.
   BARS_VALEXIEVAPRE (pevaluado, pano, ptipo, vreturn, vmensaje);

   if (vreturn = 0) then --La evaluaci�n no existe. Crear formulario (la plantilla se determina con base en el periodo)
     --Elecci�n del evaluador
     SELECT TRABAJADOR.jefe into vevaluador
       from TRABAJADOR
       where TRABAJADOR.NUMERO_IDENTIFICACION = pevaluado;
     --Verificaci�n existencia del estado 'NUEVO'
     --Verificar tipo_estado_bars = PREDICTORES_POTENCIAL
     select count(*) into vconteo_filas 
       from TIPO_ESTADO_BARS
       where upper(nombre) = k_tipo_estado_predictores;
     if vconteo_filas >= 1 then --El tipo_estado_bars s� existe
       SELECT id into vtipo_estado
         from TIPO_ESTADO_BARS
         where upper(nombre) = k_tipo_estado_predictores and rownum = 1;
     else --El tipo_estado_bars no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vtipo_estado
         from TIPO_ESTADO_BARS;
       INSERT INTO TIPO_ESTADO_BARS (id, nombre)
         values ( vtipo_estado, k_tipo_estado_predictores );
     end if;
     --Verificar estado deseado
     SELECT count(*) into vconteo_filas
       from ESTADO_BARS
       where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo;
     if vconteo_filas >= 1 then --El estado 'NUEVO' existe
       SELECT id into vestado
         from ESTADO_BARS
         where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo and rownum = 1;
     else --El estado deseado no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vestado
         from ESTADO_BARS;
       INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
         values ( vestado, k_estado_nuevo, vtipo_estado );
     end if;
     --Llave primaria formulario a crear
     SELECT NVL( max(id),0 )+1 into pformulario
       from BARS_FORMEVAPRE;
     --Creaci�n formulario en estado nuevo
     INSERT INTO BARS_FORMEVAPRE (id, gestor, evaluado, periodo, estado, fecha_actualizacion, tipo)
       values (pformulario, vevaluador, pevaluado, pano, vestado, trunc(sysdate), ptipo);
   else --La encuesta existe, retornar formulario
     SELECT id into pformulario
       from BARS_FORMEVAPRE
       where evaluado = pevaluado and periodo = pano and tipo = ptipo;
   end if;

 else --El periodo no es el activo
   BARS_VALEXIEVAPRE (pevaluado, pano, ptipo, vreturn, vmensaje);
   if (vreturn = 0) then --La evaluaci�n no existe. C�mo no es periodo activo no se puede crear.
     pformulario := NULL;
   else --La evaluaci�n existe
     SELECT id into pformulario
       from BARS_FORMEVAPRE
       where evaluado = pevaluado and periodo = pano and tipo = ptipo;
   end if;
 end if;
EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en BARS_DETFORMPRE.' || SQLCODE || ' - ' || SQLERRM);
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_GUARDRESPPRE" (pformulario IN BARS_FORMEVAPRE.ID%type, pfactor IN BARS_FACPRE.ID%TYPE,
  prespuesta IN BARS_TIPORESP_OPCION.ID%TYPE)
IS
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vconteo_filas NUMBER;
BEGIN
  --Verificar que el factor exista y tenga asociado un tipo de respuesta
  SELECT count(*) into vconteo_filas
    from BARS_FACPRE
    where id = pfactor and tiporesp is not null;
  if vconteo_filas = 0 then --El factor no existe o no es v�lido para tener respuesta asociada
    null;
  else
    --Verificar que la respuesta corresponda a las posibles para el factor
	SELECT count(*) into vconteo_filas
	  from BARS_TIPORESP_OPCION
	  where tiporesp = (select tiporesp from BARS_FACPRE where id = pfactor) and id = prespuesta;
	if vconteo_filas = 0 then --La respuesta no es v�lida para el factor
	  null;
	else
	  --Verificar si la respuesta est� siendo insertada o actualizada
	  SELECT count(*) into vconteo_filas
	    from BARS_RESPFORMEVAPRE
		where formevapre = pformulario and facpre = pfactor;
      if vconteo_filas = 0 then --Insertar la respuesta
	    INSERT INTO BARS_RESPFORMEVAPRE (formevapre, facpre, tiporesp_opcion)
	      values (pformulario, pfactor, prespuesta);
	  else
	    UPDATE BARS_RESPFORMEVAPRE set tiporesp_opcion = prespuesta
		  where formevapre = pformulario and facpre = pfactor;
	  end if;
	end if;  
  end if;

EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en BARS_GUARDRESPPRE.' || SQLCODE || ' - ' || SQLERRM);
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_PARAMETRIZA_PERIODO" 
(pano IN NUMBER)
AS
--Procedimiento para parametrizar autom�ticamente el periodo bars de un a�o a otro, si las condiciones se mantienen iguales
kestado_inactivo CONSTANT VARCHAR2(100) := 'INACTIVO';
ktipo_estado_periodo CONSTANT VARCHAR2(100) := 'PERIODO_BARS';
vestado_inactivo ESTADO_BARS.id%type;
id_pct NUMBER;
conteo NUMBER;

BEGIN

select ESTADO_BARS.id into vestado_inactivo
 from ESTADO_BARS
 where ESTADO_BARS.tipo_estado = (SELECT id from TIPO_ESTADO_BARS where upper(nombre) = ktipo_estado_periodo)
  and upper(ESTADO_BARS.nombre) = kestado_inactivo;

--PARAMETRIZACI�N PARA EVALUACI�N Y AUTOEVALUACI�N

--periodo_bars se crea en estado inactivo si no existe
select count(*) into conteo
 from PERIODO_BARS
 where ano = pano;
if (conteo = 0) then
 insert into periodo_bars (ano, estado) values (pano, vestado_inactivo);
end if;

/* ! ! ! Se elimina: https://sinerware.atlassian.net/browse/GTH2019-81 ! ! ! 
    --PCT_CONSECUCION_BARS
    select count(*) into conteo
     from PCT_CONSECUCION_BARS
     where periodo = pano;
    
    if (conteo = 0) then
    insert into PCT_CONSECUCION_BARS(id, pct_consecucion,media_comp_inf,media_comp_sup,periodo)
      SELECT PCT_CONSECUCION_BARS_SEQ.NEXTVAL,pct_consecucion, media_comp_inf, media_comp_sup, pano
        FROM PCT_CONSECUCION_BARS
        WHERE periodo = (pano-1);
    end if;
*/

--AJUSTE_HOMOGENEIZACION (No implemtado a�n)
select count(*) into conteo FROM BARS_AJUHOM
where periodo = pano;
 if (conteo = 0) then
insert into BARS_AJUHOM (id, valor, periodo)
 SELECT id+5, valor, pano
 FROM BARS_AJUHOM
 where periodo = (pano-1);
 end if; 

--bars_posicion

select count(*) into conteo
 from BARS_POSICION
 where periodo = pano;
 if (conteo = 0) then
insert into BARS_POSICION (id, nombre, pct_inf, pct_sup, periodo)
 SELECT id+3, nombre, pct_inf, pct_sup, pano
 from bars_posicion
 where periodo = (pano-1);
 end if;

--BARS_RESTRPOSICION
select count(*) into conteo
 from BARS_RESTRPOSICION
 where posicion in (select id from BARS_POSICION where periodo=pano);
 if (conteo = 0) then
insert into BARS_RESTRPOSICION (posicion, RESTRMIN, RESTRMAX)
 SELECT res.posicion+3, res.restrmin, res.restrmax
 from BARS_RESTRPOSICION res inner join BARS_POSICION pos on (res.posicion = pos.id)
 where pos.periodo = (pano-1);
 end if;
--BARS_TEXTORESPUESTA
select count(*) into conteo
 from BARS_TEXTORESPUESTA
 where periodo=pano;
 if (conteo = 0) then
insert into BARS_TEXTORESPUESTA (id, valorresp, nombre, periodo)
 SELECT id+5, valorresp, nombre, pano
 FROM BARS_TEXTORESPUESTA
 where periodo = (pano-1);
 end if;
 commit;
END BARS_PARAMETRIZA_PERIODO;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_SUBMITCAMFORMOBJ" ( pid IN BARS_FORMEVAOBJ.id%type, pestado_nuevo IN ESTADO_BARS.id%type, 
 pevaluador IN BARS_FORMEVAOBJ.EVALUADOR%type, pcargo IN BARS_FORMEVAOBJ.cargo%type, 
 pcencos IN BARS_FORMEVAOBJ.cencos%type, pzona IN BARS_FORMEVAOBJ.zona%type, 
 pevaluador_resultado IN BARS_FORMEVAOBJ.evaluador_resultado%type )
IS
knuevo CONSTANT VARCHAR2(30) := 'NUEVO';
ket CONSTANT VARCHAR2(30) := 'EN TRATAMIENTO';
kconval CONSTANT VARCHAR2(30) := 'CONCERTACION VALIDADA';
kconap CONSTANT VARCHAR2(30) := 'CONCERTACION APROBADA';
kperaj CONSTANT VARCHAR2(30) := 'PERIODO AJUSTE';
kresval CONSTANT VARCHAR2(30) := 'RESULTADO VALIDADO';
kfinalizado CONSTANT VARCHAR2(30) := 'FINALIZADO';
kanulado CONSTANT VARCHAR2(30) := 'ANULADO';
vestado_actual VARCHAR2(30);
vestado_nuevo VARCHAR2(30);

BEGIN
SELECT upper(e.nombre) into vestado_actual
 FROM BARS_FORMEVAOBJ f inner join ESTADO_BARS e on (f.estado = e.id)
 WHERE f.id = pid;

SELECT upper(nombre) into vestado_nuevo
 FROM ESTADO_BARS
 WHERE id = pestado_nuevo;

UPDATE BARS_FORMEVAOBJ SET EVALUADOR = pevaluador, CARGO = pcargo, CENCOS = pcencos, 
  ZONA = pzona, EVALUADOR_RESULTADO = pevaluador_resultado, ESTADO = pestado_nuevo, fecheva = trunc(sysdate)
  WHERE id = pid;

 CASE vestado_nuevo --Se maneja la l�gica adicional al cambio del estado

  WHEN knuevo THEN --Si se eliminan los objetivos y comentarios existentes, debe en javascript generarse un mensaje de advertencia
   NULL;

  WHEN ket THEN
   UPDATE BARS_FORMEVAOBJ SET PCTCONSFORM = NULL, evaluador_resultado = NULL, evaluador = NULL
    WHERE id = pid;

  WHEN kconval THEN
   UPDATE BARS_FORMEVAOBJ SET PCTCONSFORM = NULL, evaluador_resultado = NULL, evaluador = NULL
    WHERE id = pid;

  WHEN kconap THEN
   UPDATE BARS_FORMEVAOBJ SET PCTCONSFORM = NULL, evaluador_resultado = NULL
    WHERE id = pid;

  WHEN kresval THEN
   UPDATE BARS_FORMEVAOBJ SET PCTCONSFORM = NULL, evaluador_resultado = NULL
    WHERE id = pid;

  WHEN kfinalizado THEN --Se valida que este cambio sea imposible
   NULL;

  WHEN kanulado THEN --No se elimina su informaci�n sino que deja de salir en los resultados. Se elimina la info?
   UPDATE BARS_FORMEVAOBJ SET PCTCONSFORM = NULL
    WHERE id = pid;

  ELSE
   NULL;

 END CASE;

EXCEPTION
 WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurri� en BARS_SUBMITCAMFORMOBJ. Por favor, inf�rmelo al administrador de la aplicaci�n. ' 
	  || SQLCODE || ' - ' || SQLERRM);

END BARS_SUBMITCAMFORMOBJ;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_SUBMITOBJ" (
    pitem       IN VARCHAR2,
    pformulario IN BARS_FORMEVAOBJ.ID%TYPE,
    pmsj OUT VARCHAR2)
IS
  knuevo             CONSTANT VARCHAR2(30) := 'NUEVO';
  ket                CONSTANT VARCHAR2(30) := 'EN TRATAMIENTO';
  kconval            CONSTANT VARCHAR2(30) := 'CONCERTACION VALIDADA';
  kconap             CONSTANT VARCHAR2(30) := 'CONCERTACION APROBADA';
  kperaj             CONSTANT VARCHAR2(30) := 'CONCERTACION ANULADA';
  kresval            CONSTANT VARCHAR2(30) := 'RESULTADO VALIDADO';
  kfinalizado        CONSTANT VARCHAR2(30) := 'FINALIZADO';
  kanulado           CONSTANT VARCHAR2(30) := 'ANULADO';
  kadministradores   CONSTANT VARCHAR2(30) := 'ADMINISTRADORES OPR';
  vtipo_estado       NUMBER(2,0);
  vestado_formulario VARCHAR2(30);
  vcontador          NUMBER;
  vappuser           NUMBER;
  vevaluado BARS_FORMEVAOBJ.EVALUADO%TYPE;
  vevaluador BARS_FORMEVAOBJ.EVALUADOR%TYPE;
  l_id_target_opr BARS_FORMEVAOBJ.id_target_opr%type;
  vResultadoValFormevaobj NUMBER;
  l_numero_aprobador NUMBER;
  --Colecci�n para almacenar los grupos del usuario
TYPE grupo_array
IS
  TABLE OF GRUPO.NOMBRE%TYPE;
  vgrupos grupo_array;
  i NUMBER(2) := 0;
BEGIN
  --Determinar tipo estado OBJETIVOS_BARS
  SELECT id
  INTO vtipo_estado
  FROM TIPO_ESTADO_BARS
  WHERE upper(nombre) = 'OBJETIVOS_BARS';
  --Determinar estado del formulario de OBJETIVOS, devuelve el estado en mayuscula
  SELECT upper(trim(E.nombre))
  INTO vestado_formulario
  FROM BARS_FORMEVAOBJ F
  INNER JOIN ESTADO_BARS E
  ON F.estado = E.id
  WHERE F.id  = pformulario;
  --Determinar evaluado del formulario y evaluador del trabajador
  SELECT evaluado
  INTO vevaluado
  FROM BARS_FORMEVAOBJ
  WHERE id = pformulario;

  SELECT jefe
  INTO vevaluador
  FROM TRABAJADOR
  WHERE numero_identificacion = vevaluado;
  
  --Si se ejecuta por debajo (ie. desde PL/SQL) por omisi�n es APEX quien lo ejecuta
  SELECT numero_identificacion
      INTO vappuser
      FROM USUARIO
      WHERE UPPER (user_name)= NVL(UPPER(v('APP_USER')),'APEX');

  --Determinar grupos (i.e. roles) del usuario
  vgrupos := grupo_array();
  FOR rec_grupo IN
  (SELECT upper(trim(gb.nombre)) nombre
  FROM USU_GRUPO ubg
  INNER JOIN GRUPO gb
  ON ubg.grupo = gb.id
  INNER JOIN USUARIO u
  ON (u.id=ubg.usuario)
  WHERE UPPER(u.USER_NAME)   = UPPER(v('APP_USER'))
  )
  LOOP
    i := i + 1;
    vgrupos.EXTEND(1);
    vgrupos(i) := rec_grupo.nombre;
  END LOOP;

  --Cambio de estado del formulario de acuerdo al item de la acci�n
  CASE upper(pitem)

  WHEN 'LISTA_OBJETIVOS_SAVE' THEN
    SELECT COUNT(*)
    INTO vcontador
    FROM BARS_OBJETIVO
    WHERE formevaobj       = pformulario;
    IF vestado_formulario != kperaj AND vcontador >= 1 THEN
      BARS_CAMESTOBJ (pformulario, ket);
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;

  WHEN 'LISTA_OBJETIVOS_ADD' THEN
    SELECT COUNT(*)
    INTO vcontador
    FROM BARS_OBJETIVO
    WHERE formevaobj        = pformulario;
    IF (vestado_formulario != kperaj) AND (vcontador >= 1) THEN
      BARS_CAMESTOBJ (pformulario, ket);
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;

  WHEN 'LISTA_OBJETIVOS_DELETE' THEN
    SELECT COUNT(*)
    INTO vcontador
    FROM BARS_OBJETIVO
    WHERE formevaobj = pformulario;
    IF (vestado_formulario != kperaj) AND (vcontador >= 1) THEN --Se podr�a pasar a NUEVO un formulario con comentarios o fortalezas
      BARS_CAMESTOBJ (pformulario, ket);
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    ELSE
      BARS_CAMESTOBJ (pformulario, knuevo);
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;

  WHEN 'LISTA_OBJETIVOS_VALIDAR_CONCERTACION' THEN
    DECLARE

      l_obj_cer BOOLEAN;
      l_obj_abi BOOLEAN;
      l_peso_requerido NUMBER;
      l_periodo NUMBER;
      l_target NUMBER;
          
    BEGIN
    
    l_obj_cer := validaciones_gui.val_peso_total_objetivos(pformulario,1011);--Validar Objetivos Cerrados
    l_obj_abi := validaciones_gui.val_peso_total_objetivos(pformulario,1010);--Validar Objetivos Abiertos

      IF ((l_obj_cer and l_obj_abi) or l_obj_abi) THEN
        BARS_CAMESTOBJ (pformulario, kconval);
        UPDATE BARS_FORMEVAOBJ SET FECHEVA = sysdate WHERE ID = pformulario;
        pmsj := 'Su concertaci�n de objetivos ha sido validada. ';
        
      ELSE
      if(not l_obj_cer) then
       pmsj := 'Su concertaci�n no es v�lida. La suma de la ponderacion de objetivos cerrados no cumple el porcentaje para el Target del evaluado.';
      end if;
      if(not l_obj_abi) then
       select periodo, id_target_opr into l_periodo, l_target from BARS_FORMEVAOBJ where id = pformulario;
       l_peso_requerido := (100 * VALIDACIONES_GUI.calc_peso_requerido_evaluacion(l_periodo, l_target, 1162));
       pmsj := 'Su concertaci�n no es v�lida. La suma de la ponderacion de objetivos abiertos no cumple el porcentaje para el Target del evaluado: ' || l_peso_requerido;
      end if;
      END IF;
    END;

  WHEN 'LISTA_OBJETIVOS_APROBAR_CONCERTACION' THEN
  DECLARE 
   l_obj_cer BOOLEAN;
   l_obj_abi BOOLEAN;
   l_peso_requerido NUMBER;
   l_periodo NUMBER;
   l_target NUMBER;
   
  BEGIN
  
    l_obj_cer := validaciones_gui.val_peso_total_objetivos(pformulario,1011);--Validar Objetivos Cerrados
    l_obj_abi := validaciones_gui.val_peso_total_objetivos(pformulario,1010);--Validar Objetivos Abiertos
    
    IF ((l_obj_cer and l_obj_abi) or l_obj_abi) THEN
        UPDATE BARS_FORMEVAOBJ SET evaluador = vevaluador WHERE id = pformulario;
        BARS_CAMESTOBJ (pformulario, kconap);
        UPDATE BARS_FORMEVAOBJ SET FECHEVA = sysdate WHERE ID = pformulario;
        UPDATE BARS_FORMEVAOBJ SET FECHA_ACU_OBJ = sysdate WHERE ID = pformulario;
        --Se actualiza el evaluador (aprobador de la concertaci�n) para que coincida con quien hace la aprobaci�n de la concertaci�n
        pmsj := 'La concertaci�n de objetivos ha sido aprobada. El proceso continuar� cuando el gestor ingrese sus resultados al finalizar el periodo. ';
    else
        select periodo, id_target_opr into l_periodo, l_target from BARS_FORMEVAOBJ where id = pformulario;
        l_peso_requerido := (100 * VALIDACIONES_GUI.calc_peso_requerido_evaluacion(l_periodo, l_target, 1162));
        pmsj := 'La concertacion NO ha sido aprobada. La suma de la ponderacion de objetivos abiertos no cumple el porcentaje para el Target del evaluado: ' || l_peso_requerido;
    END IF;

    declare
    l_counter NUMBER;
    begin
    l_counter := 1;
    for i in (select * from bars_objetivo where FORMEVAOBJ = pformulario)
    loop
        update bars_objetivo set objnum = l_counter where id = i.id;
        l_counter := l_counter + 1;
    end loop;
    end;

  END;
  WHEN 'LISTA_OBJETIVOS_MODIFY' THEN
    DECLARE
      sum_ponderacion  NUMBER;
      vcountresultados NUMBER;
      vcountfortalezas NUMBER;
      vpondnul         NUMBER;
    BEGIN

      SELECT COUNT (*)
      INTO vpondnul
      FROM BARS_OBJETIVO
      WHERE FORMEVAOBJ = pformulario
      AND PONDERACION IS NULL;

      SELECT NVL(SUM(PONDERACION),0)
      INTO sum_ponderacion
      FROM BARS_OBJETIVO
      WHERE FORMEVAOBJ = pformulario;

      SELECT COUNT(*)
      INTO vcountresultados
      FROM BARS_OBJETIVO
      WHERE FORMEVAOBJ=pformulario
      AND PCTCONSIND IS NULL ;

      SELECT COUNT(*)
      INTO vcountfortalezas
      FROM BARS_FORTEVAOBJ
      WHERE FORMEVAOBJ=pformulario;


      --Grabar comentarios evaluado por administrador

      IF vpondnul=0  and (validaciones_gui.val_peso_total_objetivos(pformulario,1010)) THEN
        --Pasar a concertaci�n aprobada
        BARS_CAMESTOBJ (pformulario, kconap);
        UPDATE BARS_FORMEVAOBJ SET EVALUADOR =vappuser WHERE ID=pformulario;
        UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
        UPDATE BARS_FORMEVAOBJ SET FECHA_ACU_OBJ=sysdate WHERE ID=pformulario;
        -- Pasar a resultado validado
        IF vcountresultados=0 THEN
            BARS_CAMESTOBJ (pformulario, kfinalizado);
            UPDATE BARS_FORMEVAOBJ SET EVALUADOR_RESULTADO=vappuser WHERE ID=pformulario;
            UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
        END IF;
      ELSE
        BARS_CAMESTOBJ (pformulario, ket);
      END IF;

     declare
    l_counter NUMBER;
    begin
    l_counter:=1;
    for i in (select * from bars_objetivo where FORMEVAOBJ=pformulario)
    loop
        update bars_objetivo set objnum=l_counter where id = i.id;
        l_counter:=l_counter+1;
    end loop;
    end;

    END;
  WHEN 'LISTA_OBJETIVOS_SAVE_RESULTADOS' THEN
    IF (vestado_formulario != kconap) THEN
      BARS_CAMESTOBJ (pformulario, kconap);
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;
  WHEN 'FORTALEZAS_SAVE' THEN
    IF v('P44_FORTACT') IS NOT NULL THEN
      SELECT COUNT(*)
      INTO vcontador
      FROM BARS_FORTEVAOBJ
      WHERE formevaobj = pformulario;
      IF ( vcontador   = 0 ) THEN --No se ha ingresado fortaleza aun
        INSERT
        INTO BARS_FORTEVAOBJ
          (
            formevaobj,
            fortact
          )
          VALUES
          (
            pformulario,
            v('P44_FORTACT')
          );
      ELSE --Ya se ha ingresado la fortaleza
        UPDATE BARS_FORTEVAOBJ
        SET fortact      = v('P44_FORTACT')
        WHERE formevaobj = pformulario;
      END IF;
      pmsj := 'Fueron almacenados las fortalezas o aspectos por fortalecer del trabajador. ';
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;
    IF v('P44_FORTDES') IS NOT NULL THEN
      SELECT COUNT(*)
      INTO vcontador
      FROM BARS_FORTEVAOBJ
      WHERE formevaobj = pformulario;
      IF ( vcontador   = 0 ) THEN --No se ha ingresado fortaleza aun
        INSERT
        INTO BARS_FORTEVAOBJ
          (
            formevaobj,
            fortdes
          )
          VALUES
          (
            pformulario,
            v('P44_FORTDES')
          );
      ELSE --Ya se ha ingresado la fortaleza
        UPDATE BARS_FORTEVAOBJ
        SET fortdes      = v('P44_FORTDES')
        WHERE formevaobj = pformulario;
      END IF;
      pmsj := 'Fueron almacenados las fortalezas o aspectos por fortalecer del trabajador. ';
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;

  WHEN 'LISTA_OBJETIVOS_VALIDAR_RESULTADO' THEN

      DECLARE
      CURSOR cobjetivo
      IS
        SELECT pctconsind
        FROM BARS_OBJETIVO
        WHERE formevaobj = pformulario;
      vcontador NUMBER;
      l_tipo_validacion VARCHAR2(20);
      l_peso_eval NUMBER;
      l_periodo NUMBER;
      l_evaluado NUMBER;
      l_peso_planeado_obj_cer BOOLEAN;
      l_peso_planeado_obj_abi BOOLEAN;
      l_peso_requerido_obj_cer NUMBER;
      l_resultado_obj_cer BOOLEAN;
      
    BEGIN
    
       -- Se obtiene el TARGET_OPR del trabajador (ya no del formulario)
    select EVALUADO, periodo into l_evaluado, l_periodo from bars_formevaobj where id = pformulario;
    select ID_TARGET_OPR into l_id_target_opr from trabajador where NUMERO_IDENTIFICACION = l_evaluado;  

      --Determinar si cumple ponderaci�n de objetivos para determinar validaci�n de resultado a realizar (Total que lleva a finalizado o solo Abiertos que lleva a resulltado validado)
    l_peso_requerido_obj_cer := VALIDACIONES_GUI.calc_peso_requerido_evaluacion( l_periodo, l_id_target_opr, SNW_CONSTANTES.constante_tipo('OPR_CER') );
    
    
      SELECT count(*) into vcontador
       FROM BARS_OBJETIVO o 
       WHERE o.formevaobj = pformulario and o.pctconsind is null and o.id_tipo = SNW_CONSTANTES.constante_tipo('CERRADO');
      if vcontador = 0 then l_resultado_obj_cer := TRUE; else l_resultado_obj_cer := FALSE; end if;
      l_peso_planeado_obj_cer:=validaciones_gui.val_peso_total_objetivos(pformulario,1011);--Validar Ponderaci�n Requerida de Objetivos Cerrados
      l_peso_planeado_obj_abi:=validaciones_gui.val_peso_total_objetivos(pformulario,1010);--Validar Ponderaci�n Requerida de Objetivos Abiertos

      CASE WHEN ( l_peso_requerido_obj_cer=0 and l_peso_planeado_obj_cer and l_peso_planeado_obj_abi ) THEN l_tipo_validacion := 'TOTAL';
           WHEN ( l_peso_requerido_obj_cer!=0 and l_peso_planeado_obj_cer and l_resultado_obj_cer and l_peso_planeado_obj_abi ) THEN l_tipo_validacion := 'TOTAL';
           WHEN ( l_peso_requerido_obj_cer!=0 and not l_resultado_obj_cer and l_peso_planeado_obj_abi ) THEN l_tipo_validacion := 'SOLO_ABIERTOS';
           WHEN ( l_peso_requerido_obj_cer!=0 and NOT l_peso_planeado_obj_cer and l_resultado_obj_cer and l_peso_planeado_obj_abi ) THEN l_tipo_validacion := 'SOLO_ABIERTOS';
           WHEN ( l_peso_requerido_obj_cer!=0 and not l_peso_planeado_obj_abi ) THEN l_tipo_validacion := 'FALLA_CONCERTACION';
           ELSE l_tipo_validacion := 'FALLA_CONCERTACION';
      END CASE;

      if l_tipo_validacion = 'TOTAL' then --Validaci�n Total

      -- << Ahora se valida param�tricamente mediante la tabla RESTRICCION_RESULTADOS_OPR (Ver el issue de Jira para m�s detalles): >>
      -- https://sinerware.atlassian.net/browse/GTH2019-53 
      
      
        --val_formevaobj (pformulario, vResultadoValFormevaobj, pmsj, 'TOTAL' ); --Validaciones
        --IF ( vResultadoValFormevaobj = 1 ) then --Pas� las validaciones
          cal_pct_consecucion_obj(pformulario);
          
          if ( ADMIN_OBJETIVOS_PCK.CALCULAR_PCTCONSABI( pformulario ) ) then
            null; --No hay comportamiento especial en caso que falle o no
          end if;
          
          select sum(porcentaje_total)*100 into l_peso_eval from restriccion_target 
              where id_target = l_id_target_opr
              and periodo=l_periodo
              and id_tipo_evaluacion in (1161,1162);                             
          cal_pct_ponderado_form(pformulario,l_peso_eval);       
          cal_posicion_obj(pformulario);
          cal_pct_consecucion_formobj(pformulario);
          BARS_CAMESTOBJ (pformulario, kfinalizado);
          pmsj := 'Formulario diligenciado correctamente, muchas gracias ';
          select numero_identificacion into l_numero_aprobador from usuario where upper(user_name)= upper(v('APP_USER'));
          UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate,EVALUADOR_RESULTADO=l_numero_aprobador  WHERE ID=pformulario;
        --ELSE
        --  pmsj := 'Los resultados ingresados no son v�lidos. ' || pmsj;
        --END IF;

      elsif l_tipo_validacion = 'SOLO_ABIERTOS' then --Validaci�n solo Abiertos
        --val_formevaobj (pformulario, vResultadoValFormevaobj, pmsj, 'ABIERTO' ); --Validaciones
        --IF ( vResultadoValFormevaobj = 1 ) then --Pas� las validaciones
          cal_pct_consecucion_obj(pformulario);
          if ( ADMIN_OBJETIVOS_PCK.CALCULAR_PCTCONSABI( pformulario ) ) then
            null; --No hay comportamiento especial en caso que falle o no
          end if;
          BARS_CAMESTOBJ (pformulario, kresval);
          pmsj := 'Formulario diligenciado correctamente, muchas gracias ';
          select numero_identificacion into l_numero_aprobador from usuario where upper(user_name)= NVL(upper(v('APP_USER')),'APEX');
          UPDATE BARS_FORMEVAOBJ SET FECHEVA = sysdate,EVALUADOR_RESULTADO = l_numero_aprobador  WHERE ID = pformulario;
          
        --ELSE
        --  pmsj := 'Los resultados ingresados no son v�lidos. ' || pmsj;
        --END IF;

      else --Error concertaci�n

        pmsj := 'Debe corregir error en la ponderaci�n de objetivos.';  
        

      end if;

    EXCEPTION
     WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurri� en la Validaci�n de Resultados. Por favor, inf�rmelo al administrador de la aplicaci�n.  ' || SQLCODE || ' - ' || SQLERRM);
    END;


  WHEN 'COMENTARIOS_SAVE_O' THEN
    IF v('P44_COMENTARIO_EVALUADO') IS NOT NULL THEN
      SELECT COUNT(*)
      INTO vcontador
      FROM BARS_COMEVAOBJ
      WHERE formeva_obj = pformulario;
      IF ( vcontador    = 0 ) THEN --No se ha ingresado comentario aun
        INSERT
        INTO BARS_COMEVAOBJ
          (
            formeva_obj,
            comentario_evaluado
          )
          VALUES
          (
            pformulario,
            v('P44_COMENTARIO_EVALUADO')
          );
      ELSE --Ya se ha ingresado el comentario
        UPDATE BARS_COMEVAOBJ
        SET comentario_evaluado = v('P44_COMENTARIO_EVALUADO')
        WHERE formeva_obj       = pformulario;
      END IF;
      pmsj := 'Sus comentarios fueron almacenados. ';
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;
  WHEN 'COMENTARIOS_SAVE_R' THEN
    IF v('P44_COMENTARIO_EVALUADOR') IS NOT NULL THEN
      SELECT COUNT(*)
      INTO vcontador
      FROM BARS_COMEVAOBJ
      WHERE formeva_obj = pformulario;
      IF ( vcontador    = 0 ) THEN --No se ha ingresado comentario aun
        INSERT
        INTO BARS_COMEVAOBJ
          (
            formeva_obj,
            comentario_evaluador
          )
          VALUES
          (
            pformulario,
            v('P44_COMENTARIO_EVALUADOR')
          );
        UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
      ELSE --Ya se ha ingresado el comentario
        UPDATE BARS_COMEVAOBJ
        SET comentario_evaluador = v('P44_COMENTARIO_EVALUADOR')
        WHERE formeva_obj        = pformulario;
        UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
      END IF;
    END IF;
  WHEN 'FINALIZAR' THEN
    cal_pct_consecucion_formobj(pformulario);
    UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate,EVALUADOR_RESULTADO=vappuser WHERE ID=pformulario;
    BARS_CAMESTOBJ (pformulario, kfinalizado);
    pmsj := 'El formulario se ha finalizado. Se mantiene disponible solo en modo consulta. ';
  ELSE
    NULL;
  END CASE;
EXCEPTION
WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurri� en BARS_SUBMITOBJ. Por favor, inf�rmelo al administrador de la aplicaci�n. ' || SQLCODE || ' - ' || SQLERRM);
END BARS_SUBMITOBJ;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_SUBMITPDI" (pitem IN VARCHAR2, pformulario IN BARS_PDI.ID%TYPE, pmsj OUT VARCHAR2)
IS
kplaneado CONSTANT VARCHAR2(30) := 'PLANEADO';
kconcertado CONSTANT VARCHAR2(30) := 'CONCERTADO';
kfinalizado CONSTANT VARCHAR2(30) := 'FINALIZADO';
kanulado CONSTANT VARCHAR2(30) := 'ANULADO';
knoaprobado CONSTANT VARCHAR2(30) := 'NO APROBADO';
kadministradores CONSTANT VARCHAR2(30) := 'ADMINISTRADORES';
kmodoconsulta CONSTANT VARCHAR2(30) := 'CONSULTA';
kmodoedicion CONSTANT VARCHAR2(30) := 'EDICION';
ktipoestadopdi CONSTANT VARCHAR2(30) := 'PDI';
vtipo_estado NUMBER(2,0);
vestado_formulario ESTADO_BARS.nombre%type;
vestado_anterior ESTADO_BARS.nombre%type;
vevaluado BARS_FORMEVAOBJ.EVALUADO%TYPE;
vgestor TRABAJADOR.JEFE%TYPE; --Gestor actual del trabajador
vevaluador BARS_encPDI.EVALUADOR%type; --Gestor registrado en el PDI como concertador
vaccion tipo.nombre%type;
vconteo NUMBER;


BEGIN
/*Para el caso en que se actualiza el formulario (posibilidad �nica del Trabajador cuando est� nuevo o planeado, o del administrador
  se requiere saber en qu� estado se almacen� (solo el administrador puede editar manualmente dicho campo). Tener presente que este
  procedimiento se ejecuta despu�s del process row
  */
BEGIN
 SELECT upper(ESTADO_BARS.nombre) into vestado_formulario
 FROM ESTADO_BARS
 WHERE ESTADO_BARS.id = ( SELECT ID_estado FROM BARS_PDI WHERE id = pformulario );
EXCEPTION
 WHEN NO_DATA_FOUND THEN --El formulario est� reci�n creado y sin estado asignado (como cuando se crea el PDI)
      vestado_formulario := 'NUEVO';
END;
--Determinar acci�n de desarrollo
SELECT upper(nombre) into vaccion
  FROM tipo 
  WHERE id = ( SELECT ID_accion FROM BARS_PDI WHERE id = pformulario );

--Cambios de estado de pdis de acuerdo al item(request) y al estado del formulario
CASE upper(pitem)
WHEN 'CREATE' THEN
  BARS_CAMESTPDI(pformulario, kplaneado);
  pmsj := ' El PDI ha sido planeado. Solicite a su gestor que apruebe la concertaci�n del PDI. ';
WHEN 'SAVE' THEN

  --El comportamiento depende del estado que se asign� al formulario
  BEGIN
    SELECT upper(nombre) into vestado_anterior from ESTADO_BARS where id = v('P78_ESTADO_ANTERIOR');
  EXCEPTION
    WHEN NO_DATA_FOUND THEN --El estado anterior era nulo (como cuando se crea el PDI)
      vestado_anterior := 'NUEVO';
  END;

  CASE vestado_formulario
    WHEN kplaneado THEN --Aplica cuando se hacen modificaciones estando planeado o el administrador lo devuelve a planeado
      UPDATE BARS_ENCPDI SET evaluador = null
        WHERE id = pformulario;
      pmsj := ' El PDI est� en estado planeado. Puede ser modificado hasta que solicite al gestor que apruebe la concertaci�n del PDI. ';
    WHEN kconcertado THEN --Aplica cuando el administrador lo modifica a estado concertado
      IF vestado_anterior != kconcertado then --El cambio s� fue de estado
        UPDATE BARS_ENCPDI SET evaluador = ( SELECT numero_identificacion FROM USUARIO WHERE upper(user_name) = upper(v('APP_USER')) )
          WHERE id = pformulario;
        pmsj := ' El PDI ha sido concertado. Cuando sea ejecutado, ingrese los datos faltantes y Final�celo. ';
      ELSE
        pmsj := ' Cambios en el PDI almacenados. ';
      END IF;
    WHEN knoaprobado THEN --Aplica cuando el administrador lo modifica a estado no aprobado
      IF vestado_anterior != knoaprobado then --El cambio s� fue de estado
        UPDATE BARS_ENCPDI SET evaluador = ( SELECT numero_identificacion FROM USUARIO WHERE upper(user_name) = upper(v('APP_USER')) )
          WHERE id = pformulario;
        pmsj := ' El PDI no ha sido aprobado.';
      ELSE
        pmsj := ' Cambios en el PDI almacenados. ';
      END IF;
    WHEN kanulado THEN --Aplica cuando el administrador lo modifica a estado anulado
      pmsj := ' El PDI ha sido anulado.';
   -- Esta acci�n se hace en la fase 2      
    /*WHEN kfinalizado THEN --Aplica cuando el administrador lo modifica a estado finalizado
      SELECT count(*) into vconteo
        FROM BARS_PDI
        WHERE id = pformulario AND NVL(estfinformacion, 0) = 
          CASE WHEN ( vaccion LIKE 'FORMACI_N' ) THEN NVL(estfinformacion, -1) ELSE NVL(estfinformacion, 0) END;
      if (vconteo = 0) then --no tiene los campos requeridos para finalizarse. Se devuelve al estado anterior
        BARS_CAMESTPDI( pformulario, vestado_anterior );
        pmsj := ' El PDI no puede finalizarse. Debe registrar el concepto sobre asistencia a la actividad de formaci�n. ';
      else
        SELECT evaluador into vevaluador
          FROM BARS_ENCPDI
          WHERE id = pformulario;
        if vevaluador IS NULL then --El administrador probablemente lo est� pasando de PLANEADO a FINALIZADO
          UPDATE BARS_ENCPDI SET evaluador = ( SELECT numero_identificacion FROM USUARIO WHERE upper(user_name) = upper(v('APP_USER')) )
            WHERE id = pformulario;
        end if;
        pmsj := ' El PDI fue finalizado. ';
      end if;*/
    ELSE
      NULL;
    END CASE;

--WHEN 'DELETE' THEN
 -- NULL;
WHEN 'CONCERTAR' THEN
  UPDATE BARS_ENCPDI SET evaluador = ( SELECT numero_identificacion FROM USUARIO WHERE upper(user_name) = upper(v('APP_USER')) ) 
    WHERE id = pformulario;
  BARS_CAMESTPDI(pformulario, kconcertado);
  pmsj := ' El PDI ha sido concertado. Cuando sea ejecutado, ingrese los datos faltantes y Final�celo. ';
WHEN 'REPROBAR_CONCERTACION' THEN
  UPDATE BARS_ENCPDI SET evaluador = v('APP_USER') WHERE id = pformulario;
  BARS_CAMESTPDI(pformulario, knoaprobado);
  pmsj := ' El PDI no ha sido aprobado.';

-- Esta acci�n se hace en la fase 2
/*WHEN 'FINALIZAR' THEN --Este bot�n no llama al proceso de almacenamiento. Solo administrador lo usa.
  if ( vaccion LIKE 'FORMACI_N' AND v('P78_ESTFINFORMACION') is null ) then --no tiene los campos requeridos para finalizarse
    pmsj := ' El PDI no puede finalizarse. Debe registrar el concepto sobre asistencia a la actividad de formaci�n. ';
  else
    UPDATE BARS_PDI 
      SET fecha_inicio = v('P78_FECHA_INICIO'), fecha_fin = v('P78_FECHA_FIN'), estfinformacion = v('P78_ESTFINFORMACION'),
        tutor = v('P78_TUTOR')
      WHERE id = pformulario;
    BARS_CAMESTPDI(pformulario, kfinalizado);
    pmsj := ' El PDI fue finalizado. ';
  end if;*/
ELSE
  pmsj := ' El PDI fue eliminado. ';
END CASE;

EXCEPTION
  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurri� en BARS_SUBMITPDI. Por favor, inf�rmelo al administrador de la aplicaci�n. ' 
      || SQLCODE || ' - ' || SQLERRM);
END BARS_SUBMITPDI;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALCAMESTFORMPRE" (pformulario IN BARS_FORMEVAPRE.id%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
 vestado_anterior VARCHAR2(50);
BEGIN
 --Obtener estado actual
 SELECT e.nombre into vestado_anterior
   from BARS_FORMEVAPRE f inner join ESTADO_BARS e on (f.estado = e.id)
   where f.id = pformulario;

 --Validar que el cambio sea a un estado anterior
 CASE upper(vestado_anterior)
   WHEN 'NUEVO' THEN --Imposible hacer el cambio a uno anterior
     preturn := 0;
	 pmensaje := 'No se puede realizar ning�n cambio de estado a un formulario en estado NUEVO. ';
   WHEN 'EN TRATAMIENTO' THEN --Solo a nuevo
     if upper(pestado) = 'NUEVO' then 
	   preturn := 1;
	   pmensaje := 'Se puede realizar el cambio de estado del formulario. ';
	 else
	   preturn := 0;
	   pmensaje := 'No se puede realizar el cambio de estado del formulario. Solo es v�lido el cambio a NUEVO para dicho formulario. ';
	 end if;
   WHEN 'FACTORES FINALIZADOS' THEN --A nuevo o en tratamiento
     if upper(pestado) IN ('NUEVO','EN TRATAMIENTO') then
	   preturn := 1;
	   pmensaje := 'Se puede realizar el cambio de estado del formulario. ';
	 else
	   preturn := 0;
	   pmensaje := 'No se puede realizar el cambio de estado del formulario. Solo es v�lido el cambio a NUEVO o EN TRATAMIENTO para dicho formulario. ';
	 end if;
   WHEN 'CERRADO' THEN --A nuevo, en tratamiento o factores finalizados
     if upper(pestado) IN ('NUEVO','EN TRATAMIENTO','FACTORES FINALIZADOS') then
	   preturn := 1;
	   pmensaje := 'Se puede realizar el cambio de estado del formulario. ';
	 else
	   preturn := 0;
	   pmensaje := 'No se puede realizar el cambio de estado del formulario. Solo es v�lido el cambio a NUEVO o EN TRATAMIENTO o FACTORES FINALIZADOS para dicho formulario. ';
	 end if;
   ELSE 
     preturn := 0;
	 pmensaje := 'No se puede realizar el cambio de estado del formulario. Su estado inicial es desconocido. ';
 END CASE;

EXCEPTION
 WHEN TOO_MANY_ROWS THEN
   preturn := 0;
   pmensaje := 'No se pudo validar el cambio de estado. Hay un problema de parametrizaci�n de estados. Contacte al administrador de la aplicaci�n. ';
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en BARS_VALCAMESTFORMPRE. Contacte al administrador
     de la aplicaci�n. ' || SQLCODE || ' - ' || SQLERRM || ' ');
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALCAMTRA" (pevaluado IN BARS_FORMEVAOBJ.evaluado%type, pnuevoestado IN TRABAJADOR.id_estado%type,
  pnuevogestor IN TRABAJADOR.jefe%type, pnuevagerencia IN TRABAJADOR.id_gerencia%type, pnuevocargo IN TRABAJADOR.id_cargo%type,
  pnuevaunidad IN TRABAJADOR.id_unidad_organizativa%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo NUMBER;
 vestado VARCHAR2(30);
 vgestor TRABAJADOR.jefe%TYPE;
 vgerencia TRABAJADOR.id_gerencia%type;
 vcargo TRABAJADOR.id_cargo%type;
 vunidad TRABAJADOR.id_unidad_organizativa%type;
 kinactivo CONSTANT VARCHAR2(30) := 'INACTIVO';

BEGIN
pmensaje := '';
SELECT DECODE(ID_ESTADO, 0, 'INACTIVO', 1 , 'ACTIVO' , 'INACTIVO') into vestado
 FROM TRABAJADOR
 WHERE numero_identificacion = pevaluado;

--Obtenci�n de datos actuales previo a la actualizaci�n del trabajador
SELECT jefe into vgestor
 FROM TRABAJADOR
 WHERE numero_identificacion = pevaluado;

SELECT id_gerencia into vgerencia
 FROM TRABAJADOR
 WHERE numero_identificacion = pevaluado;

SELECT id_cargo into vcargo
 FROM TRABAJADOR
 WHERE numero_identificacion = pevaluado;

SELECT id_unidad_organizativa into vunidad
 FROM TRABAJADOR
 WHERE numero_identificacion = pevaluado;

CASE 
 WHEN TRUE THEN --AJuste temporal para inactivar la validaci�n en la BD y no en la APP
   preturn := 1;

 WHEN vestado = kinactivo THEN --El trabajador se actualiza con estado Inactivo

    --Cambios relacionados con la existencia de evaluaciones de objetivos
    SELECT count(*) into vconteo
     FROM BARS_FORMEVAOBJ f inner join estado_bars on (f.estado = estado_bars.id) 
     WHERE f.evaluado = pevaluado and upper(estado_bars.nombre) not in ('ANULADO', 'FINALIZADO');
    if (vconteo != 0) then --El trabajador tiene formularios pendientes de anular o finalizar
     preturn := 0;
     pmensaje := pmensaje || 'El trabajador tiene formularios de objetivos sin finalizar. Por favor final�celos o an�lelos antes de inactivar al trabajador. ';
    else
     preturn := 1;
    end if;

 WHEN ( (vgerencia != pnuevagerencia) OR (vcargo != pnuevocargo) OR (vunidad != pnuevaunidad) ) THEN --Hay cambio en centro_costo (divisi�n), cargo o zona

   --Cambios relacionados con la existencia de evaluaciones de objetivos
    SELECT count(*) into vconteo
     FROM BARS_FORMEVAOBJ f inner join estado_bars on (f.estado = estado_bars.id) 
     WHERE f.evaluado = pevaluado and upper(estado_bars.nombre) not in ('ANULADO', 'FINALIZADO');
    if (vconteo != 0) then --El trabajador tiene formularios pendientes de anular o finalizar
     preturn := 0;
     pmensaje := pmensaje || 'El trabajador tiene formularios de objetivos sin finalizar. Por favor final�celos o an�lelos antes de modificar al trabajador. ';
    else
     preturn := 1;
    end if;

 WHEN (vgestor != pnuevogestor) THEN
   --Validar si el nuevo gestor tiene perfil de gestor en seguridad
   SELECT count(*) into vconteo 
    from FUNCION inner join GRUPO_FUNCION on FUNCION.id = GRUPO_FUNCION.funcion
    inner join GRUPO on GRUPO_FUNCION.grupo = GRUPO.id
    inner join USU_GRUPO on GRUPO.id = USU_GRUPO.grupo
    inner join USUARIO on USU_GRUPO.usuario = USUARIO.numero_identificacion
    where UPPER(FUNCION.nombre) = 'SER GESTOR' AND USUARIO.numero_identificacion = pnuevogestor;
       if (vconteo = 0) then --El nuevo gestor asignado no tiene el perfil gestor de seguridad
     preturn := 0;
     pmensaje := pmensaje || 'El gestor asignado al trabajador no tiene el perfil de autorizaci�n requerido para ser gestor. Ingrese a la administraci�n de seguridad y asigne al usuario a un grupo que tenga la funci�n Ser Gestor. ';
    else
     preturn := 1;
    end if;

 ELSE
  preturn := 1;
END CASE;

/*EXCEPTION
 WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurri� en BARS_VALCAMTRA. Por favor, inf�rmelo al administrador de la aplicaci�n. ' 
      || SQLCODE || ' - ' || SQLERRM);*/

END BARS_VALCAMTRA;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALCREAPDI" (pperiodo IN PDI_PERIODO.id%type,  preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo NUMBER;
 vestado_activo NUMBER;
BEGIN
 --Verificar estado del periodo
 SELECT count(*) into vconteo
  FROM PDI_PERIODO
  WHERE id = pperiodo and ESTADO = 1;

 IF vconteo = 1 then -- El periodo est� en estado ACTIVO
  pmensaje := 'Puede crear un nuevo PDI. El periodo est� activo. ';
  preturn := 1;
 ELSE
  pmensaje := 'El periodo est� inactivo. ';
  preturn := 0;
 END IF;

EXCEPTION
 WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurri� en BARS_VALCREAPDI. Por favor, inf�rmelo al administrador de la aplicaci�n. ' 
      || SQLCODE || ' - ' || SQLERRM);

END BARS_VALCREAPDI;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALESTFORMOBJ" (pformulario IN BARS_FORMEVAOBJ.id%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
BEGIN
 --Validar que el formulario est� en el estado solicitado
 SELECT count(*) into vconteo_filas
   from BARS_FORMEVAOBJ
   where id = pformulario and estado = ( SELECT id from ESTADO_BARS where upper(nombre) = upper(pestado)
     and tipo_estado = (SELECT id from TIPO_ESTADO_BARS where upper(nombre) = 'OBJETIVOS_BARS') );
 if vconteo_filas = 0 then
   preturn := 0;
   pmensaje := 'El formulario no se encuentra en el estado ' || pestado || '. ';
 else
   preturn := 1;
   pmensaje := 'El formulario se encuentra en el estado ' || pestado || '. ';
 end if;

EXCEPTION
 WHEN TOO_MANY_ROWS THEN
   preturn := 0;
   pmensaje := 'No se pudo validar si el formulario se encuentra en el estado ' || pestado || '.
     Hay un problema de parametrizaci�n de estados. Contacte al administrador de la aplicaci�n. ';
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en BARS_VALESTFORMOBJ. Contacte al administrador
     de la aplicaci�n. ' || SQLCODE || ' - ' || SQLERRM || ' ');
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALESTFORMPRE" (pformulario IN BARS_FORMEVAPRE.id%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
 ktipo_estado_predictores CONSTANT VARCHAR2(30) := 'PREDICTORES_POTENCIAL';
BEGIN
 --Validar que el formulario est� en el estado solicitado
 SELECT count(*) into vconteo_filas
   from BARS_FORMEVAPRE
   where id = pformulario and estado = ( SELECT id from ESTADO_BARS where upper(nombre) = upper(pestado)
     and tipo_estado = (SELECT id from TIPO_ESTADO_BARS where upper(nombre) = ktipo_estado_predictores) );
 if vconteo_filas = 0 then
   preturn := 0;
   pmensaje := 'El formulario no se encuentra en el estado ' || pestado || '. ';
 else
   preturn := 1;
   pmensaje := 'El formulario se encuentra en el estado ' || pestado || '. ';
 end if;

EXCEPTION
 WHEN TOO_MANY_ROWS THEN
   preturn := 0;
   pmensaje := 'No se pudo validar si el formulario se encuentra en el estado ' || pestado || '.
     Hay un problema de parametrizaci�n de estados. Contacte al administrador de la aplicaci�n. ';
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en BARS_VALESTFORMPRE. Contacte al administrador
     de la aplicaci�n. ' || SQLCODE || ' - ' || SQLERRM || ' ');
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALESTPERPRE" (pano IN BARS_PERPRE.ano%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
 knombre_tipo_estado CONSTANT VARCHAR2(20) := 'PERIODO_BARS';
BEGIN
 --Validar que exista tipo estado para PERIODO_BARS
 SELECT count(*) into vconteo_filas
   from TIPO_ESTADO_BARS where upper(nombre) = knombre_tipo_estado;
 if vconteo_filas = 0 then --El tipo estado no existe
   SELECT nvl( max(id), 0 ) + 1 into vtipo_estado
     from TIPO_ESTADO_BARS;
   INSERT INTO TIPO_ESTADO_BARS (id, nombre) VALUES (vtipo_estado, knombre_tipo_estado);
 else --El tipo estado est� una o m�s veces
   SELECT id into vtipo_estado
     from TIPO_ESTADO_BARS
     where upper(nombre) = knombre_tipo_estado and rownum = 1;
 end if;

 --Validar que exista el estado solicitado para PERIODO_BARS
 SELECT count(*) into vconteo_filas
   from ESTADO_BARS where upper(nombre) = upper(pestado) and tipo_estado = vtipo_estado;
 if vconteo_filas = 0 then --El estado no existe
   SELECT nvl( max(id), 0 ) + 1 into vestado
     from ESTADO_BARS;
   INSERT INTO ESTADO_BARS (id, nombre, tipo_estado) VALUES (vestado, upper(pestado), vtipo_estado);
 else --El estado existe una o m�s veces
   SELECT id into vestado
     from ESTADO_BARS
     where upper(nombre) = upper(pestado) and tipo_estado = vtipo_estado and rownum = 1;
 end if;

 --Validar que el periodo est� en el estado solicitado
 SELECT count(*) into vconteo_filas
   from BARS_PERPRE
   where ano = pano and estado = vestado;
 if vconteo_filas = 0 then --El periodo no est� en el estado suministrado
   preturn := 0;
   pmensaje := 'El periodo de evaluaci�n no est� en el estado: ' || pestado || '. ';
 else
   preturn := 1;
   pmensaje := 'El periodo de evaluaci�n est� en el estado: ' || pestado || '. ';
 end if;

EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en BARS_VALESTPERPRE. ' || SQLCODE || ' - '
     || SQLERRM);
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALEXIEVAPRE" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN BARS_PERPRE.ANO%type, ptipo IN BARS_FORMEVAPRE.TIPO%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 select count(*) into vconteo_filas
   from BARS_FORMEVAPRE
   where evaluado = pevaluado and periodo = pano and ptipo = tipo;
 if vconteo_filas = 0 then --La evaluaci�n no existe
   preturn := 0;
   pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' no tiene una evaluaci�n de predictores de potencial de tipo '
     || ptipo|| ' en el periodo ' || pano || '. ';
 else
   preturn := 1;
   pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' tiene una evaluaci�n de predictores de potencial de tipo ' 
     || ptipo || ' en el periodo ' || pano || '. ';
 end if;

EXCEPTION
  WHEN OTHERS THEN
    preturn := 0;
    pmensaje := 'Error inesperado en BARS_VALEXIEVAPRE. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BORRAR_BPR" (pusuario varchar2, pperiodo number, ptipo varchar2) as 
 lnumero_identificacion trabajador.numero_identificacion%type;
 lformulario number;
begin 
 /*Tipo puede ser EVALUACION O AUTOEVALUACION*/
 --select numero_identificacion into lnumero_identificacion from usuario where upper(user_name) = upper(pusuario);
 determinar_formulario(pusuario, pperiodo, UPPER(ptipo), lformulario);
 if lformulario is not null then
  delete respuesta where seccion_formulario in (select id from seccion_formulario where formulario = lformulario);
  delete seccion_formulario where formulario = lformulario;
  delete comentario_bars where formulario = lformulario;
  delete formulario_bars where id = lformulario;
 end if;
end;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CAL_PCT_CONSECUCION_FORMOBJ" ( i_formevaobj in BARS_FORMEVAOBJ.id%type ) as 
/*Calcula el porcentaje de consecuci�n promedio ponderado del formulario, 
transform�ndolo en base 100%, y actualiza el valor en la tabla*/
l_pctcons_crudo NUMBER(5,2); --Porcentaje de consecuci�n promedio para objetivos totales seg�n el peso original
l_pctcons_base100 NUMBER(5,2); --Porcentaje de consecuci�n promedio para objetivos totales en base 100%
l_peso NUMBER(5,2); --Peso en proporci�n de los objetivos totales en la evaluaci�n integral
l_periodo BARS_PEROBJ.ano%type;
l_evaluado BARS_FORMEVAOBJ.evaluado%type;
l_target_opr TIPO.id%type;

BEGIN
  --Calcular porcentaje consecuci�n ponderado para objetivos sin interpolaci�n
  select sum( (ponderacion/100)*pctconspond ) into l_pctcons_crudo
    from BARS_OBJETIVO
    where formevaobj = i_formevaobj;
  
  --Calcular peso de objetivos en la evaluaci�n del trabajador con su target actual
  select periodo, evaluado into l_periodo, l_evaluado
    from BARS_FORMEVAOBJ
    where id = i_formevaobj;
  select id_target_opr into l_target_opr
    from TRABAJADOR
    where numero_identificacion = l_evaluado;
  l_peso := VALIDACIONES_GUI.calc_peso_requerido_evaluacion( l_periodo, l_target_opr,
    SNW_CONSTANTES.get_id_tipo('OPR_ABI', 'TIPO_EVALUACION_TARGET') )
    + VALIDACIONES_GUI.calc_peso_requerido_evaluacion( l_periodo, l_target_opr,
    SNW_CONSTANTES.get_id_tipo('OPR_CER', 'TIPO_EVALUACION_TARGET') );

  --Interpolaci�n de la base de peso objetivos abiertos a 100
  l_pctcons_base100 := l_pctcons_crudo * (1/l_peso);
  UPDATE BARS_FORMEVAOBJ set pctconsform = l_pctcons_base100
    where id = i_formevaobj;

/* ! ! ! Se cambia por:  https://sinerware.atlassian.net/browse/GTH2019-81 ! ! !  -- Estado anterior:
  SELECT count(*) into l_conteo_filas
    from PCT_CONSECUCION_BARS
    where tipo_id = 60 and periodo = l_periodo AND l_Resultado_Ponderado BETWEEN media_comp_inf AND media_comp_sup;

    if l_conteo_filas = 1 then --Se determin� el porcentaje de consecuci�n  
      SELECT pct_consecucion into l_pctconspond
      from PCT_CONSECUCION_BARS
      where tipo_id = 60 and periodo = l_periodo AND l_Resultado_Ponderado BETWEEN media_comp_inf AND media_comp_sup;

      update BARS_FORMEVAOBJ set pctconsform = l_pctconspond where id=i_formulario;

    else --No se determin� un porcentaje de consecuci�n
      l_pctconspond := NULL;
    end if;
*/
EXCEPTION
  WHEN OTHERS THEN
    raise_application_error(-20000,'Error en el c�lculo de la consecuci�n total para objetivos del formulario: '||i_formevaobj);
end CAL_PCT_CONSECUCION_FORMOBJ;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CAL_PCT_CONSECUCION_OBJ" ( i_formulario in BARS_FORMEVAOBJ.id%type) as 
    l_periodo BARS_FORMEVAOBJ.periodo%type;
    l_conteo_filas number;
    l_pctconspond NUMBER;
begin


select periodo into l_periodo from BARS_FORMEVAOBJ where id = i_formulario;

for i in (select * from BARS_OBJETIVO where FORMEVAOBJ in (i_formulario) and
        id_tipo not in (select id_tipo from bars_objetivo where formevaobj=i_formulario and pctconsind 
                       is null and id_tipo in SNW_CONSTANTES.constante_tipo('CERRADO'))
    )
loop
  
    l_pctconspond := OBTENER_PORCENTAJE_CONSECUCION(i.PCTCONSIND);
        
    /* ! ! ! Se cambia por:  https://sinerware.atlassian.net/browse/GTH2019-81 ! ! !  -- Estado anterior:
   SELECT count(*) into l_conteo_filas
    from PCT_CONSECUCION_BARS
    where tipo_id = 60 and periodo = l_periodo AND i.PCTCONSIND BETWEEN media_comp_inf AND media_comp_sup;

    if l_conteo_filas = 1 then -- Se determin� el porcentaje de consecuci�n     
    SELECT pct_consecucion into l_pctconspond
      from PCT_CONSECUCION_BARS
      where tipo_id = 60 and periodo = l_periodo AND i.PCTCONSIND BETWEEN media_comp_inf AND media_comp_sup;  
      
      
  else --No se determin� un porcentaje de consecuci�n
  --Dbms_Output.Put_Line('   Porcentaje no determinado! :(');
    l_pctconspond := NULL;
  end if;
    */
    

  --Dbms_Output.Put_Line('   Persistiendo...');
  update BARS_OBJETIVO set pctconspond = l_pctconspond where id = i.id;

 
end loop;
end cal_pct_consecucion_obj;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CAL_PCT_PONDERADO_FORM" 
(i_formulario IN BARS_FORMEVAOBJ.id%type, limite in NUMBER)
is
l_resultado_promedio_crudo NUMBER;
l_resultado_promedio_base100 NUMBER;
l_trabajador trabajador%rowtype;
l_periodo NUMBER;
begin

--Carga la informacion del trabajador
select trabajador.*  into l_trabajador 
from BARS_FORMEVAOBJ left join trabajador on (BARS_FORMEVAOBJ.evaluado=Trabajador.Numero_Identificacion) 
where BARS_FORMEVAOBJ.id_target_opr not in (64) and BARS_FORMEVAOBJ.id=i_formulario;--Si el trabajador no tiene un target opr asociado entonces no aplica.
select periodo into l_periodo from BARS_FORMEVAOBJ where id = i_formulario;

--Si el trabajador tiene un target opr asignado, se procede a calcular el porcentaje de consecucion teniendo como restriccion la ponderacion que no puede ser menos o mas del 80%
select sum( (ponderacion/100)*pctconsind ) into l_resultado_promedio_crudo
    from BARS_OBJETIVO
    where formevaobj = i_formulario;
l_resultado_promedio_base100 := l_resultado_promedio_crudo * (100/limite);

/*20200311 CODIGO ANTERIOR guardado porque no entiendo el objetivo de ese c�digo
--Si el trabajador tiene un target opr asignado, se procede a calcular el porcentaje de consecucion teniendo como restriccion la ponderacion que no puede ser menos o mas del 80%
select 
(sum(bars_objetivo.ponderacion*nvl(bars_objetivo.pctconsind,8))*100)/(100*sum(bars_objetivo.ponderacion)) into l_resultado_promedio_crudo
from BARS_FORMEVAOBJ right join BARS_OBJETIVO on (BARS_OBJETIVO.FORMEVAOBJ=BARS_FORMEVAOBJ.id) 
where BARS_FORMEVAOBJ.id=i_formulario
group by BARS_FORMEVAOBJ.id 
having sum(bars_objetivo.ponderacion) between (limite-0.1) and (limite+0.1);
*/

--actualiza el resultado en el formulario (en 2019 se evidenci� un problema y por eso el parche
if ( substr(l_periodo,4) < 2019 ) then
  update BARS_FORMEVAOBJ set Resultado_Ponderado = l_resultado_promedio_crudo where id = i_formulario;
end if;

exception
when no_data_found then
if(l_trabajador.numero_identificacion is not null) then --Si el trabajador no tiene un target opr asociado entonces no aplica.
raise_application_error(-20000,'La ponderacion de objetivos no alcanza o excede el '||limite||'% para el formulario de objetivos: '||i_formulario);
end if;
end;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CAL_POSICION_BASE_MEDIA" 
(i_formulario IN formulario_bars.id%type)
is
l_formulario formulario_bars%rowtype;
l_posicion formulario_bars.posicion%type;
begin
select * into l_formulario from formulario_bars where id= I_FORMULARIO;
select nombre into l_posicion from bars_posicion where tipo_id = 61 and l_formulario.media_comportamientos between pct_inf and pct_sup and periodo=l_formulario.periodo;
update formulario_bars set posicion=l_posicion where id=l_formulario.id;
exception
when no_data_found then
    if(l_formulario.id is null) then
        raise_application_error(-20000,'No hay informacion del formulario: '||I_FORMULARIO);
    end if;
    if(l_posicion is null and l_formulario.id is not null) then
        raise_application_error(-20000,'No hay informacion en la tabla BARS_POSICION para el periodo: '||l_formulario.periodo||' y media de comportamientos '||l_formulario.media_comportamientos);
    end if;

end;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CAL_POSICION_OBJ" 
(
  i_formulario in BARS_FORMEVAOBJ.id%type 
) as 
l_periodo BARS_FORMEVAOBJ.periodo%type;
l_conteo_filas number;
l_posicion varchar2(50);
l_resultado_ponderado NUMBER;
begin
--Primero calcula el ponderado a nivel de formulario.
--CAL_PCT_PONDERADO_FORM(i_formulario);

select periodo,resultado_ponderado into l_periodo, l_resultado_ponderado from BARS_FORMEVAOBJ where id = i_formulario;


  SELECT count(*) into l_conteo_filas
    from BARS_POSICION
    where tipo_id = 60 and periodo = l_periodo AND l_resultado_ponderado BETWEEN pct_inf AND pct_sup;

    if l_conteo_filas = 1 then --Se determin� la posicion  
    SELECT nombre into l_posicion
      from BARS_POSICION
      where tipo_id = 60 and periodo = l_periodo AND l_resultado_ponderado BETWEEN pct_inf AND pct_sup;

      update BARS_FORMEVAOBJ set posicion= l_posicion where id=i_formulario;

  else --No se determin� la posicion
    l_posicion := NULL;
  end if;



end cal_posicion_obj;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CAMBIAR_ESTADO_FORM_BARS" (pformulario IN FORMULARIO_BARS.ID%TYPE, pestado IN ESTADO_BARS.NOMBRE%TYPE)
is
 vconteo_filas NUMBER;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
BEGIN
  --Verificar que el formulario exista
  select count(*) into vconteo_filas
  from FORMULARIO_BARS
  where id = pformulario;
  if vconteo_filas = 0 then --formulario inexistente
    null;
  else
    --Verificar tipo_estado_bars = 'FORMULARIO_BARS'
    select count(*) into vconteo_filas 
    from TIPO_ESTADO_BARS
    where upper(nombre) = 'FORMULARIO_BARS';
    if vconteo_filas >= 1 then --El tipo_estado_bars s� existe
      SELECT id into vtipo_estado
        from TIPO_ESTADO_BARS
        where upper(nombre) = 'FORMULARIO_BARS' and rownum = 1;
    else --El tipo_estado_bars no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into vtipo_estado
        from TIPO_ESTADO_BARS;
      INSERT INTO TIPO_ESTADO_BARS (id, nombre)
           values ( vtipo_estado, 'FORMULARIO_BARS' );
    end if;
    --Verificar estado deseado
    SELECT count(*) into vconteo_filas
      from ESTADO_BARS
      where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado);
    if vconteo_filas >= 1 then --El estado deseado existe
      SELECT id into vestado
        from ESTADO_BARS
        where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado) and rownum = 1;
    else --El estado deseado no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into vestado
        from ESTADO_BARS;
      INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
        values ( vestado, upper(pestado), vtipo_estado );
    end if;
    --Actualizar el estado del formulario  
    UPDATE FORMULARIO_BARS SET estado = vestado, fecha_evaluacion = trunc(sysdate) where id = pformulario;
  end if;
EXCEPTION
  when others then
    raise_application_error(-20000,'Un error inesperado ocurri� en CAMBIO_ESTADO_FORMS_BARS. 
      Favor contactar al administrador del sistema. ' ||SQLCODE||' - '||SQLERRM || ' ');
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CARGUE_GRUPOS_PDI" AS 
 l_conteo_curso number; 
 l_id_negocio number; 
 l_tipo number; 
 l_id_facultad number; 
 l_foco_holding number;
 l_foco_interno number;
 l_conteo_grupo number; 
 l_id_curso number;
 l_estado_grupo number; 
 l_tipo_grupo number; 
 l_periodo number;
 l_jurisdiccion number;
 l_rol number;
 
 
BEGIN

                                             -- > > > CARGUE DE CURSOS < < < --
    
-- Se itera sobre cada grupo en la tabla temporal
for i in (SELECT * FROM TEMP_CARGUE_GRUPO) loop
    begin

        select count(*) into l_conteo_curso from pdi_curso where upper(nombre_curso) = upper(i.nombre_curso); 

        if l_conteo_curso = 0 then 

            l_id_negocio := i.id_negocio;

            select tipo into l_id_facultad from tipo where id = l_id_negocio and tipo in (
            select id from tipo where tipo = SNW_CONSTANTES.constante_tipo('FACULTAD'));
                        
            
            select id into l_tipo from tipo where tipo = SNW_CONSTANTES.constante_tipo('TIPO_CURSO_PDI') and upper(nombre) = upper(i.tipo);                             
            select id into l_foco_holding from PDI_FOCO where upper(nombre) = upper(i.foco_holding);            
            select id into l_foco_interno from PDI_FOCO where upper(nombre) = upper(i.foco_interno);        
                        
            /*if l_id_negocio is not null and l_tipo is not null and l_id_facultad is not null then
                insert into PDI_CURSO (id_encargado,id_negocio,nombre_curso,nombre_visible,objetivo,tipo,id_facultad,CALIFICABLE_ASIST,FOCO_HOLDING,FOCO_INTERNO,provedor,horas,no_pdi) 
                values (i.encargado,l_id_negocio,i.nombre_curso,i.nombre_visible,i.objetivo,l_tipo,l_id_facultad,'S',l_foco_holding,l_foco_interno,i.proveedor,i.horas,i.no_pdi); 
            end if; */

        end if; 

        exception when others then 
            dbms_output.put_line('Curso: ' || i.nombre_curso||' - '||i.nombre_grupo||' = = = > '||' - '||sqlerrm);             
        end; 
end loop; 

                                    -- > > > CARGUE DE GRUPOS < < < --


-- Se itera sobre cada grupo en la tabla temporal
for i in (SELECT * FROM TEMP_CARGUE_GRUPO) loop

    begin
    
    select id into l_id_curso from pdi_curso where upper(nombre_curso) = upper(i.nombre_curso); 
    select id into l_periodo from pdi_periodo where ano = i.periodo; 
    select count(*) into l_conteo_grupo from pdi_grupo where upper(nombre_grupo) = upper(i.nombre_grupo) and id_pdi_curso = l_id_curso and id_periodo = l_periodo;

    if l_conteo_grupo = 0 then
        
        select id into l_estado_grupo from tipo where upper(constante) = upper(i.estado); 
        
        l_jurisdiccion := null;
        if i.jurisdiccion is not null then
        
            select id into l_jurisdiccion from tipo where upper(nombre) = upper(i.jurisdiccion);                         
        end if;
        
        l_rol := null;
        if i.rol is not null then --  TODO: !! Gestor por ahora !!. Generalizar para mas roles
            l_rol := 14;
        end if;
        /*
        if l_id_curso is not null then
            
            insert into pdi_grupo (id_pdi_curso,visible,nombre_grupo,estado,tipo_grupo,cupo_maximo,id_periodo,fecha_inicio,fecha_fin,cupo_minimo,cupo_analisis,id_tipo_jurisdiccion,id_rol) 
            values (l_id_curso,trim(i.visible),i.nombre_grupo,l_estado_grupo,decode(i.tipo_grupo,'ABIERTO_GRUPO',
            SNW_CONSTANTES.constante_tipo('CONFORMADO_GRUPO'),SNW_CONSTANTES.constante_tipo('LISTA_ESPERA_GRUPO')),i.cupo_maximo,l_periodo,
            to_date(i.fecha_inicio,'mm/dd/yyyy'),to_date(i.fecha_fin,'mm/dd/yyyy'),i.cupo_minimo,i.cupo_analisis,l_jurisdiccion,l_rol); 
        end if; 
        */
        
    end if; 
    exception when others then 
        dbms_output.put_line('Grupo: ' || i.nombre_curso||' - '||i.nombre_grupo|| ' - ' || i.jurisdiccion ||' = = = > '||' - '||sqlerrm);             
    end; 
end loop; 
END CARGUE_GRUPOS_PDI;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CARGUE_GRUPOS_PDI_LEGACY" AS 
 l_conteo_curso number; 
 l_id_negocio number; 
 l_tipo number; 
 l_id_facultad number; 
 l_conteo_grupo number; 
 l_id_curso number;
 l_estado_grupo number; 
 l_tipo_grupo number; 
 l_periodo number;
BEGIN

    -- > > > CARGUE DE CURSOS < < < --

-- Se itera sobre cada grupo en la tabla temporal
for i in (SELECT * FROM TEMP_CARGUE_GRUPO) loop
    begin
        l_conteo_curso := null; 
        l_id_negocio := null; 
        l_tipo := null; 
        l_id_facultad := null; 

        select count(*) into l_conteo_curso from pdi_curso where upper(nombre_curso)  = upper(i.nombre_curso); 

        if l_conteo_curso = 0 then 
            select id,tipo into l_id_negocio,l_id_facultad from tipo where upper(nombre) = upper(i.negocio) and tipo in (
            select id from tipo where tipo = SNW_CONSTANTES.constante_tipo('FACULTAD')); 

            select id into l_tipo from tipo where tipo = SNW_CONSTANTES.constante_tipo('TIPO_CURSO_PDI') and upper(nombre) = upper(i.tipo); 

            if l_id_negocio is not null and l_tipo is not null and l_id_facultad is not null then
                insert into PDI_CURSO (id_encargado,id_negocio,nombre_curso,nombre_visible,objetivo,tipo,id_facultad,CALIFICABLE_ASIST) 
                values (i.encargado,l_id_negocio,i.nombre_curso,i.nombre_visible,i.objetivo,l_tipo,l_id_facultad,'S'); 
            end if; 

        end if; 
        exception when others then 
            dbms_output.put_line(i.nombre_grupo||' - '||i.nombre_curso||' - '||i.encargado||' - '||sqlerrm); 
        end; 
end loop; 

    -- > > > CARGUE DE CURSOS < < < --


-- Se itera sobre cada grupo en la tabla temporal
for i in (SELECT * FROM TEMP_CARGUE_GRUPO) loop
    l_conteo_grupo := null; 
    l_estado_grupo := null; 

    begin

    select id into l_id_curso from pdi_curso where upper(nombre_curso) = upper(i.nombre_curso); 

    select count(*) into l_conteo_grupo from pdi_grupo where upper(nombre_grupo)  = upper(i.nombre_grupo) and id_pdi_curso = l_id_curso;

    if l_conteo_grupo = 0 then
        select id into l_estado_grupo from tipo where upper(constante) = upper(i.estado); 

        select id into l_periodo from pdi_periodo where ano = i.periodo; 

        if l_id_curso is not null then
            insert into pdi_grupo (id_pdi_curso,visible,nombre_grupo,estado,tipo_grupo,cupo_maximo,id_periodo,fecha_inicio,fecha_fin,cupo_minimo,cupo_analisis) 
            values (l_id_curso,trim(i.visible),i.nombre_grupo,l_estado_grupo,decode(i.tipo_grupo,'ABIERTO_GRUPO',
            SNW_CONSTANTES.constante_tipo('CONFORMADO_GRUPO'),SNW_CONSTANTES.constante_tipo('LISTA_ESPERA_GRUPO')),i.cupo_maximo,l_periodo,
            to_date(i.fecha_inicio,'mm/dd/yyyy'),to_date(i.fecha_fin,'mm/dd/yyyy'),i.cupo_minimo,i.cupo_analisis); 
        end if; 
    end if; 
    exception when others then 
        dbms_output.put_line(i.nombre_grupo||' - '||i.fecha_fin||' - '||i.fecha_inicio||' - '||sqlerrm); 
    end; 
end loop; 
END CARGUE_GRUPOS_PDI_LEGACY;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CARGUE_TRABAJADOR" IS 

CURSOR trabajadores IS SELECT * FROM TEMP_TRABAJADOR;
dummy NUMBER;

l_gerencia NUMBER;
l_cargo NUMBER;
l_unidad_organizativa NUMBER;
l_target_bpr NUMBER;
l_target_opr NUMBER;
l_tipo_regimen NUMBER;
l_empresa NUMBER;
l_nuevo_estado NUMBER;
l_jefe NUMBER;
l_email varchar2(4000);
l_identificacion number;
l_error varchar2(4000);

FUNCTION is_number (p_string IN VARCHAR2) RETURN INT
IS
   v_new_num NUMBER;
BEGIN
   if (p_string is null) then
     RETURN 2;
   else 
     v_new_num := TO_NUMBER(p_string);
   RETURN 1;
   end if;
EXCEPTION
WHEN VALUE_ERROR THEN
   RETURN 0;
END;

BEGIN
/*Esta funci�n opera sobre la tabla TEMP_ACTUALIZACIONTRA, la cual contiene cada uno de los trabajadores reportados por Codensa
con sus atributos iniciales, y los nuevos. Permite que la columna con los nuevos valores corresponda a: un nulo (en cuyo caso no se actualiza); 
al id del atributo referenciado (en cuyo caso se actualiza); o sea un texto, en cuyo caso se analiza si ya existe o debe crearse.
Si el trabajador no existe, se crea con los atributos reportados.*/

/*TODO: No maneja el valor nulo como indicador de no actualizar. Debe garantizarse que los valores a actualizar no sean nulos.
Se podr�a mejorar para que fuera un procedimiento reutilizable el que recorre columna a columna para crearla o identificarla
Uso de constantes para algunas variables.
*/

FOR c in trabajadores LOOP
 --Se empieza consultando el estado porque a los inactivos no vale la pena actualizarle ning�n dato.
 if upper(c.nuevo_estado) is not null then
 SELECT DECODE( upper(c.nuevo_estado), 'ACTIVO', 1, 'INACTIVO', 10, null) into l_nuevo_estado FROM DUAL;
 else 
 SELECT DECODE( upper(c.estado), 'ACTIVO', 1, 'INACTIVO', 10, null) into l_nuevo_estado FROM DUAL;
end if;

 if (l_nuevo_estado = 10) then -- Inactivo por tanto solo se actualiza el estado
   BEGIN
   UPDATE TRABAJADOR SET id_estado = l_nuevo_estado
     WHERE numero_identificacion = c.numero_identificacion;
   UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador inactivado' WHERE numero_identificacion = c.numero_identificacion;
   EXCEPTION
   WHEN NO_DATA_FOUND THEN --No existe pero como se va a inactivar no importa
     UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador inactivado' WHERE numero_identificacion = c.numero_identificacion;
   WHEN OTHERS THEN
     UPDATE TEMP_TRABAJADOR SET LOG = 'Error en actualizaci�n' WHERE numero_identificacion = c.numero_identificacion;
   END;

 elsif (l_nuevo_estado = 1) then --Los de estado Activo. Por tanto se actualizan o crean. 

 --Se empiezan creando las variables que contendr�n los valores de atributos referenciados para crear o actualizar el trabajador

 --gerencia
 if  ( is_number(trim(c.nueva_gerencia)) = 0 ) then --no es n�mero as� que: es nulo o el valor referenciado.
   BEGIN
     select id into l_gerencia
		from TIPO
		where tipo = 1 and upper(nombre) = upper(c.nueva_gerencia);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN 
	   l_gerencia := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_gerencia, upper(c.nueva_gerencia), 'A', 1, trunc(sysdate), 2);
	END;
 elsif ( is_number(trim(c.nueva_gerencia)) = 2 ) then
	l_gerencia := c.ID_GERENCIA; 
 else
   l_gerencia := c.nueva_gerencia; 
 end if;

 --cargo
 if ( is_number(trim(c.nuevo_cargo)) = 0 ) then --no es n�mero as� que o es el mismo valor o debe crearse
   BEGIN
   select id into l_cargo
		from TIPO
		where tipo = 3 and upper(nombre) = upper(c.nuevo_cargo);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN 
	 --Creaci�n del cargo
	   l_cargo := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_cargo, upper(c.nuevo_cargo), 'A', 3, trunc(sysdate), 2);
   END;
elsif ( is_number(trim(c.nuevo_cargo)) = 2 ) then
	l_cargo := c.ID_CARGO; 	 
 else
   l_cargo := c.nuevo_cargo; 
 end if;

 --unidad_organizativa
 if ( is_number(trim(c.nueva_unidad_organizativa)) = 0 ) then --no es n�mero as� que o es el mismo valor o debe crearse
   BEGIN
   select id into l_unidad_organizativa
		from TIPO
		where tipo = 2 and upper(nombre) = upper(c.nueva_unidad_organizativa);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Creaci�n de la unidad organizativa
	   l_unidad_organizativa := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_unidad_organizativa, upper(c.nueva_unidad_organizativa), 'A', 2, trunc(sysdate), 2);
   END;
elsif ( is_number(trim(c.nueva_unidad_organizativa)) = 2 ) then
	l_unidad_organizativa := c.ID_UNIDAD_ORGANIZATIVA; 
 else
   l_unidad_organizativa := c.nueva_unidad_organizativa; 
 end if;

 --target_bpr
 if ( is_number(trim(c.nuevo_target_bpr)) = 0 ) then --no es n�mero as� que o es el texto referenciado, o un target no manejado
   BEGIN
   select id into l_target_bpr
		from TIPO
		where tipo = snw_constantes.constante_tipo ('TARGET_BPR') and upper(nombre) = upper(c.nuevo_target_bpr);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Es un par�metro sensible, as� que no se puede crear sin control
	   l_target_bpr := null;
   END;
elsif ( is_number(trim(c.nuevo_target_bpr)) = 2 ) then
	l_target_bpr := c.ID_TARGET_BPR; 
 else --En la fuente viene el id del target_bpr
   l_target_bpr := c.nuevo_target_bpr; 
 end if;

 --target_opr
 if ( is_number(trim(c.nuevo_target_opr)) = 0 ) then --no es n�mero as� que o es el texto referenciado, o un target no manejado
   BEGIN
   select id into l_target_opr
		from TIPO
		where tipo = snw_constantes.constante_tipo ('TARGET_OPR') and upper(nombre) = upper(c.nuevo_target_opr);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Es un par�metro sensible, as� que no se puede crear sin control
	   l_target_opr := null;
   END;
elsif ( is_number(trim(c.nuevo_target_opr)) = 2 ) then
	l_target_opr := c.ID_TARGET_OPR; 
 else --En la fuente viene el id del target_opr
   l_target_opr := c.nuevo_target_opr; 
 end if;

 --tipo_r�gimen
 if ( is_number(trim(c.nuevo_tipo_regimen)) = 0 ) then --no es n�mero as� que o es el texto referenciado, o debe crearse (deuda t�cnica: est� creando filas en tipo para tipo_regimen con nombre null
   BEGIN
   select id into l_tipo_regimen
		from TIPO
		where tipo = snw_constantes.constante_tipo ('TIPO_REGIMEN') and upper(nombre) = upper(c.nuevo_tipo_regimen);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Creaci�n del tipo r�gimen
	   l_tipo_regimen := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_tipo_regimen, c.nuevo_tipo_regimen, 'A', snw_constantes.constante_tipo ('TIPO_REGIMEN'), trunc(sysdate), 2);
   END;
elsif ( is_number(trim(c.nuevo_tipo_regimen)) = 2 ) then
	BEGIN
	SELECT  id_tipo_regimen INTO l_tipo_regimen FROM TRABAJADOR 
	WHERE numero_identificacion = c.numero_identificacion;
	EXCEPTION
     	WHEN NO_DATA_FOUND THEN
	l_tipo_regimen:=NULL;
	END;


 else --En la fuente viene el id del tipo_regimen
   l_tipo_regimen := c.nuevo_tipo_regimen; 
 end if;

 --nueva empresa
 if ( is_number(trim(c.nueva_empresa)) = 0 ) then --no es n�mero as� que o es el mismo valor o debe crearse
	 BEGIN
	   select id into l_empresa
			from TIPO
			where tipo = 8 and upper(nombre) = upper(c.nueva_empresa);
	   EXCEPTION
		 WHEN NO_DATA_FOUND THEN
		   --Creaci�n de la empresa
		   l_empresa := tipo_seq.nextval;
		   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
			 values (l_empresa, upper(c.nueva_empresa), 'A', 8, trunc(sysdate), 2);
	   END;
elsif ( is_number(trim(c.nueva_empresa)) = 2 ) then
	l_empresa := c.ID_EMPRESA;
	 else
 l_empresa := c.nueva_empresa;
 end if;

 --jefe
 l_jefe := nvl(c.nuevo_num_asignado_jefe,c.jefe);

 --email
  if  ( c.nuevo_email is not null) then 
    l_email := c.nuevo_email;
  else
    l_email := c.email;
  end if;
--identificaci�n 
  if  ( c.nueva_identificacion is not null) then 
    l_identificacion := c.nueva_identificacion;
  else
    l_identificacion := c.identificacion;
  end if;

  BEGIN
   --Confirmar si el trabajador existe
   SELECT count(*) into dummy from TRABAJADOR where numero_identificacion = c.numero_identificacion;

   if ( dummy >= 1 ) then --El trabajador existe
	   UPDATE TRABAJADOR  
		SET id_gerencia = l_gerencia, id_cargo = l_cargo, id_unidad_organizativa = l_unidad_organizativa, id_target_bpr = l_target_bpr, id_target_opr = l_target_opr,
			 id_empresa = l_empresa, id_estado = l_nuevo_estado, email = l_email, jefe = l_jefe,identificacion = l_identificacion
		 WHERE numero_identificacion = c.numero_identificacion;
	   UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador actualizado' WHERE numero_identificacion = c.numero_identificacion;

	else --El trabajador no existe
       INSERT INTO TRABAJADOR (numero_identificacion, nombres, apellidos, id_cargo, jefe, id_estado, id_gerencia, id_unidad_organizativa, id_target_bpr, id_target_opr,
	     id_tipo_regimen, id_empresa, identificacion, email)
		 VALUES (c.numero_identificacion, c.nombres, c.apellidos, l_cargo, l_jefe, l_nuevo_estado, l_gerencia, l_unidad_organizativa, l_target_bpr, l_target_opr, 
		 l_tipo_regimen, l_empresa, l_identificacion, l_email);
         
    
         -- Se debe crear el PEC predeterminado        
         ADMIN_PEC_PCK.CREAR_PEC_PREDETERMINADO(c.numero_identificacion, sysdate, snw_constantes.constante_tipo('PEC_MOTIVO_INGRESO'));
                  
       UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador creado' WHERE numero_identificacion = c.numero_identificacion;
    end if; 

    EXCEPTION
  WHEN OTHERS THEN
	--dbms_output.put_line(SQLERRM||' identificacion:'||c.numero_identificacion||' error 1');
    l_error := SQLERRM||' identificacion:'||c.numero_identificacion||' error 1';
     UPDATE TEMP_TRABAJADOR SET LOG = 'Error en actualizaci�n - '||l_error WHERE numero_identificacion = c.numero_identificacion;
	 dbms_output.put_line( dbms_utility.format_error_backtrace );
  END;

 else --El estado es desconocido por tanto no se puede hacer ni actualizaci�n ni creaci�n
	--dbms_output.put_line(SQLERRM||' identificacion:'||c.numero_identificacion||' error '||l_nuevo_estado);
    l_error := SQLERRM||' identificacion:'||c.numero_identificacion||' error 1';
     UPDATE TEMP_TRABAJADOR SET LOG = 'Error en actualizaci�n - '||l_error WHERE numero_identificacion = c.numero_identificacion;

 end if;

 END LOOP;

END;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CARGUE_TRABAJADOR_JURISDICCION" IS 

l_id_jurisdiccion NUMBER;
l_conteo NUMBER;

BEGIN

/* Procedimiento que actualiza las jurisdicciones de los trabajadores segun la tabla TEMP_TRABAJADOR_JURISDICCION */
-- Se itera sobre cada grupo en la tabla temporal

    for i in (SELECT * FROM TEMP_TRABAJADOR_JURISDICCION) loop

        begin


            -- Se busca si el trabajador existe
            select count(*) into l_conteo from trabajador where NUMERO_IDENTIFICACION = i.asignado;

            if l_conteo > 0 then

                -- Obtener el id de tipo de la jurisdiccion que se va a actalizar
                select id into l_id_jurisdiccion from tipo where upper(nombre) = i.jurisdiccion;

                -- Se actualiza la jurisdiccion del trabajdor
                update trabajador set ID_TIPO_JURISDICCION = l_id_jurisdiccion where NUMERO_IDENTIFICACION = i.asignado;

            end if;  

            exception when others then 
                dbms_output.put_line('Error con el trabajador y jurisdiccion: ' || i.asignado||' - '||i.jurisdiccion||' = = = > '||' - '||sqlerrm);             
            end; 
    end loop; 
END;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CARGUE_TRABAJDOR" IS 

CURSOR trabajadores IS SELECT * FROM TEMP_TRABAJADOR;
dummy NUMBER;

l_gerencia NUMBER;
l_cargo NUMBER;
l_unidad_organizativa NUMBER;
l_target_bpr NUMBER;
l_target_opr NUMBER;
l_tipo_regimen NUMBER;
l_empresa NUMBER;
l_nuevo_estado NUMBER;
l_jefe NUMBER;
l_email varchar2(4000);
l_identificacion number;
l_error varchar2(4000);

FUNCTION is_number (p_string IN VARCHAR2) RETURN INT
IS
   v_new_num NUMBER;
BEGIN
   if (p_string is null) then
     RETURN 2;
   else 
     v_new_num := TO_NUMBER(p_string);
   RETURN 1;
   end if;
EXCEPTION
WHEN VALUE_ERROR THEN
   RETURN 0;
END;

BEGIN
/*Esta funci�n opera sobre la tabla TEMP_ACTUALIZACIONTRA, la cual contiene cada uno de los trabajadores reportados por Codensa
con sus atributos iniciales, y los nuevos. Permite que la columna con los nuevos valores corresponda a: un nulo (en cuyo caso no se actualiza); 
al id del atributo referenciado (en cuyo caso se actualiza); o sea un texto, en cuyo caso se analiza si ya existe o debe crearse.
Si el trabajador no existe, se crea con los atributos reportados.*/

/*TODO: No maneja el valor nulo como indicador de no actualizar. Debe garantizarse que los valores a actualizar no sean nulos.
Se podr�a mejorar para que fuera un procedimiento reutilizable el que recorre columna a columna para crearla o identificarla
Uso de constantes para algunas variables.
*/

FOR c in trabajadores LOOP
 --Se empieza consultando el estado porque a los inactivos no vale la pena actualizarle ning�n dato.
 if upper(c.nuevo_estado) is not null then
 SELECT DECODE( upper(c.nuevo_estado), 'ACTIVO', 1, 'INACTIVO', 10, null) into l_nuevo_estado FROM DUAL;
 else 
 SELECT DECODE( upper(c.estado), 'ACTIVO', 1, 'INACTIVO', 10, null) into l_nuevo_estado FROM DUAL;
end if;

 if (l_nuevo_estado = 10) then -- Inactivo por tanto solo se actualiza el estado
   BEGIN
   UPDATE TRABAJADOR SET id_estado = l_nuevo_estado
     WHERE numero_identificacion = c.numero_identificacion;
   UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador inactivado' WHERE numero_identificacion = c.numero_identificacion;
   EXCEPTION
   WHEN NO_DATA_FOUND THEN --No existe pero como se va a inactivar no importa
     UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador inactivado' WHERE numero_identificacion = c.numero_identificacion;
   WHEN OTHERS THEN
     UPDATE TEMP_TRABAJADOR SET LOG = 'Error en actualizaci�n' WHERE numero_identificacion = c.numero_identificacion;
   END;

 elsif (l_nuevo_estado = 1) then --Los de estado Activo. Por tanto se actualizan o crean. 

 --Se empiezan creando las variables que contendr�n los valores de atributos referenciados para crear o actualizar el trabajador

 --gerencia
 if  ( is_number(trim(c.nueva_gerencia)) = 0 ) then --no es n�mero as� que: es nulo o el valor referenciado.
   BEGIN
     select id into l_gerencia
		from TIPO
		where tipo = 1 and upper(nombre) = upper(c.nueva_gerencia);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN 
	   l_gerencia := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_gerencia, upper(c.nueva_gerencia), 'A', 1, trunc(sysdate), 2);
	END;
 elsif ( is_number(trim(c.nueva_gerencia)) = 2 ) then
	l_gerencia := c.ID_GERENCIA; 
 else
   l_gerencia := c.nueva_gerencia; 
 end if;

 --cargo
 if ( is_number(trim(c.nuevo_cargo)) = 0 ) then --no es n�mero as� que o es el mismo valor o debe crearse
   BEGIN
   select id into l_cargo
		from TIPO
		where tipo = 3 and upper(nombre) = upper(c.nuevo_cargo);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN 
	 --Creaci�n del cargo
	   l_cargo := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_cargo, upper(c.nuevo_cargo), 'A', 3, trunc(sysdate), 2);
   END;
elsif ( is_number(trim(c.nuevo_cargo)) = 2 ) then
	l_cargo := c.ID_CARGO; 	 
 else
   l_cargo := c.nuevo_cargo; 
 end if;

 --unidad_organizativa
 if ( is_number(trim(c.nueva_unidad_organizativa)) = 0 ) then --no es n�mero as� que o es el mismo valor o debe crearse
   BEGIN
   select id into l_unidad_organizativa
		from TIPO
		where tipo = 2 and upper(nombre) = upper(c.nueva_unidad_organizativa);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Creaci�n de la unidad organizativa
	   l_unidad_organizativa := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_unidad_organizativa, upper(c.nueva_unidad_organizativa), 'A', 2, trunc(sysdate), 2);
   END;
elsif ( is_number(trim(c.nueva_unidad_organizativa)) = 2 ) then
	l_unidad_organizativa := c.ID_UNIDAD_ORGANIZATIVA; 
 else
   l_unidad_organizativa := c.nueva_unidad_organizativa; 
 end if;

 --target_bpr
 if ( is_number(trim(c.nuevo_target_bpr)) = 0 ) then --no es n�mero as� que o es el texto referenciado, o un target no manejado
   BEGIN
   select id into l_target_bpr
		from TIPO
		where tipo = snw_constantes.constante_tipo ('TARGET_BPR') and upper(nombre) = upper(c.nuevo_target_bpr);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Es un par�metro sensible, as� que no se puede crear sin control
	   l_target_bpr := null;
   END;
elsif ( is_number(trim(c.nuevo_target_bpr)) = 2 ) then
	l_target_bpr := c.ID_TARGET_BPR; 
 else --En la fuente viene el id del target_bpr
   l_target_bpr := c.nuevo_target_bpr; 
 end if;

 --target_opr
 if ( is_number(trim(c.nuevo_target_opr)) = 0 ) then --no es n�mero as� que o es el texto referenciado, o un target no manejado
   BEGIN
   select id into l_target_opr
		from TIPO
		where tipo = snw_constantes.constante_tipo ('TARGET_OPR') and upper(nombre) = upper(c.nuevo_target_opr);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Es un par�metro sensible, as� que no se puede crear sin control
	   l_target_opr := null;
   END;
elsif ( is_number(trim(c.nuevo_target_opr)) = 2 ) then
	l_target_opr := c.ID_TARGET_OPR; 
 else --En la fuente viene el id del target_opr
   l_target_opr := c.nuevo_target_opr; 
 end if;

 --tipo_r�gimen
 if ( is_number(trim(c.nuevo_tipo_regimen)) = 0 ) then --no es n�mero as� que o es el texto referenciado, o debe crearse (deuda t�cnica: est� creando filas en tipo para tipo_regimen con nombre null
   BEGIN
   select id into l_tipo_regimen
		from TIPO
		where tipo = snw_constantes.constante_tipo ('TIPO_REGIMEN') and upper(nombre) = upper(c.nuevo_tipo_regimen);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Creaci�n del tipo r�gimen
	   l_tipo_regimen := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_tipo_regimen, c.nuevo_tipo_regimen, 'A', snw_constantes.constante_tipo ('TIPO_REGIMEN'), trunc(sysdate), 2);
   END;
elsif ( is_number(trim(c.nuevo_tipo_regimen)) = 2 ) then
	BEGIN
	SELECT  id_tipo_regimen INTO l_tipo_regimen FROM TRABAJADOR 
	WHERE numero_identificacion = c.numero_identificacion;
	EXCEPTION
     	WHEN NO_DATA_FOUND THEN
	l_tipo_regimen:=NULL;
	END;


 else --En la fuente viene el id del tipo_regimen
   l_tipo_regimen := c.nuevo_tipo_regimen; 
 end if;

 --nueva empresa
 if ( is_number(trim(c.nueva_empresa)) = 0 ) then --no es n�mero as� que o es el mismo valor o debe crearse
	 BEGIN
	   select id into l_empresa
			from TIPO
			where tipo = 8 and upper(nombre) = upper(c.nueva_empresa);
	   EXCEPTION
		 WHEN NO_DATA_FOUND THEN
		   --Creaci�n de la empresa
		   l_empresa := tipo_seq.nextval;
		   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
			 values (l_empresa, upper(c.nueva_empresa), 'A', 8, trunc(sysdate), 2);
	   END;
elsif ( is_number(trim(c.nueva_empresa)) = 2 ) then
	l_empresa := c.ID_EMPRESA;
	 else
 l_empresa := c.nueva_empresa;
 end if;

 --jefe
 l_jefe := nvl(c.nuevo_num_asignado_jefe,c.jefe);

 --email
  if  ( c.nuevo_email is not null) then 
    l_email := c.nuevo_email;
  else
    l_email := c.email;
  end if;
--identificaci�n 
  if  ( c.nueva_identificacion is not null) then 
    l_identificacion := c.nueva_identificacion;
  else
    l_identificacion := c.identificacion;
  end if;

  BEGIN
   --Confirmar si el trabajador existe
   SELECT count(*) into dummy from TRABAJADOR where numero_identificacion = c.numero_identificacion;

   if ( dummy >= 1 ) then --El trabajador existe
	   UPDATE TRABAJADOR  
		SET id_gerencia = l_gerencia, id_cargo = l_cargo, id_unidad_organizativa = l_unidad_organizativa, id_target_bpr = l_target_bpr, id_target_opr = l_target_opr,
			 id_empresa = l_empresa, id_estado = l_nuevo_estado, email = l_email, jefe = l_jefe,identificacion = l_identificacion
		 WHERE numero_identificacion = c.numero_identificacion;
	   UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador actualizado' WHERE numero_identificacion = c.numero_identificacion;

	else --El trabajador no existe
       INSERT INTO TRABAJADOR (numero_identificacion, nombres, apellidos, id_cargo, jefe, id_estado, id_gerencia, id_unidad_organizativa, id_target_bpr, id_target_opr,
	     id_tipo_regimen, id_empresa, identificacion, email)
		 VALUES (c.numero_identificacion, c.nombres, c.apellidos, l_cargo, l_jefe, l_nuevo_estado, l_gerencia, l_unidad_organizativa, l_target_bpr, l_target_opr, 
		 l_tipo_regimen, l_empresa, l_identificacion, l_email);
       UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador creado' WHERE numero_identificacion = c.numero_identificacion;
    end if; 

    EXCEPTION
  WHEN OTHERS THEN
	--dbms_output.put_line(SQLERRM||' identificacion:'||c.numero_identificacion||' error 1');
    l_error := SQLERRM||' identificacion:'||c.numero_identificacion||' error 1';
     UPDATE TEMP_TRABAJADOR SET LOG = 'Error en actualizaci�n - '||l_error WHERE numero_identificacion = c.numero_identificacion;
	 dbms_output.put_line( dbms_utility.format_error_backtrace );
  END;

 else --El estado es desconocido por tanto no se puede hacer ni actualizaci�n ni creaci�n
	--dbms_output.put_line(SQLERRM||' identificacion:'||c.numero_identificacion||' error '||l_nuevo_estado);
    l_error := SQLERRM||' identificacion:'||c.numero_identificacion||' error 1';
     UPDATE TEMP_TRABAJADOR SET LOG = 'Error en actualizaci�n - '||l_error WHERE numero_identificacion = c.numero_identificacion;

 end if;

 END LOOP;

END;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CREAR_NUEVA_CONCERTACION" (
    pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
    pano IN BARS_PEROBJ.ANO%type,
    pfecheva IN BARS_FORMEVAOBJ.FECHEVA%type,
    pformulario OUT BARS_FORMEVAOBJ.ID%type)
IS

 l_formulario_anterior NUMBER;
 l_consecutivo NUMBER;
 l_fecha_fin_periodo BARS_PEROBJ.fecha_fin%type;
 l_estado_nuevo NUMBER := 6; -- Estado NUEVO
 l_evaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 l_id_unidad_organizativa bars_formevaobj.id_unidad_organizativa%TYPE;
 l_id_gerencia bars_formevaobj.id_gerencia%TYPE; 
 l_id_target_opr bars_formevaobj.id_target_opr%type;
 l_cargo VARCHAR2(400);
 l_unidad VARCHAR2(400);
 l_fecha_inicio BARS_FORMEVAOBJ.fecha_inicio%type;
 l_fecha_fin BARS_FORMEVAOBJ.fecha_fin%type;


BEGIN

-- Partimos de recibir el evaluado y el periodo para crear una nueva concertacion en el mismo periodo para el mism otrabajador

 -- Se comenta, para que permita crear la concertacion aun en el periodo inactivo
 -- VAL_BARS_PEROBJ(pano, k_estado_activo, vconteo_filas, vmensaje);
 
 -- if (vconteo_filas = 1) then --El periodo es el activo.


 /* 
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO';
 k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO';
 k_tipo_estado_formulario CONSTANT VARCHAR2(20) := 'OBJETIVOS_BARS';

 
 
 */
     
     -- Llave primaria formulario a crear
     SELECT NVL( max(id),0 ) + 1 into pformulario
       from BARS_FORMEVAOBJ;

     --Datos del trabajador que determinan el formulario de objetivos
     SELECT jefe ,id_gerencia, id_unidad_organizativa, id_target_opr INTO l_evaluador, l_id_gerencia, l_id_unidad_organizativa, l_id_target_opr
       FROM TRABAJADOR
       WHERE numero_identificacion = pevaluado;

     -- Numero consecutivo correspondiente
     SELECT NVL(max(consecutivo),0) + 1 into l_consecutivo
       from BARS_FORMEVAOBJ 
       where evaluado = pevaluado and periodo = pano;

     -- Obtener la fecha fin del periodo recibido
     select fecha_fin into l_fecha_fin_periodo from BARS_PEROBJ where ANO = pano;

     -- C�lculo del cargo como nombre de la posicion vigente del evaluado y su Unidad vigente
     BEGIN
     select posicion, unidad_organizativa into l_cargo, l_unidad from snw_cargos.V_POSICION_UO
       where numero_identificacion = pevaluado;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         l_cargo := 'NO DETERMINADO';
         l_unidad := 'NO DETERMINADA';
     END;
     
     -- Obtener el formulario con consecutivo anterior
     select id into l_formulario_anterior from BARS_FORMEVAOBJ
       where evaluado = pevaluado and periodo = pano and consecutivo = l_consecutivo - 1;

     -- Se actualiza la fecha fin del formulario anterior
     UPDATE bars_formevaobj set fecha_fin = trunc(sysdate) - 1 where id = l_formulario_anterior;

     -- Calculo de la fecha inicio y fecha fin
     -- Se asume que la primera concertaci�n que se crea, siempre va tener el rango completo del periodo
     l_fecha_inicio := trunc(sysdate);
     l_fecha_fin := l_fecha_fin_periodo;

       
     -- Creaci�n formulario en estado nuevo
      INSERT INTO BARS_FORMEVAOBJ (id, evaluado, periodo, estado, fecheva, cargo, nombre_cargo, cencos, zona, id_gerencia, id_unidad_organizativa, nombre_unidad_organizativa, id_target_opr, consecutivo, fecha_inicio, fecha_fin)
        values (pformulario, pevaluado, pano, l_estado_nuevo, pfecheva, null, l_cargo, null, null, l_id_gerencia, l_id_unidad_organizativa, l_unidad, l_id_target_opr, l_consecutivo, l_fecha_inicio, l_fecha_fin);

EXCEPTION
when no_data_found then
    RAISE_APPLICATION_ERROR (-20000,'Error de datos en CREAR_NUEVA_CONCERTACION.' || SQLCODE || ' - ' || SQLERRM);
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en CREAR_NUEVA_CONCERTACION.' || SQLCODE || ' - ' || SQLERRM);

END;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."DETERMINAR_FORMULARIO" 
(pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN PERIODO_BARS.ANO%type, ptipo IN FORMULARIO_BARS.TIPO%type, pformulario OUT FORMULARIO_BARS.ID%type)
IS
 vconteo_filas NUMBER;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vplantilla PLANTILLA_FORMULARIO.ID%TYPE;
 vevaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO';
 k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO';
 k_tipo_estado_formulario CONSTANT VARCHAR2(20) := 'FORMULARIO_BARS';

 l_id_cargo TRABAJADOR.ID_CARGO%TYPE;
 l_id_gerencia TRABAJADOR.ID_GERENCIA%TYPE;
 l_id_unidad_organizativa TRABAJADOR.ID_UNIDAD_ORGANIZATIVA%TYPE;
BEGIN
  VAL_ESTADO_PERIODO(pano, k_estado_activo, vconteo_filas, vmensaje);
 if (vconteo_filas = 1) then --El periodo es el activo.
   VAL_ENCUESTA_EXISTE (pevaluado, pano, ptipo, vreturn, vmensaje);

   if (vreturn = 0) then --La evaluaci�n no existe. Crear formulario de acuerdo a la plantilla
     --Escogencia de la plantilla suponiendo que existe solo una para el colectivo (REQUIERE MEJORA EN SIGUIENTE VERSION)
     SELECT PLANTILLA_FORMULARIO.id into vplantilla
       from TRABAJADOR inner join tipo on (TRABAJADOR.ID_TARGET_BPR = tipo.id)
       inner join PLANTILLA_FORMULARIO on (tipo.id = PLANTILLA_FORMULARIO.TARGET_BPR)
       where TRABAJADOR.numero_identificacion = pevaluado;
     --Elecci�n del evaluador
     SELECT TRABAJADOR.jefe into vevaluador
       from TRABAJADOR
       where TRABAJADOR.NUMERO_IDENTIFICACION = pevaluado;
     --Verificaci�n existencia del estado 'NUEVO'
     --Verificar tipo_estado_bars = 'FORMULARIO_BARS'
     select count(*) into vconteo_filas 
       from TIPO_ESTADO_BARS
       where upper(nombre) = k_tipo_estado_formulario;
     if vconteo_filas >= 1 then --El tipo_estado_bars s� existe
       SELECT id into vtipo_estado
         from TIPO_ESTADO_BARS
         where upper(nombre) = k_tipo_estado_formulario and rownum = 1;
     else --El tipo_estado_bars no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vtipo_estado
         from TIPO_ESTADO_BARS;
       INSERT INTO TIPO_ESTADO_BARS (id, nombre)
         values ( vtipo_estado, k_tipo_estado_formulario );
     end if;
     --Verificar estado deseado
     SELECT count(*) into vconteo_filas
       from ESTADO_BARS
       where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo;
     if vconteo_filas >= 1 then --El estado 'NUEVO' existe
       SELECT id into vestado
         from ESTADO_BARS
         where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo and rownum = 1;
     else --El estado deseado no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vestado
         from ESTADO_BARS;
       INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
         values ( vestado, k_estado_nuevo, vtipo_estado );
     end if;
     --Llave primaria formulario a crear
     SELECT NVL( max(id),0 )+1 into pformulario
       from FORMULARIO_BARS;

       select id_cargo, id_gerencia, id_unidad_organizativa into l_id_cargo, l_id_gerencia, l_id_unidad_organizativa 
       from trabajador where numero_identificacion in (pevaluado);

     --Creaci�n formulario en estado nuevo
     INSERT INTO FORMULARIO_BARS (id, plantilla, evaluador, evaluado, periodo, estado, fecha_evaluacion, tipo, id_cargo, id_gerencia, id_unidad_organizativa)
       values (pformulario, vplantilla, vevaluador, pevaluado, pano, vestado, trunc(sysdate), ptipo,  l_id_cargo, l_id_gerencia, l_id_unidad_organizativa);
   else --La encuesta existe, retornar formulario
     SELECT id into pformulario
       from FORMULARIO_BARS
       where evaluado = pevaluado and periodo = pano and tipo = ptipo;
   end if;

 else --El periodo no es el activo
   VAL_ENCUESTA_EXISTE (pevaluado, pano, ptipo, vreturn, vmensaje);
   if (vreturn = 0) then --La evaluaci�n no existe. C�mo no es periodo activo no se puede crear.
     pformulario := NULL;
   else --La evaluaci�n existe
     SELECT id into pformulario
       from FORMULARIO_BARS
       where evaluado = pevaluado and periodo = pano and tipo = ptipo;
   end if;
 end if;
/*EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_FORMULARIO.' || SQLCODE || ' - ' || SQLERRM);*/
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."DETERMINAR_FORMULARIO_EVAL_OBJ" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,  pano IN BARS_PEROBJ.ANO%type, pformulario OUT BARS_FORMEVAOBJ.ID%type, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vevaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO'; k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO'; k_tipo_estado_formulario CONSTANT VARCHAR2(20) := 'OBJETIVOS_BARS'; vcargo TRABAJADOR.id_cargo%type;
 vcencos TRABAJADOR.cencos%type;
 vzona TRABAJADOR.zona%type;

/*DETERMINAR_FORMULARIO_EVAL_OBJ (APROBACION)
-Valida periodo activo
  SI ACTIVO
     -VAL_FORMULARIO_EXISTE
        SI NO EXISTE
             RETORNAR FALSO
        SI EXISTE
             RETORNAR FORMULARIO

  SI NO ACTIVO
      -VAL_FORMULARIO_EXISTE
        SI NO EXISTE
             RETORNAR FALSO
        SI EXISTE
             RETORNAR FORMULARIO
*/

BEGIN
 --Datos del trabajador que determinan el formulario de objetivos
 SELECT id_cargo, cencos, zona, jefe INTO vcargo, vcencos, vzona, vevaluador
   FROM TRABAJADOR
   WHERE numero_identificacion = pevaluado;

 VAL_BARS_PEROBJ(pano, k_estado_activo, vconteo_filas, vmensaje);
 if (vconteo_filas = 1) then --El periodo es el activo.
   VAL_FORMULARIO_EXISTE (pevaluado, pano, vreturn, vmensaje);
   if (vreturn = 0) then --La evaluci�n no existe.
     pmensaje := 'El formulario no existe. ' || vmensaje;     pformulario := 0;
   else --La evaluaci�n existe, retornar formulario
     SELECT id into pformulario
       from BARS_FORMEVAOBJ
       where evaluado = pevaluado and periodo = pano and cargo = vcargo and cencos = vcencos 
         and NVL(zona,0) = NVL(vzona,0);
       pmensaje := 'El formulario existe. ' || vmensaje;   end if;

 else --El periodo no es el activo
   VAL_FORMULARIO_EXISTE (pevaluado, pano, vreturn, vmensaje);
   if (vreturn = 0) then --La evaluaci�n no existe. Como no es periodo activo no se puede crear.
     pformulario := NULL;
   else --La evaluaci�n existe
     SELECT id into pformulario
       from BARS_FORMEVAOBJ
       where evaluado = pevaluado and periodo = pano and cargo = vcargo;
   end if;
 end if;

EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_FORMULARIO_EVAL_OBJ.' || SQLCODE || ' - ' || SQLERRM);END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."DETERMINAR_FORMULARIO_OBJ" (
    pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
    pano IN BARS_PEROBJ.ANO%type,     
    pconsecutivo IN NUMBER,   -- Si se recibe 0, es porque no hay evaluaciones o hay solo 1
                              -- Si se recibe mayor a 0, indica el numero del consecutivo para determinar
    pformulario OUT BARS_FORMEVAOBJ.ID%type)

IS

 vconteo_filas NUMBER;
 l_consecutivo NUMBER;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vevaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO';
 k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO';
 k_tipo_estado_formulario CONSTANT VARCHAR2(20) := 'OBJETIVOS_BARS';
 l_cargo VARCHAR2(400);
 l_unidad VARCHAR2(400);
 l_id_unidad_organizativa bars_formevaobj.id_unidad_organizativa%TYPE;
 l_id_gerencia bars_formevaobj.id_gerencia%TYPE; 
 l_id_target_opr bars_formevaobj.id_target_opr%type;
 l_fecha_inicio BARS_FORMEVAOBJ.fecha_inicio%type;
 l_fecha_fin BARS_FORMEVAOBJ.fecha_fin%type;

BEGIN

 -- Datos del trabajador que determinan el formulario de objetivos
 SELECT jefe,id_gerencia, id_unidad_organizativa, id_target_opr INTO vevaluador, l_id_gerencia, l_id_unidad_organizativa,l_id_target_opr
   FROM TRABAJADOR
   WHERE numero_identificacion = pevaluado;

 VAL_BARS_PEROBJ(pano, k_estado_activo, vconteo_filas, vmensaje);
 
 -- Si el consecutivo es 0, debe crearse o determinarse el unico existente
 if (pconsecutivo = 0) then

   if (vconteo_filas = 1) then -- El periodo es el activo.

   VAL_FORMULARIO_EXISTE (pevaluado, pano, vreturn, vmensaje);

   if (vreturn = 0) then -- La evaluaci�n no existe. Crear formulario
     --Verificaci�n existencia del estado 'NUEVO'
     --Verificar tipo_estado_bars = 'OBJETIVOS_BARS'
     select count(*) into vconteo_filas 
       from TIPO_ESTADO_BARS
       where upper(nombre) = k_tipo_estado_formulario;
     if vconteo_filas >= 1 then --El tipo_estado_bars s� existe
       SELECT id into vtipo_estado
         from TIPO_ESTADO_BARS
         where upper(nombre) = k_tipo_estado_formulario and rownum = 1;
     else --El tipo_estado_bars no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vtipo_estado
         from TIPO_ESTADO_BARS;
       INSERT INTO TIPO_ESTADO_BARS (id, nombre)
         values ( vtipo_estado, k_tipo_estado_formulario );
     end if;
     --Verificar estado deseado
     SELECT count(*) into vconteo_filas
       from ESTADO_BARS
       where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo;
     if vconteo_filas >= 1 then --El estado 'NUEVO' existe
       SELECT id into vestado
         from ESTADO_BARS
         where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo and rownum = 1;
     else --El estado deseado no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vestado
         from ESTADO_BARS;
       INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
         values ( vestado, k_estado_nuevo, vtipo_estado );
     end if;
     
     -- Llave primaria formulario a crear
     SELECT NVL( max(id),0 ) + 1 into pformulario
       from BARS_FORMEVAOBJ;

     -- C�lculo del cargo como nombre de la posicion vigente del evaluado y su Unidad vigente
     BEGIN
     select posicion, unidad_organizativa into l_cargo, l_unidad from snw_cargos.V_POSICION_UO
       where numero_identificacion = pevaluado;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         l_cargo := 'NO DETERMINADO';
         l_unidad := 'NO DETERMINADA';
     END;
     
     -- EL Numero consecutivo es 1 por ser la primera concertacion
     l_consecutivo := 1;      

     -- Calculo de la fecha inicio y fecha fin
     -- Dado que es la primera concertacion, ocupan todo el rango del periodo en cuestion
     select fecha_inicio into l_fecha_inicio from bars_perobj where ano = pano;
     select fecha_fin into l_fecha_fin from bars_perobj where ano = pano;

       
     -- Creaci�n formulario en estado nuevo
       
         /* Se cambia: (El cargo se desarticula de la creaci�n de objetivos: https://sinerware.atlassian.net/browse/GTH2019-90  
         INSERT INTO BARS_FORMEVAOBJ (id, evaluado, periodo, estado, fecheva, cargo, cencos, zona,id_gerencia, id_unidad_organizativa, evaluador, id_target_opr)
           values (pformulario, pevaluado, pano, vestado, trunc(sysdate), pcargo, null, null,l_id_gerencia, l_id_unidad_organizativa, vevaluador,l_id_target_opr);
         */
         
          -- Creaci�n formulario en estado nuevo
          INSERT INTO BARS_FORMEVAOBJ (id, evaluado, periodo, estado, fecheva, cargo, nombre_cargo, cencos, zona, id_gerencia, id_unidad_organizativa, nombre_unidad_organizativa, id_target_opr, consecutivo, fecha_inicio, fecha_fin)
            values (pformulario, pevaluado, pano, vestado, trunc(sysdate), null, l_cargo, null, null, l_id_gerencia, l_id_unidad_organizativa, l_unidad, l_id_target_opr, l_consecutivo, l_fecha_inicio, l_fecha_fin);
       
    else --La encuesta existe, retornar formulario
      SELECT max(id) into pformulario
        from BARS_FORMEVAOBJ
        where evaluado = pevaluado and periodo = pano;
    end if;

  else -- El periodo no es el activo
    VAL_FORMULARIO_EXISTE (pevaluado, pano, vreturn, vmensaje);
    if (vreturn = 0) then --La evaluaci�n no existe. C�mo no es periodo activo no se puede crear.
      pformulario := NULL;
    else -- La evaluaci�n existe
      SELECT max(id) into pformulario
        from BARS_FORMEVAOBJ
        where evaluado = pevaluado and periodo = pano;
    end if;
  end if;
 
 else -- Hay mas de una concertaci�n, se retorna la concertacion exacta con: periodo, evaluado, consecutivo
    SELECT max(id) into pformulario
      from BARS_FORMEVAOBJ
      where evaluado = pevaluado and periodo = pano and consecutivo = pconsecutivo;

 end if;

EXCEPTION
when no_data_found then
pformulario := null;
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_FORMULARIO_OBJ.' || SQLCODE || ' - ' || SQLERRM);
END;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."DML_SECCION_FORMULARIO" (pformulario IN
    FORMULARIO_BARS.ID%type, pseccion IN SECCION_BARS.ID%TYPE,
  ptipo IN SECCION_FORMULARIO.TIPO%TYPE, pnum_rtasx IN NUMBER, prta_1 IN RESPUESTA.valor%type DEFAULT NULL,
  prta_2 IN RESPUESTA.valor%type DEFAULT NULL, prta_3 IN RESPUESTA.valor%type DEFAULT NULL,
  prta_4 IN RESPUESTA.valor%type DEFAULT NULL, prta_5 IN RESPUESTA.valor%type DEFAULT NULL)
IS
 vseccion_formulario SECCION_FORMULARIO.ID%TYPE;
 vplantilla_pregunta PLANTILLA_PREGUNTAS.ID%TYPE;
 vnombre_parametro VARCHAR2(10);
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vconteo_filas NUMBER;
 vpromedio NUMBER;
 pnum_rtas NUMBER;
BEGIN
  --Determinar si la secci�n existe
  SELECT count(*) into vconteo_filas
    from SECCION_FORMULARIO
    where formulario = pformulario and seccion = pseccion and tipo = ptipo;


    select count(*) into pnum_rtas 
    from plantilla_preguntas 
    where plantilla_formulario in (select plantilla from formulario_bars where id=pformulario) 
    and seccion=pseccion;

  if vconteo_filas = 0 then --La secci�n no existe
  Dbms_Output.Put_Line('--La secci�n no existe');
    --Insertar la secci�n formulario
    SELECT NVL( max(id),0 )+1 into vseccion_formulario
      from SECCION_FORMULARIO;
    INSERT INTO SECCION_FORMULARIO (formulario, id, seccion, tipo)
      values (pformulario, vseccion_formulario, pseccion, ptipo);
    --Insertar las respuestas
    FOR i IN 1..pnum_rtas LOOP
    Dbms_Output.Put_Line('Loop '||i||' de '||pnum_rtas);
      --Determinar la plantilla de la pregunta
      SELECT PLANTILLA_PREGUNTAS.id into vplantilla_pregunta
        from FORMULARIO_BARS inner join PLANTILLA_FORMULARIO on (FORMULARIO_BARS.plantilla = PLANTILLA_FORMULARIO.id)
        inner join PLANTILLA_PREGUNTAS on (PLANTILLA_FORMULARIO.id = PLANTILLA_PREGUNTAS.plantilla_formulario)
        where FORMULARIO_BARS.id = pformulario and PLANTILLA_PREGUNTAS.seccion = pseccion and
        PLANTILLA_PREGUNTAS.codigo_pregunta = i;
      vnombre_parametro := 'prta_' || to_char(i);
      INSERT INTO RESPUESTA (id, seccion_formulario, plantilla_pregunta, valor)
        values ( respuesta_seq.nextval, vseccion_formulario, vplantilla_pregunta,
        CASE vnombre_parametro WHEN 'prta_1' then prta_1 WHEN 'prta_2' then prta_2 WHEN 'prta_3' then prta_3
        when 'prta_4' then prta_4 when 'prta_5' then prta_5 END);
    END LOOP;
  else --La secci�n ya existe
  Dbms_Output.Put_Line('La seccion ya existe ');
    --Obtener la secci�n formulario
    SELECT id into vseccion_formulario
      from SECCION_FORMULARIO
      where formulario = pformulario and seccion = pseccion and tipo = ptipo;
    --Actualizar las respuestas
     FOR i IN 1..pnum_rtas LOOP
      --Determinar la plantilla de la pregunta
      SELECT PLANTILLA_PREGUNTAS.id into vplantilla_pregunta
        from FORMULARIO_BARS inner join PLANTILLA_FORMULARIO on (FORMULARIO_BARS.plantilla = PLANTILLA_FORMULARIO.id)
        inner join PLANTILLA_PREGUNTAS on (PLANTILLA_FORMULARIO.id = PLANTILLA_PREGUNTAS.plantilla_formulario)
        where FORMULARIO_BARS.id = pformulario and PLANTILLA_PREGUNTAS.seccion = pseccion and
        PLANTILLA_PREGUNTAS.codigo_pregunta = i;
      vnombre_parametro := 'prta_' || to_char(i);
      UPDATE RESPUESTA
        SET valor = CASE vnombre_parametro WHEN 'prta_1' then prta_1 WHEN 'prta_2' then prta_2 WHEN 'prta_3' then prta_3
        when 'prta_4' then prta_4 when 'prta_5' then prta_5 END
        where seccion_formulario = vseccion_formulario and plantilla_pregunta = vplantilla_pregunta;
    END LOOP;
  end if;
  --Actualizar el promedio de la secci�n del formulario
  vpromedio := DETERMINAR_PROMEDIO_SECCION(vseccion_formulario);
  UPDATE SECCION_FORMULARIO
    set resultado = round(vpromedio,4)
    where id = vseccion_formulario;

-- ATRONCOSO 20150203 Se integra el cambio de estado en el dml

    if pseccion = 1 then
      CAMBIAR_ESTADO_FORM_BARS(pformulario, 'EN TRATAMIENTO');
    end If;
    if pseccion = 7 then    
      CAMBIAR_ESTADO_FORM_BARS(pformulario, 'FACTORES FINALIZADOS');
    end If;

 EXCEPTION WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DML_SECCION_FORMULARIO.' || SQLCODE || ' - ' || SQLERRM);
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."MOSTRAR_IMAGEN" (p_image_id IN NUMBER)
AS
   l_mime        VARCHAR2 (255);
   l_length      NUMBER;
   l_file_name   VARCHAR2 (2000);
   lob_loc       BLOB;
BEGIN
   SELECT mime_type, blob_content, file_name, DBMS_LOB.getlength (blob_content)
     INTO l_mime, lob_loc, l_file_name, l_length
     FROM TRABAJADOR
    WHERE NUMERO_IDENTIFICACION = p_image_id;

   OWA_UTIL.mime_header (NVL (l_mime, 'application/octet'), FALSE);
   HTP.p ('Content-length: ' || l_length);
   OWA_UTIL.http_header_close;
   WPG_DOCLOAD.download_file (lob_loc);
END MOSTRAR_IMAGEN;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."NUEVO_TIPO_REGIMEN" IS
l_tipo_regimen  varchar(100);
l_id_tipo_regimen  number;

BEGIN
---actualizar tipo de regimen de trabajadores  de acuerdo a datos en plantilla
    FOR i IN (SELECT ID, identificacion, tipo_regimen FROM TEMP_NUEVO_TIPO_REGIMEN)
    LOOP
    CASE
    WHEN i.tipo_regimen = 'Nuevo Convenio' THEN l_id_tipo_regimen:=75;
    WHEN i.tipo_regimen = 'R�gimen Integral' THEN l_id_tipo_regimen:= 73;
    WHEN i.tipo_regimen = 'Directivos Grupo' THEN l_id_tipo_regimen:= 74;
    ELSE NULL;
    END CASE;
    UPDATE trabajador
    SET id_tipo_regimen = l_id_tipo_regimen
    WHERE numero_identificacion = i.identificacion;
    END LOOP;
 ---actualizar tabla trabajadores   
    FOR i IN (SELECT ID, ID_TIPO_REGIMEN FROM TRABAJADOR)
    LOOP
    IF i.id_tipo_regimen = 1045 THEN  UPDATE trabajador
                                        SET id_tipo_regimen = 75
                                        WHERE id = i.id ;
    END IF;

    END LOOP;
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."PARAMETRIZAR_PERIODO_OPR" (p_periodo IN BARS_PEROBJ.ano%type) IS
--Parametrizaci�n de periodos de OPR GTH en CODENSA
--Premisa: los parametros se copian del periodo anterior y luego se editan si es necesario
--El periodo a parametrizar debe existir en BARS_PEROBJ
--Se depende del trigger de creaci�n de la llave primaria de los registros a insertar para RESTRICCION_TARGET y PCT_CONSECUCION_BARS
l_cuenta_bars_posicion NUMBER;

BEGIN
--RESTRICCION POR TARGET
insert into RESTRICCION_TARGET (id_target, id_tipo_evaluacion, porcentaje_total, pct_ind_min, pct_ind_max, periodo) 
  ( select id_target, id_tipo_evaluacion, porcentaje_total, pct_ind_min, pct_ind_max, p_periodo
      from RESTRICCION_TARGET
      where periodo = (p_periodo - 1) 
  );

/* ! ! ! Se elimina por:  https://sinerware.atlassian.net/browse/GTH2019-81 ! ! ! :
    --PCT CONSECUCION BARS
    insert into PCT_CONSECUCION_BARS (pct_consecucion, media_comp_inf, media_comp_sup, periodo, tipo_id)
    (
      select pct_consecucion, media_comp_inf, media_comp_sup, p_periodo, tipo_id 
      from PCT_CONSECUCION_BARS 
      where periodo = (p_periodo - 1)
    );
*/

--BARS_POSICION
select count(*) into l_cuenta_bars_posicion
  from BARS_POSICION
  where periodo = (p_periodo - 1);

insert into BARS_POSICION (id, nombre, pct_inf, pct_sup, periodo, tipo_id)
(
  select (id+l_cuenta_bars_posicion), nombre, pct_inf, pct_sup, p_periodo, tipo_id
  from bars_posicion 
  where periodo = (p_periodo - 1)
);

END;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."SET_AUTORIZACION" 
(
  i_numero_identificacion in number 
, i_nombre_grupo in varchar2
) as 
l_counter NUMBER;
l_id_grupo VARCHAR2(100);
l_msg VARCHAR2(200);

--Elecci�n del permiso
BEGIN
select id into l_id_grupo
 from GRUPO
 where upper(trim(nombre)) = upper(i_nombre_grupo);

--Registro del permiso
BEGIN
insert into usu_grupo (id, grupo, usuario) values (usu_grupo_seq.nextval, l_id_grupo, i_numero_identificacion);
l_msg:='El trabajador '||i_numero_identificacion||' ahora tiene perfil de ' || i_nombre_grupo;
DBMS_OUTPUT.PUT_LINE(l_msg);

EXCEPTION
 when DUP_VAL_ON_INDEX then 
   l_msg:='El usuario ya tiene el permiso';
   DBMS_OUTPUT.PUT_LINE(l_msg);
 when OTHERS then l_msg:='El usuario no existe';
   DBMS_OUTPUT.PUT_LINE(l_msg);
END;

EXCEPTION --No encontr� el permiso
 when NO_DATA_FOUND then 
   l_msg:='El permiso solicitado no existe';
   DBMS_OUTPUT.PUT_LINE(l_msg);
 when TOO_MANY_ROWS then 
   l_msg:='El permiso solicitado est� duplicado';
   DBMS_OUTPUT.PUT_LINE(l_msg);
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."SET_GESTOR" 
(
  i_numero_identificacion in number 
, msg out varchar2 
) as 
l_counter NUMBER;
begin
select count(*) into l_counter from usu_grupo where usuario in (i_numero_identificacion) and grupo=14;
if(l_counter<1) then
insert into usu_grupo values ((select max(id)+1 from usu_grupo), 14, i_numero_identificacion);
msg:='El trabajador '||i_numero_identificacion||' ahora tiene perfil de gestor!.';
else
msg:='El trabajador '||i_numero_identificacion||' ya tenia perfil de gestor!.';
end if;

end set_gestor;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."SNW_CREATE_USER" (
    p_NUMERO_ASIGNADO NUMBER )
IS
  l_trabajador trabajador%rowtype;
  l_password VARCHAR2(60);
BEGIN
   SELECT
    *
     INTO
    l_trabajador
     FROM
    TRABAJADOR
    WHERE
    NUMERO_IDENTIFICACION        = p_NUMERO_ASIGNADO;
  l_password := 1;
  -- ID de USUARIO se llena por el trigger
   INSERT INTO USUARIO (NUMERO_IDENTIFICACION,FECHA_EXPIRACION,USER_NAME,PASSWORD,FIRST_TIME)
    VALUES (l_trabajador.NUMERO_IDENTIFICACION, to_date('2099-12-31','YYYY-MM-DD'), 'CO'||l_trabajador.identificacion , l_password, 'Y');
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."SNW_CREATE_USER_CONTRATISTA" (
    p_cedula IN NUMBER )
IS
  l_contratista contratista%rowtype;
  l_user USUARIO%rowtype;
  l_password VARCHAR2(60);
  l_id_grupo NUMBER;

BEGIN
   SELECT * INTO l_contratista
     FROM
    CONTRATISTA
    WHERE NIT        = p_cedula;

  BEGIN
  -- ID de USUARIO se llena por el trigger
   INSERT INTO USUARIO (NUMERO_IDENTIFICACION,FECHA_EXPIRACION,USER_NAME,PASSWORD,FIRST_TIME)
    VALUES (l_contratista.nit, to_date('2099-12-31','YYYY-MM-DD'), to_char(l_contratista.nit) , '1', 'N');
  EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN --El usuario ya existe
   null;
  END;

  SELECT *
  INTO l_user
  FROM usuario
  WHERE user_name = to_char(l_contratista.nit);

  l_password :=  rawtohex(   sys.dbms_crypto.hash (  sys.utl_raw.cast_to_raw( l_contratista.nit||l_user.id||upper(to_char(l_contratista.nit)) ), 2  )   );

  update usuario set password = l_password where user_name = to_char(l_contratista.nit);

  --Por omisi�n un contratista es usuario de tipo Evaluador de Cliente Interno
  select id into l_id_grupo from grupo where trim(upper(nombre)) = 'EVALUADORES DE SERVICIO';

  BEGIN
    insert into usu_grupo (grupo, usuario) values (l_id_grupo, l_user.numero_identificacion);
  EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN --El usu_grupo ya existe
   null;
  END;

EXCEPTION
WHEN OTHERS THEN  
  dbms_output.put_line( dbms_utility.format_error_backtrace );
  raise_application_error(-20001, 'Error encontrado en crear usuario contratista - ' || SQLCODE || ' - ' || SQLERRM);

END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."SNW_RESET_PASSWORD" 
(
  i_numero_asignado in number 
, msg out varchar2 
) as 

l_random_password VARCHAR2(8);
l_username VARCHAR2(60);

Begin

l_random_password := snw_random_password;

select user_name into l_username from usuario where numero_identificacion=i_numero_asignado;

UPDATE usuario
  SET password=rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(l_random_password
    ||id
    ||l_username), 2 )),
    first_time  ='Y'
  WHERE user_name=l_username;

  msg:='Para '||l_username||' la nueva constrase�a es: '||l_random_password;

end snw_reset_password;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."SNW_SCRIPT_AUDITORIA_V1" (
    esquema      IN VARCHAR2 ,
    tabla_modelo IN VARCHAR2)
AS
  trigger_body     VARCHAR2(1000) := ' 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('|| chr(39)||'APP_USER'|| chr(39)||'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
:NEW.AUD_VERSION        := 1;  
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('|| chr(39)||'APP_USER'|| chr(39)||'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('|| chr(39)||'USERENV'|| chr(39)||','|| chr(39)||'IP_ADDRESS'|| chr(39)||');
:NEW.AUD_VERSION                := NVL(:OLD.AUD_VERSION,0)+1;
END;
/';

  -- script_resultado VARCHAR2(32766);
  
BEGIN
  --  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.
  --
  --    MODIFIED   (YYYY/MM/DD)
  --      atroncoso 2015-05-03 - Created
  --
  --    VERSION 1.0
  /* Procedimiento que crea una salida en dbms.output  las instrucciones necesarias para agregar las columnas de auditor�a
  a las tablas que no las tienen y agregarles un default. Para ejecutarlo ver las lineas de ejemplo al final del script*/
  
  --Forma de uso
  --BEGIN
  --snw_script_auditoria_v1(ESQUEMA =>'CODENSA_GTH', TABLA_MODELO => 'TIPO');
  --END;
  ----------------------------------------------------------------------------------------------------------------
  FOR l_table IN
  (SELECT table_name
  FROM all_tables
  WHERE table_name NOT LIKE '%APEX%'
  AND owner IN (esquema)
  )
  LOOP
    dbms_output.put_line('--***************** TABLA: ' || l_table.table_name);
    FOR model_table IN
    (SELECT table_name,
      column_name,
      DECODE(data_type,'VARCHAR2','VARCHAR2('
      ||data_length
      ||')',data_type) data_type,
      data_length
    FROM user_tab_columns
    WHERE table_name IN
      (SELECT object_name
      FROM user_objects
      WHERE object_type IN ('TABLE')
      AND object_name   IN (tabla_modelo)
      )
      AND column_name LIKE 'AUD_%'
    )
    LOOP
      DECLARE
        tabla_objetivo user_tab_columns%rowtype;
      BEGIN
        SELECT *
        INTO tabla_objetivo
        FROM user_tab_columns
        WHERE table_name IN
          (SELECT object_name
          FROM user_objects
          WHERE object_type IN ('TABLE')
          AND object_name   IN (l_table.table_name)
          )
        AND column_name = model_table.column_name
        AND data_type   = model_table.data_type;
      EXCEPTION
      WHEN no_data_found THEN
        dbms_output.put_line('--AGREGANDO COLUMNA: '||model_table.column_name);
        dbms_output.put_line('ALTER TABLE '||l_table.table_name||' ADD ( '||model_table.column_name||' '|| model_table.data_type||' );');
        IF model_table.column_name = 'AUD_FECHA_CREACION' THEN
          dbms_output.put_line('ALTER TABLE '||l_table.table_name||' MODIFY ( '||model_table.column_name||' DEFAULT '|| ' SYSDATE);');
        END IF;
        IF model_table.column_name = 'AUD_CREADO_POR' THEN
          dbms_output.put_line('ALTER TABLE '||l_table.table_name||' MODIFY ( '||model_table.column_name||' DEFAULT '|| ' ''HISTORICO'');');
        END IF;
        IF model_table.column_name = 'AUD_ACTUALIZADO_POR' THEN
          dbms_output.put_line('ALTER TABLE '||l_table.table_name||' MODIFY ( '||model_table.column_name||' DEFAULT '|| ' ''HISTORICO'');');
        END IF;
        IF model_table.column_name = 'AUD_FECHA_ACTUALIZACION' THEN
          dbms_output.put_line('ALTER TABLE '||l_table.table_name||' MODIFY ( '||model_table.column_name||' DEFAULT '|| ' SYSDATE);');
        END IF;
        IF model_table.column_name = 'AUD_TERMINAL_ACTUALIZACION' THEN
          dbms_output.put_line('ALTER TABLE '||l_table.table_name||' MODIFY ( '||model_table.column_name||' DEFAULT '|| ' ''IP_ADDRESS'');');
        END IF;
      END;
    END LOOP; --Fin del loop de model_table
    DECLARE
      counter NUMBER;
    BEGIN
      SELECT COUNT(*)
      INTO counter
      FROM user_source
      WHERE name IN
        (SELECT trigger_name
        FROM all_triggers
        WHERE owner    IN (esquema)
        AND table_name IN
          (SELECT table_name
          FROM user_tables
          WHERE table_name NOT LIKE '%APEX%'
          AND table_name IN (l_table.table_name)
          )
        )
      AND upper(text) LIKE '%AUD_%'
      GROUP BY name
      HAVING COUNT(*) > 0;
      dbms_output.put_line('-- Existe triggger de auditoria para : '||l_table.table_name);
      NULL;
    EXCEPTION
    WHEN no_data_found THEN
      dbms_output.put_line('--- Creando triggger de auditoria para : '||l_table.table_name);
      dbms_output.put_line('CREATE OR REPLACE TRIGGER BIU_AUD_'||l_table.table_name||' 
      BEFORE INSERT OR UPDATE ON '||l_table.table_name||' FOR EACH ROW'||trigger_body);
      --EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER BIU_AUD_'||l_table.table_name||' BEFORE INSERT OR UPDATE ON '||l_table.table_name||' FOR EACH ROW
      --'||trigger_body;
    END;
    -- Poblar columnas de auditoria
    dbms_output.put_line('update '||l_table.table_name || ' set AUD_CREADO_POR=''HISTORICO'', AUD_FECHA_CREACION=SYSDATE, AUD_ACTUALIZADO_POR=''HISTORICO'', AUD_FECHA_ACTUALIZACION=SYSDATE, AUD_TERMINAL_ACTUALIZACION=''IP_ADDRESS'', AUD_VERSION=1 ;');
    -- Dejarlas not null
    dbms_output.put_line('alter table '||l_table.table_name || ' modify (AUD_FECHA_CREACION not null, AUD_CREADO_POR NOT NULL, AUD_ACTUALIZADO_POR NOT NULL, AUD_FECHA_ACTUALIZACION NOT NULL, AUD_TERMINAL_ACTUALIZACION NOT NULL, AUD_VERSION NOT NULL);');
  END LOOP; --Fin del loop de todas las tablas
  dbms_output.put_line('commit; ');
END snw_script_auditoria_v1;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."SNW_UPDATE_PASSWORD" 
  (
    p_username VARCHAR2,
    p_password VARCHAR2
  )
IS
BEGIN
  UPDATE usuario
  SET password=rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(p_password
    ||id
    ||p_username), 2 )),
    first_time  ='Y'
  WHERE user_name=p_username;
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VALIDAR_TARGET" (
    i_trabajador  IN NUMBER,
    i_nuevo_target  IN NUMBER,
    i_mensaje OUT VARCHAR2)
IS

  l_conteo           NUMBER;
  l_periodo          NUMBER;
  l_target           NUMBER;
  l_pct_obj          NUMBER;
  l_pct_total        NUMBER;
  l_tipo_evaluacion  NUMBER;

BEGIN

    select ID_TARGET_OPR into l_target from trabajador where NUMERO_IDENTIFICACION = i_trabajador;
    
    -- Si el target en BD no cambio, no es necesario hacer la validaci�n de OPR
    if l_target = i_nuevo_target then
        i_mensaje := 'El target no cambia !!';
        return;
    end if;
            
    -- Se buscan los formularios del trabajador para periodos activos, y que esten en uno de los 3 estados se�alados
    select count(*) into l_conteo from BARS_FORMEVAOBJ 
    where EVALUADO = i_trabajador and 
    periodo in (select ano from BARS_PEROBJ WHERE ESTADO = 1) and -- Periodo activo
    estado in 
        (select ID from estado_bars where TIPO_ESTADO = 3 AND -- Objetivos 
        NOMBRE in ('CONCERTACION VALIDADA','CONCERTACION APROBADA','RESULTADO VALIDADO'));


    if l_conteo = 0 then
        i_mensaje := 'NO se encuentran formularios que se afecten. �Target actualizado!';
        return; 
    end if;

    -- Se encontro uno o mas formularios para los periodos activos. Se itera sobre cada uno el an�lisis
    FOR formulario IN
        (select id from BARS_FORMEVAOBJ 
        where EVALUADO = i_trabajador and 
        periodo in (select ano from BARS_PEROBJ WHERE ESTADO = 1) and -- Periodo activo
        estado in 
            (select ID from estado_bars where TIPO_ESTADO = 3 AND -- Objetivos 
            NOMBRE in ('CONCERTACION VALIDADA','CONCERTACION APROBADA','RESULTADO VALIDADO')))
    LOOP
        
        i_mensaje := 'SI HAY Objetivos por analizar';
    
        -- Objetivos abiertos 1010        
       -- FOR abierto IN
       --     (select id from BARS_OBJETIVO where FORMEVAOBJ = formulario.id and ID_TIPO in (1010))
       -- LOOP
        
        
            -- Se realiza la validacion de los objetivos actuales con el nuevo target           
            select periodo into l_periodo from bars_formevaobj where id = formulario.id;
            
            --Calcular ponderado de la concertaci�n para el tipo de evaluaci�n (tipo de objetivo). 
            -- La funci�n SUM retorna valores independiente del NO_DATA_FOUND
            select nvl( sum( nvl(ponderacion,0) ) , 0  )/100 into l_pct_obj
            from bars_objetivo 
            where formevaobj = formulario.id and bars_objetivo.id_tipo = 1010;
            
            -- Calcular ponderaci�n requerida para el tipo de evaluaci�n
            l_tipo_evaluacion := SNW_CONSTANTES.constante_tipo('OPR_ABI');
                                                  
            l_pct_total := VALIDACIONES_GUI.calc_peso_requerido_evaluacion(l_periodo, i_nuevo_target, l_tipo_evaluacion);
            
            --Validar con margen de tolerancia de 0.01
              if ( (l_pct_obj-0.01) <= l_pct_total and (l_pct_obj+0.01) >= l_pct_total ) then
                return;
              else
                return;
              end if;
        
 

    END LOOP;


END VALIDAR_TARGET;

  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_BARS_PEROBJ" (pano IN BARS_PEROBJ.ano%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
 vnombre_tipo_estado CONSTANT VARCHAR2(20) := 'PERIODO_BARS';
BEGIN
 --Validar que exista tipo estado para PERIODO_BARS
 SELECT count(*) into vconteo_filas
   from TIPO_ESTADO_BARS where upper(nombre) = vnombre_tipo_estado;
 if vconteo_filas = 0 then --El tipo estado no existe
   SELECT nvl( max(id), 0 ) + 1 into vtipo_estado
     from TIPO_ESTADO_BARS;
   INSERT INTO TIPO_ESTADO_BARS (id, nombre) VALUES (vtipo_estado, vnombre_tipo_estado);
 else --El tipo estado est� una o m�s veces
   SELECT id into vtipo_estado
     from TIPO_ESTADO_BARS
     where upper(nombre) = vnombre_tipo_estado and rownum = 1;
 end if;

 --Validar que exista el estado solicitado para PERIODO_BARS
 SELECT count(*) into vconteo_filas
   from ESTADO_BARS where upper(nombre) = upper(pestado) and tipo_estado = vtipo_estado;
 if vconteo_filas = 0 then --El estado no existe
   SELECT nvl( max(id), 0 ) + 1 into vestado
     from ESTADO_BARS;
   INSERT INTO ESTADO_BARS (id, nombre, tipo_estado) VALUES (vestado, upper(pestado), vtipo_estado);
 else --El estado existe una o m�s veces
   SELECT id into vestado
     from ESTADO_BARS
     where upper(nombre) = upper(pestado) and tipo_estado = vtipo_estado and rownum = 1;
 end if;

 --Validar que el periodo est� en el estado solicitado
 SELECT count(*) into vconteo_filas
   from BARS_PEROBJ
   where ano = pano and estado = vestado;
 if vconteo_filas = 0 then --El periodo no est� en el estado suministrado
   preturn := 0;
   pmensaje := 'El periodo de evaluaci�n no est� en el estado: ' || pestado || '. ';
 else
   preturn := 1;
   pmensaje := 'El periodo de evaluaci�n est� en el estado: ' || pestado || '. ';
 end if;

EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en VAL_ESTADO_PERIODO. ' || SQLCODE || ' - '
     || SQLERRM);
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_CAMBESTFORM_BARS" (pformulario IN FORMULARIO_BARS.id%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
 vestado_anterior VARCHAR2(50);
BEGIN
 --Obtener estado actual
 SELECT e.nombre into vestado_anterior
   from FORMULARIO_BARS f inner join ESTADO_BARS e on (f.estado = e.id)
   where f.id = pformulario;

 --Validar que el cambio sea a un estado anterior
 CASE upper(vestado_anterior)
   WHEN 'NUEVO' THEN --Imposible hacer el cambio a uno anterior
     preturn := 0;
	 pmensaje := 'No se puede realizar ning�n cambio de estado a un formulario en estado NUEVO. ';
   WHEN 'EN TRATAMIENTO' THEN --Solo a nuevo
     if upper(pestado) = 'NUEVO' then 
	   preturn := 1;
	   pmensaje := 'Se puede realizar el cambio de estado del formulario. ';
	 else
	   preturn := 0;
	   pmensaje := 'No se puede realizar el cambio de estado del formulario. Solo es v�lido el cambio a NUEVO para dicho formulario. ';
	 end if;
   WHEN 'FACTORES FINALIZADOS' THEN --A nuevo o en tratamiento
     if upper(pestado) IN ('NUEVO','EN TRATAMIENTO') then
	   preturn := 1;
	   pmensaje := 'Se puede realizar el cambio de estado del formulario. ';
	 else
	   preturn := 0;
	   pmensaje := 'No se puede realizar el cambio de estado del formulario. Solo es v�lido el cambio a NUEVO o EN TRATAMIENTO para dicho formulario. ';
	 end if;
   WHEN 'CERRADO' THEN --A nuevo, en tratamiento o factores finalizados
     if upper(pestado) IN ('NUEVO','EN TRATAMIENTO','FACTORES FINALIZADOS') then
	   preturn := 1;
	   pmensaje := 'Se puede realizar el cambio de estado del formulario. ';
	 else
	   preturn := 0;
	   pmensaje := 'No se puede realizar el cambio de estado del formulario. Solo es v�lido el cambio a NUEVO o EN TRATAMIENTO o FACTORES FINALIZADOS para dicho formulario. ';
	 end if;
   ELSE 
     preturn := 0;
	 pmensaje := 'No se puede realizar el cambio de estado del formulario. Su estado inicial es desconocido. ';
 END CASE;

EXCEPTION
 WHEN TOO_MANY_ROWS THEN
   preturn := 0;
   pmensaje := 'No se pudo validar el cambio de estado. Hay un problema de parametrizaci�n de estados. Contacte al administrador de la aplicaci�n. ';
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en VAL_CAMBESTFORM_BARS. Contacte al administrador
     de la aplicaci�n. ' || SQLCODE || ' - ' || SQLERRM || ' ');
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_CORRESP_EVALUADO" (pevaluador IN TRABAJADOR.numero_identificacion%type,
  pevaluado IN TRABAJADOR.numero_identificacion%type, pano IN PERIODO_BARS.ano%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
vconteo_filas NUMBER;
vmensaje VARCHAR2(4000);
BEGIN
 /*Ana determin� que para formularios antiguos, la edici�n la debe hacer el que actualmente figura como jefe
 --Determinaci�n si el periodo es el activo (o vigente)
 VAL_ESTADO_PERIODO (pano, 'ACTIVO', vconteo_filas, vmensaje);
 if (vconteo_filas = 0) then --El a�o no es el activo. Por tanto, la relaci�n debe buscarse    
                             --en el formulario del a�o suministrado.
   SELECT count(*) into vconteo_filas
     from FORMULARIO_BARS
     where periodo = pano and evaluador = pevaluador and evaluado = pevaluado;
   if vconteo_filas = 0 then --No hay correspondencia
     preturn := 0;
     pmensaje := 'No hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   else
     preturn := 1;
     pmensaje := 'Hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   end if;
 else --El a�o es el activo. Por tanto, la relaci�n debe buscarse entre jefes y empleados.
 */
   SELECT count(*) into vconteo_filas
     from TRABAJADOR EMP left join TRABAJADOR JEFE on (EMP.jefe = JEFE.NUMERO_IDENTIFICACION)
     where EMP.NUMERO_IDENTIFICACION = pevaluado and JEFE.NUMERO_IDENTIFICACION = pevaluador;
   if vconteo_filas = 0 then --No hay correspondencia
     preturn := 0;
     pmensaje := 'No hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   else
     preturn := 1;
     pmensaje := 'Hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   end if;
-- end if;
EXCEPTION
 WHEN NO_DATA_FOUND THEN
   preturn := 0;
   pmensaje := 'Faltan par�metros en la base de datos para verificar la correspondencia entre el evaluado y el evaluador. ';
 WHEN OTHERS THEN
   preturn := 0;
   pmensaje := 'Error inesperado. No se pudo verificar la correspondencia entre el evaluado y el evaluador. ' || SQLCODE
     || ' - ' || SQLERRM || ' ';
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_CORRESP_EVALUADOR_OBJ" (pevaluador IN TRABAJADOR.numero_identificacion%type,
  pevaluado IN TRABAJADOR.numero_identificacion%type, pano IN PERIODO_BARS.ano%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
vconteo_filas NUMBER;
vmensaje VARCHAR2(4000);
BEGIN
 /*25/08: Se analiz� con Ana Mar�a y en general debe validarse la correspondencia con el jefe actual
 --Determinaci�n si el periodo es el activo (o vigente)
 VAL_BARS_PEROBJ (pano, 'ACTIVO', vconteo_filas, vmensaje);
 if (vconteo_filas = 0) then --El a�o no es el activo. Por tanto, la relaci�n debe buscarse en el formulario del a�o suministrado.
   SELECT count(*) into vconteo_filas
     from BARS_FORMEVAOBJ
     where periodo = pano and evaluador = pevaluador and evaluado = pevaluado;
   if vconteo_filas = 0 then --No hay correspondencia
     preturn := 0;
     pmensaje := 'No hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   else
     preturn := 1;
     pmensaje := 'Hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   end if;
 else --El a�o es el activo. Por tanto, la relaci�n debe buscarse entre jefes y empleados.
 */
   SELECT count(*) into vconteo_filas
     from TRABAJADOR EMP left join TRABAJADOR JEFE on (EMP.jefe = JEFE.NUMERO_IDENTIFICACION)
     where EMP.NUMERO_IDENTIFICACION = pevaluado and JEFE.NUMERO_IDENTIFICACION = pevaluador;
   if vconteo_filas = 0 then --No hay correspondencia
     preturn := 0;
     pmensaje := 'No hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   else
     preturn := 1;
     pmensaje := 'Hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   end if;
 --end if;
EXCEPTION
 WHEN NO_DATA_FOUND THEN
   preturn := 0;
   pmensaje := 'Faltan par�metros en la base de datos para verificar la correspondencia entre el evaluado y el evaluador. ';
 WHEN OTHERS THEN
   preturn := 0;
   pmensaje := 'Error inesperado. No se pudo verificar la correspondencia entre el evaluado y el evaluador. ' || SQLCODE
     || ' - ' || SQLERRM || ' ';
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_ENCUESTA_EXISTE" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN PERIODO_BARS.ANO%type, ptipo IN FORMULARIO_BARS.TIPO%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 --Validar que el periodo no sea superior al a�o actual (REVISAR SI ES NECESARIO EN FASE II)
 if ( pano > extract( YEAR from sysdate ) ) then
   preturn := 0;
   pmensaje := 'Aun no existen encuestas para el periodo ' || pano || '. ';
 else --El periodo es igual o inferior al a�o actual
   select count(*) into vconteo_filas
     from FORMULARIO_BARS
     where evaluado = pevaluado and periodo = pano and ptipo = tipo;
   if vconteo_filas = 0 then --La evaluaci�n no existe
     preturn := 0;
     pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' no tiene una evaluaci�n de tipo ' || ptipo || ' en el periodo '
       || pano || '. ';
   else
     preturn := 1;
     pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' tiene una evaluaci�n de tipo ' || ptipo || ' en el periodo '
       || pano || '. ';
   end if;
 end if;

EXCEPTION
  WHEN OTHERS THEN
    preturn := 0;
    pmensaje := 'Error inesperado en VAL_ENCUESTA_EXISTE. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_ENCUESTA_NO_EXISTE" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN PERIODO_BARS.ANO%type, ptipo IN FORMULARIO_BARS.TIPO%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 select count(*) into vconteo_filas
   from FORMULARIO_BARS
   where evaluado = pevaluado and periodo = pano and ptipo = tipo;
 if vconteo_filas = 0 then --La evaluaci�n no existe
   preturn := 1;
   pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' no tiene una evaluaci�n de tipo ' || ptipo || ' en el periodo '
     || pano || '. ';
 else
   preturn := 0;
   pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' tiene una evaluaci�n de tipo ' || ptipo || ' en el periodo '
     || pano || '. ';
 end if;

EXCEPTION
  WHEN OTHERS THEN
    preturn := 0;
    pmensaje := 'Error inesperado en VAL_ENCUESTA_NO_EXISTE. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_ENC_COMPLETA" (pformulario IN FORMULARIO_BARS.ID%TYPE,
  preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vnum_preguntas NUMBER;
 vnum_respuestas NUMBER;
 CURSOR csecciones IS SELECT id, seccion from SECCION_FORMULARIO where formulario = pformulario;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 SELECT COUNT(*) into vnum_preguntas
   from FORMULARIO_BARS inner join PLANTILLA_FORMULARIO on (FORMULARIO_BARS.PLANTILLA = PLANTILLA_FORMULARIO.ID)
   inner join PLANTILLA_PREGUNTAS on (PLANTILLA_FORMULARIO.ID = PLANTILLA_PREGUNTAS.plantilla_formulario)
   where FORMULARIO_BARS.ID = pformulario;
 SELECT COUNT(*) into vnum_respuestas
   from SECCION_FORMULARIO inner join RESPUESTA on (SECCION_FORMULARIO.ID = RESPUESTA.seccion_formulario)
   where SECCION_FORMULARIO.FORMULARIO = pformulario;
 if (vnum_preguntas = vnum_respuestas) then --Encuesta con todas las respuestas
   preturn := 1; --Preasignaci�n para verificar si VAL_MAX_NO_APLICA cambia su valor
   pmensaje := 'Evaluaci�n con todas las secciones diligenciadas. ';
   --Validar m�ximo de No Aplicas
   FOR c1 IN csecciones LOOP
     VAL_MAX_NO_APLICA (c1.id, vreturn, vmensaje);
     if (vreturn = 0) then --No satisface m�ximo n�mero de no aplicas
       preturn := 0;
       pmensaje := pmensaje || 'La secci�n ' || c1.seccion || ' no satisface n�mero m�ximo de "No Aplicas". ';
     end if;
   END LOOP;
 else --Encuesta incompleta en respuestas
   preturn := 0;
   pmensaje := 'Evaluaci�n incompleta. Faltan respuestas por contestar. ';
 end if;
EXCEPTION
 WHEN OTHERS THEN
   preturn := 0;
   pmensaje := pmensaje || 'Error inesperado en VAL_ENC_COMPLETA. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_ESTADO_FORM_BARS" (pformulario IN FORMULARIO_BARS.id%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
BEGIN
 --Validar que el formulario est� en el estado solicitado
 SELECT count(*) into vconteo_filas
   from FORMULARIO_BARS
   where id = pformulario and estado = ( SELECT id from ESTADO_BARS where upper(nombre) = upper(pestado)
     and tipo_estado = (SELECT id from TIPO_ESTADO_BARS where upper(nombre) = 'FORMULARIO_BARS') );
 if vconteo_filas = 0 then
   preturn := 0;
   pmensaje := 'El formulario no se encuentra en el estado ' || pestado || '. ';
 else
   preturn := 1;
   pmensaje := 'El formulario se encuentra en el estado ' || pestado || '. ';
 end if;

EXCEPTION
 WHEN TOO_MANY_ROWS THEN
   preturn := 0;
   pmensaje := 'No se pudo validar si el formulario se encuentra en el estado ' || pestado || '.
     Hay un problema de parametrizaci�n de estados. Contacte al administrador de la aplicaci�n. ';
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en VAL_ESTADO_FORM_BARS. Contacte al administrador
     de la aplicaci�n. ' || SQLCODE || ' - ' || SQLERRM || ' ');
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_ESTADO_PERIODO" (pano IN PERIODO_BARS.ano%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
 vnombre_tipo_estado CONSTANT VARCHAR2(20) := 'PERIODO_BARS';
BEGIN
 --Validar que exista tipo estado para PERIODO_BARS
 SELECT count(*) into vconteo_filas
   from TIPO_ESTADO_BARS where upper(nombre) = vnombre_tipo_estado;
 if vconteo_filas = 0 then --El tipo estado no existe
   SELECT nvl( max(id), 0 ) + 1 into vtipo_estado
     from TIPO_ESTADO_BARS;
   INSERT INTO TIPO_ESTADO_BARS (id, nombre) VALUES (vtipo_estado, vnombre_tipo_estado);
 else --El tipo estado est� una o m�s veces
   SELECT id into vtipo_estado
     from TIPO_ESTADO_BARS
     where upper(nombre) = vnombre_tipo_estado and rownum = 1;
 end if;

 --Validar que exista el estado solicitado para PERIODO_BARS
 SELECT count(*) into vconteo_filas
   from ESTADO_BARS where upper(nombre) = upper(pestado) and tipo_estado = vtipo_estado;
 if vconteo_filas = 0 then --El estado no existe
   SELECT nvl( max(id), 0 ) + 1 into vestado
     from ESTADO_BARS;
   INSERT INTO ESTADO_BARS (id, nombre, tipo_estado) VALUES (vestado, upper(pestado), vtipo_estado);
 else --El estado existe una o m�s veces
   SELECT id into vestado
     from ESTADO_BARS
     where upper(nombre) = upper(pestado) and tipo_estado = vtipo_estado and rownum = 1;
 end if;

 --Validar que el periodo est� en el estado solicitado
 SELECT count(*) into vconteo_filas
   from PERIODO_BARS
   where ano = pano and estado = vestado;
 if vconteo_filas = 0 then --El periodo no est� en el estado suministrado
   preturn := 0;
   pmensaje := 'El periodo de evaluaci�n no est� en el estado: ' || pestado || '. ';
 else
   preturn := 1;
   pmensaje := 'El periodo de evaluaci�n est� en el estado: ' || pestado || '. ';
 end if;

EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en VAL_ESTADO_PERIODO. ' || SQLCODE || ' - '
     || SQLERRM);
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_FORMEVAOBJ" 
(
  i_formulario in bars_formevaobj.id%type, presultado in out NUMBER, pmsj in out VARCHAR2,
  ptipo in VARCHAR2 DEFAULT 'TOTAL'
) as 
l_formulario bars_formevaobj%rowtype;
esTargetT5 number; --0 No, Otro Si
kTipoCurvaOnOff CONSTANT VARCHAR2(100) := 'ON/OFF';
kTipoCurvaProject CONSTANT VARCHAR2(100) := '2 PROJECT';
vTipoCurva TIPO.nombre%type;
vTipoObjetivoAbierto BARS_OBJETIVO.id_tipo%type;
valPeso number;
valResultado number;
vPonderacionTotal number;

begin

select * into l_formulario from bars_formevaobj where bars_formevaobj.id=i_formulario;
--Obtener el id_tipo ABIERTO VIGENTE para hacer el filtro
select id into vTipoObjetivoAbierto
  FROM TIPO where tipo = ( select id from TIPO where upper(nombre) = 'TIPO_OBJETIVO' )
  and upper(nombre) = 'ABIERTO' and upper(estado) = 'A';
--Determinar si es targetT5
select count(*) into esTargetT5
  FROM TIPO 
    where id = l_formulario.id_target_opr and tipo = (
    SELECT id FROM TIPO WHERE UPPER(NOMBRE) = 'TARGET_OPR' ) and upper(nombre) IN ('T5', 'N/A');

presultado := 1; --Se empieza con la suposici�n que est�n correctos todos los objetivos

for l_bars_objetivo in (select * from bars_objetivo where formevaobj=i_formulario 
                        and id_tipo = CASE ptipo WHEN 'TOTAL' THEN id_tipo
                                                 WHEN 'ABIERTO' THEN vTipoObjetivoAbierto
                                                 ELSE id_tipo
                                      END)
loop

--Determinar tipo curva
BEGIN
select nombre into vTipoCurva
  FROM TIPO
    where id = l_bars_objetivo.id_tipo_curva;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    vTipoCurva := '';
END;
----Validaciones por objetivo---------/*

if ( esTargetT5=0 ) then --No es T5. 

  --El resultado depende del tipo curva. Si es OnOff -> (0,10) TODO: Parametrizar por Target. Como parche, se ajusta manual a 99
  if ( vTipoCurva = kTipoCurvaOnOff ) then
      if ( l_bars_objetivo.PCTCONSIND between 0 and 99 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores [0 - 99] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Si tipo de curva es Project -> {8,10} TODO: Parametrizar por Target. Como parche, se ajusta manual a 99
  elsif ( vTipoCurva = kTipoCurvaProject ) then
      if ( l_bars_objetivo.PCTCONSIND between 0 and 99 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores [0 - 99] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Otro tipo de curva -> [0,10] TODO: Parametrizar por Target. Como parche, se ajusta manual a 99
  else
      if ( l_bars_objetivo.PCTCONSIND between 0 and 99 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en el intervalo [0 - 99] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  end if;

else --Es T5

  --El resultado depende del tipo curva. Si es OnOff -> (0,10) 
  if ( vTipoCurva = kTipoCurvaOnOff ) then
      if ( l_bars_objetivo.PCTCONSIND between 0 and 10 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores [0 - 10] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Si tipo de curva es Project -> {8,10}
  elsif ( vTipoCurva = kTipoCurvaProject ) then
      if ( l_bars_objetivo.PCTCONSIND between 0 and 10 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores [0 - 10] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Otro tipo de curva -> [0,10]
  else
      if ( l_bars_objetivo.PCTCONSIND between 0 and 10 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en el intervalo [0 - 10] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  end if;

end if;

----Validaciones por Objetivo---------*/

if ( valResultado = 0 ) then
    presultado := 0;
end if;

end loop;


EXCEPTION
  WHEN OTHERS THEN
    presultado := 0;
    pmsj := 'Error inesperado en VAL_FORMEVAOBJ. ' || SQLCODE || ' - ' || SQLERRM || ' ';

end val_formevaobj;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_FORMEVAOBJ2" 
(
  i_formulario in bars_formevaobj.id%type, presultado in out NUMBER, pmsj in out VARCHAR2,
  ptipo in VARCHAR2 DEFAULT 'TOTAL'
) as 
l_formulario bars_formevaobj%rowtype;
esTargetT5 number; --0 No, Otro Si
kTipoCurvaOnOff CONSTANT VARCHAR2(100) := 'ON/OFF';
kTipoCurvaProject CONSTANT VARCHAR2(100) := '2 PROJECT';
vTipoCurva TIPO.nombre%type;
vTipoObjetivoAbierto BARS_OBJETIVO.id_tipo%type;
valPeso number;
valResultado number;
vPonderacionTotal number;

begin

select * into l_formulario from bars_formevaobj where bars_formevaobj.id=i_formulario;
--Obtener el id_tipo ABIERTO VIGENTE para hacer el filtro
select id into vTipoObjetivoAbierto
  FROM TIPO where tipo = ( select id from TIPO where upper(nombre) = 'TIPO_OBJETIVO' )
  and upper(nombre) = 'ABIERTO' and upper(estado) = 'A';
--Determinar si es targetT5
select count(*) into esTargetT5
  FROM TIPO 
    where id = l_formulario.id_target_opr and tipo = (
    SELECT id FROM TIPO WHERE UPPER(NOMBRE) = 'TARGET_OPR' ) and upper(nombre) IN ('T5', 'N/A');

presultado := 1; --Se empieza con la suposici�n que est�n correctos todos los objetivos

for l_bars_objetivo in (select * from bars_objetivo where formevaobj=i_formulario 
                        and id_tipo = CASE ptipo WHEN 'TOTAL' THEN id_tipo
                                                 WHEN 'ABIERTO' THEN vTipoObjetivoAbierto
                                                 ELSE id_tipo
                                      END)
loop

--Determinar tipo curva
BEGIN
select nombre into vTipoCurva
  FROM TIPO
    where id = l_bars_objetivo.id_tipo_curva;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    vTipoCurva := '';
END;
----Validaciones por objetivo---------/*

if ( esTargetT5=0 ) then --No es T5. 

  --La ponderaci�n debe estar entre 10 y 30
  if ( (l_bars_objetivo.ponderacion between 10 and 30) ) then
    valPeso := 1;
  else
    valPeso := 0;
    pmsj := pmsj || 'El peso debe estar entre 10 y 30 para el objetivo: ' || l_bars_objetivo.objnum || '. ';
  end if;
  --El resultado depende del tipo curva. Si es OnOff -> (0,10)
  if ( vTipoCurva = kTipoCurvaOnOff ) then
      if ( l_bars_objetivo.pctconsind in (0,10)) or (l_bars_objetivo.pctconsind is null) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores {0,10} para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Si tipo de curva es Project -> {8,10}
  elsif ( vTipoCurva = kTipoCurvaProject ) then
      if ( l_bars_objetivo.pctconsind in (8,10)) or (l_bars_objetivo.pctconsind is null) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores {8,10} para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Otro tipo de curva -> [0,10]
  else
      if ( (l_bars_objetivo.PCTCONSIND between 0 and 10) or (l_bars_objetivo.pctconsind is null)) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en el intervalo [0,10] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  end if;

else --Es T5

  --La ponderaci�n debe estar entre 10 y 100
  if ( (l_bars_objetivo.ponderacion between 10 and 100) ) then
    valPeso := 1;
  else
    valPeso := 0;
    pmsj := pmsj || 'El peso debe estar entre 10 y 100 para el objetivo: ' || l_bars_objetivo.objnum || '. ';
  end if;
  --El resultado depende del tipo curva. Si es OnOff -> (0,10)
  if ( vTipoCurva = kTipoCurvaOnOff ) then
      if ( l_bars_objetivo.pctconsind in (0,10)) or (l_bars_objetivo.pctconsind is null) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores {0,10} para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Si tipo de curva es Project -> {8,10}
  elsif ( vTipoCurva = kTipoCurvaProject ) then
      if ( l_bars_objetivo.pctconsind in (8,10)) or (l_bars_objetivo.pctconsind is null) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores {8,10} para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Otro tipo de curva -> [0,10]
  else
      if (( l_bars_objetivo.PCTCONSIND between 0 and 10) or (l_bars_objetivo.pctconsind is null)) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en el intervalo [0,10] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  end if;

end if;

----Validaciones por Objetivo---------*/

if ( valPeso = 0 or valResultado = 0 ) then
    presultado := 0;
end if;

end loop;

----Validaciones de ponderaci�n total---------
select sum(ponderacion) into vPonderacionTotal
    from BARS_OBJETIVO
    where formevaobj = l_formulario.id;

if ( esTargetT5=0 ) then --No es T5.

    --Se valida que la ponderaci�n sea el 80%
  if ( vPonderacionTotal != 80 ) then
    presultado := 0;
    pmsj := pmsj || 'El peso de los objetivos cerrados y abiertos debe sumar en total 80%. Comun�quese con el contacto de la aplicaci�n para que lo modifiquen. ';  
  end if;

else --Es T5
  if ( vPonderacionTotal != 100 ) then
    presultado := 0;
    pmsj := pmsj || 'El peso de los objetivos cerrados y abiertos debe sumar en total 100%. Comun�quese con el contacto de la aplicaci�n para que lo modifiquen. ';  
  end if;

end if;

EXCEPTION
  WHEN OTHERS THEN
    presultado := 0;
    pmsj := 'Error inesperado en VAL_FORMEVAOBJ. ' || SQLCODE || ' - ' || SQLERRM || ' ';

end val_formevaobj2;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_FORMULARIO_EXISTE" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,  pano IN BARS_PEROBJ.ANO%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 --Validar que el periodo no sea superior al a�o actual (eliminar si se va a concertar anticipado)
 if ( SUBSTR(pano,1,4) > extract( YEAR from sysdate ) ) then   preturn := 0;
   pmensaje := 'Aun no existen concertaciones para el periodo ' || pano || '. '; else --El periodo es igual o inferior al a�o actual
   --Determinar si el periodo es el activo (se divide para que el mensaje sea m�s apropiado)
    VAL_PEROBJ (pano, 1, vconteo_filas, vmensaje);
   if (vconteo_filas = 0) then --El a�o no es el activo.
     select count(*) into vconteo_filas
       from BARS_FORMEVAOBJ
       where evaluado = pevaluado and periodo = pano;
     if vconteo_filas = 0 then --La evaluaci�n no existe
       preturn := 0;
       pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' no fue evaluado en el periodo: '         || pano || '.';     else
       preturn := 1;
       pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' fue evaluado en el periodo '         || pano || '.';     end if;
   else --El a�o es el activo
     select count(*) into vconteo_filas
       from BARS_FORMEVAOBJ
       where evaluado = pevaluado and periodo = pano;
     if vconteo_filas = 0 then --La evaluaci�n no existe
       preturn := 0;
       pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' no ha concertado en el periodo '         || pano || '.';     else
       preturn := 1;
       pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' tiene una evaluaci�n en el periodo '         || pano || '.';     end if;
   end if;
 end if;

EXCEPTION
  WHEN OTHERS THEN
    preturn := 0;
    pmensaje := 'Error inesperado en VAL_FORMULARIO_EXISTE. ' || SQLCODE || ' - ' || SQLERRM || ' ';END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_MAX_NO_APLICA" (pseccion_formulario IN SECCION_FORMULARIO.ID%TYPE,
  preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vsuma NUMBER;
BEGIN
 SELECT NVL(sum(RESPUESTA.valor),0) into vsuma
   from RESPUESTA
   where RESPUESTA.SECCION_FORMULARIO = pseccion_formulario;
 if (vsuma = 0) then --Todas las respuestas fueron 'No aplica'
   preturn := 0;
   pmensaje := 'No se puede almacenar la secci�n. Al menos una respuesta debe ser diferente a "No Aplica". ';
 else
   preturn := 1;
   pmensaje := 'La secci�n no supera el m�ximo n�mero de respuestas "No Aplica". ';
 end if;
EXCEPTION
 WHEN OTHERS THEN 
   preturn := 0;
   pmensaje := 'Error inesperado en VAL_MAX_NO_APLICA. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;



  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_PEROBJ" 
    (i_ano IN BARS_PEROBJ.ano%type,
     i_estado IN BARS_PEROBJ.ESTADO%type, 
     preturn OUT NUMBER, 
     pmensaje OUT VARCHAR2)
IS

 vconteo_filas NUMBER;
 l_estado VARCHAR2(100);
 
BEGIN

    select nombre into l_estado 
    from estado_bars 
    where tipo_estado = 1 and id = i_estado;
    
     SELECT count(*) into vconteo_filas
       from BARS_PEROBJ
       where ano = i_ano and estado = i_estado;
     if vconteo_filas = 0 then 
       preturn := 0;
       pmensaje := 'El periodo de evaluaci�n no est� en el estado: ' || l_estado || '. ';
     else
       preturn := 1;
       pmensaje := 'El periodo de evaluaci�n est� en el estado: ' || l_estado || '. ';
     end if;
    
    EXCEPTION
     WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en VAL_ESTADO_PERIODO. ' || SQLCODE || ' - '
         || SQLERRM);
END;