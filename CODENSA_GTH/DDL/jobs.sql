
  
BEGIN 
dbms_scheduler.create_job('"alertar_pecs_no_terminados"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
  ADMIN_PEC_PCK.GESTION_AUTOMATICA_PEC;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('13-APR-2020 06.00.00,000000000 AM UTC','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'FREQ=DAILY;BYHOUR=6;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Ejecuta el orquestador de tareas autom�ticas para PEC como notificaciones o ejecuci�n autom�tica, dependiendo el n�mero de d�as transcurridos y el estado de cada PEC.'
);
dbms_scheduler.enable('"alertar_pecs_no_terminados"');
COMMIT; 
END; 


  
BEGIN 
dbms_scheduler.create_job('"DISPARADOR_CORREO"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
NOTIFICACION_PCK.DISPARADOR_CORREO;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('22-JUL-2019 05.35.33,644513000 PM AMERICA/BOGOTA','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'FREQ=SECONDLY;BYSECOND=10'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"DISPARADOR_CORREO"');
COMMIT; 
END; 


