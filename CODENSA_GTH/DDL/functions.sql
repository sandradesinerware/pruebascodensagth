
  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."BARS_CALCPERSPOSPRE" ( pposicion IN BARS_POSPRE.id%type, ppersonas IN NUMBER, ptotal_mesa IN NUMBER ) RETURN VARCHAR2
is
retV    VARCHAR2(200);
n1      NUMBER;
rmin    NUMBER;
rmax    NUMBER;
idPos   NUMBER;
porcent NUMBER;
begin
 select RESTRMIN, RESTRMAX into rmin, rmax from BARS_POSPRE where id = pposicion;
 porcent := ROUND((ppersonas/ptotal_mesa)*100,2);

 if( rmax is not null and porcent > rmax ) then
    n1  := (rmax*ptotal_mesa)/100;
    if( ppersonas > n1) then
        retV := 'Remover '||ceil( ppersonas - n1 )||' persona(s).';
    end if;
 elsif( rmin is not null and porcent < rmin )  then
    retV := 'Agregar '||ceil(rmin*ptotal_mesa/100 - ppersonas)||' persona(s).';
 end if;

 return retV;

EXCEPTION
 when NO_DATA_FOUND then
   return '-';
end;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."BARS_CALC_LEVELS" ( pposicion IN BARS_POSICION.id%type, PERSONAS IN NUMBER, total_mesa IN NUMBER ,pmesa NUMBER ) RETURN VARCHAR2
is
retV    VARCHAR2(200);
n1      NUMBER;
rmin    NUMBER;
rmax    NUMBER;
porcent NUMBER;
asup NUMBER;
ainf NUMBER;
begin
        select NVL(ajuste_superior,0),NVL( ajuste_inferior,0) into asup, ainf from BARS_MESAHOM where id=pmesa;
        select RESTRMIN, RESTRMAX into rmin, rmax from BARS_RESTRPOSICION where POSICION = pposicion;


        porcent := PERSONAS*100/total_mesa;

        if( rmax is not null and porcent > rmax+asup) then
              n1  := ((rmax+asup)*total_mesa)/100;
              if( PERSONAS > n1) then
               retV := 'Remover '||ceil( PERSONAS - n1 )||' persona(s).';
              end if;
        elsif( rmin is not null and porcent < rmin-ainf )  then
          retV := 'Agregar '||ceil((rmin-ainf)*total_mesa/100 - PERSONAS)||' persona(s).';
        end if;


   return retV;
exception
 when NO_DATA_FOUND then
   return '-';
end;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."BARS_GET_NIVELES" ( i_porcentaje NUMBER, i_ajuste NUMBER, i_periodo PCT_CONSECUCION_BARS.periodo%type  ) return NUMBER
IS
    iter NUMBER;
    inf  NUMBER;
    sup  NUMBER;
BEGIN

          iter := 1;

          if( i_ajuste < 0) then

              for c in( select PCT_CONSECUCION from PCT_CONSECUCION_BARS where periodo = i_periodo
			    and PCT_CONSECUCION < i_porcentaje order by 1 desc ) loop
               if( iter = abs(i_ajuste) ) then
                     return c.PCT_CONSECUCION;
               end if;
                 iter := iter+1;
              end loop;
          else    
             for c in(  select PCT_CONSECUCION from PCT_CONSECUCION_BARS where periodo = i_periodo
                and PCT_CONSECUCION > i_porcentaje order by 1 asc ) loop
               if( iter = i_ajuste ) then
                     return c.PCT_CONSECUCION;
               end if;
                  iter := iter+1;
             end loop;
          end if;

 return i_porcentaje;
END;

  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."BARS_VALCAMESTOBJ" ( pestado_nuevo IN ESTADO_BARS.id%type ) RETURN BOOLEAN 
IS
knuevo CONSTANT VARCHAR2(30) := 'NUEVO';
ket CONSTANT VARCHAR2(30) := 'EN TRATAMIENTO';
kconval CONSTANT VARCHAR2(30) := 'CONCERTACION VALIDADA';
kconap CONSTANT VARCHAR2(30) := 'CONCERTACION APROBADA';
kperaj CONSTANT VARCHAR2(30) := 'PERIODO AJUSTE';
kresval CONSTANT VARCHAR2(30) := 'RESULTADO VALIDADO';
kfinalizado CONSTANT VARCHAR2(30) := 'FINALIZADO';
kanulado CONSTANT VARCHAR2(30) := 'ANULADO';
vestado_actual VARCHAR2(30);
vestado_nuevo VARCHAR2(30);

BEGIN
SELECT upper(e.nombre) into vestado_actual
 FROM BARS_FORMEVAOBJ f inner join ESTADO_BARS e on (f.estado = e.id)
 WHERE f.id = v('P85_ID');

SELECT upper(nombre) into vestado_nuevo
 FROM ESTADO_BARS
 WHERE id = pestado_nuevo;

 CASE vestado_nuevo

  WHEN knuevo THEN
   if vestado_actual in (kfinalizado) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  WHEN ket THEN
   if vestado_actual in (kfinalizado,kanulado, knuevo) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  WHEN kconval THEN
   if vestado_actual in (kfinalizado,kanulado,knuevo,ket) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  WHEN kconap THEN
   if vestado_actual in (kanulado,knuevo,ket,kconval) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  WHEN kresval THEN
   if vestado_actual in (kanulado,knuevo,ket,kconval,kconap) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  WHEN kfinalizado THEN
   RETURN FALSE;

  WHEN kanulado THEN
   if vestado_actual in (kfinalizado) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  ELSE
   RETURN FALSE;

 END CASE;

EXCEPTION
 WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurri� en BARS_VALCAMESTOBJ. Por favor, inf�rmelo al administrador de la aplicaci�n. ' 
	  || SQLCODE || ' - ' || SQLERRM);

END BARS_VALCAMESTOBJ;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."BARS_VALIDARPORCENT" ( pposicion IN BARS_POSICION.id%type, porcent IN NUMBER, pmesa IN NUMBER  ) RETURN VARCHAR2 
is
retV  VARCHAR2(200);
rmin  NUMBER;
rmax  NUMBER;
asup NUMBER;
ainf NUMBER;
begin

         retV := 'Correcto.';
         select NVL(ajuste_superior,0),NVL( ajuste_inferior,0) into asup, ainf from BARS_MESAHOM where id=pmesa; 
         select RESTRMIN, RESTRMAX into rmin, rmax from BARS_RESTRPOSICION where POSICION = pposicion;

       if( rmin is not null and porcent < rmin-ainf) then
              retV := 'El porcentaje es inferior al minimo.';
       elsif( rmax is not null and porcent > rmax+asup) then
           retV := 'El porcentaje supera el maximo.';
       end if; 

    return retV;

EXCEPTION
  WHEN NO_DATA_FOUND THEN --La posici�n no tiene restricci�n
    return retV;
end;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."BARS_VALITEMSOBJ" (
    pitem         IN            VARCHAR2,
    pformulario   IN            bars_formevaobj.id%TYPE
) RETURN NUMBER IS

    knuevo               CONSTANT VARCHAR2(30) := 'NUEVO';
    ket                  CONSTANT VARCHAR2(30) := 'EN TRATAMIENTO';
    kconval              CONSTANT VARCHAR2(30) := 'CONCERTACION VALIDADA';
    kconap               CONSTANT VARCHAR2(30) := 'CONCERTACION APROBADA';
    kperaj               CONSTANT VARCHAR2(30) := 'PERIODO AJUSTE';
    kresval              CONSTANT VARCHAR2(30) := 'RESULTADO VALIDADO';
    kfinalizado          CONSTANT VARCHAR2(30) := 'FINALIZADO';
    kanulado             CONSTANT VARCHAR2(30) := 'ANULADO';
    kadministradores     CONSTANT VARCHAR2(30) := 'ADMINISTRADORES OPR';
    kmodoconsulta        CONSTANT VARCHAR2(30) := 'CONSULTA';
    kmodoedicion         CONSTANT VARCHAR2(30) := 'EDICION';
    l_modo               CONSTANT VARCHAR2(30) := 'EDICION';
    --v('AI_MODO_FORMULARIO');
    l_app_user           VARCHAR2(100); 
    vtipo_estado         NUMBER(2, 0);
    vestado_formulario   VARCHAR2(30);
    vfecha_pago          DATE;
    vusuario             usuario.numero_identificacion%TYPE;
    l_usuario_id         usuario.id%TYPE;
    vevaluado            bars_formevaobj.evaluado%TYPE;
    vevaluador           bars_formevaobj.evaluador%TYPE;
    vgestor              trabajador.jefe%TYPE;
    flag                 BOOLEAN;
    isadmin              BOOLEAN;
    -- l_id_target_opr      NUMBER;
    -- l_periodo   NUMBER;
    l_peso_requerido_obj_cer NUMBER;
    l_peso_planeado_obj_cer BOOLEAN;
-- Colecci�n para almacenar los grupos del usuario
    TYPE grupo_array IS
        TABLE OF grupo.nombre%TYPE;
    vgrupos              grupo_array;
    i                    NUMBER(2) := 0;
BEGIN

--Identificaci�n usuario conectado a la aplicaci�n
l_app_user := v('APP_USER'); -- Esta es una forma de tomar desde PL el valor de APP_USER para la sesi�n activa

select numero_identificacion into vusuario
  from usuario
  where upper(user_name) = l_app_user;

--Determinar tipo estado OBJETIVOS_BARS
    SELECT
        id
    INTO vtipo_estado
    FROM
        tipo_estado_bars
    WHERE
        upper(nombre) = 'OBJETIVOS_BARS';
--Determinar estado del formulario de OBJETIVOS

    SELECT
        upper(TRIM(e.nombre))
    INTO vestado_formulario
    FROM
        bars_formevaobj   f
        INNER JOIN estado_bars       e ON f.estado = e.id
    WHERE
        f.id = pformulario;
--Determinar evaluado y evaluador del formulario

    SELECT
        evaluado
    INTO vevaluado
    FROM
        bars_formevaobj
    WHERE
        id = pformulario;

    SELECT
        evaluador
    INTO vevaluador
    FROM
        bars_formevaobj
    WHERE
        id = pformulario;

    SELECT
        jefe
    INTO vgestor
    FROM
        trabajador
    WHERE
        numero_identificacion = vevaluado;
-- Determinar fecha de pago bono

    SELECT
        fecha_pago_bono
    INTO vfecha_pago
    FROM
        bars_formevaobj   bf
        JOIN bars_perobj       bp ON ( bp.ano = bf.periodo )
    WHERE
        bf.id = pformulario;

--Determinar id usuario (tener en cuenta que vusuario tiene es el numero de identificacion)
SELECT id INTO l_usuario_id
    FROM usuario
    WHERE upper(user_name) = upper(l_app_user);
--Determinar grupos (i.e. roles) del usuario

    vgrupos := grupo_array();
    FOR rec_grupo IN (
        SELECT
            upper(TRIM(gb.nombre)) nombre
        FROM
            usu_grupo   ubg
            INNER JOIN grupo       gb ON ubg.grupo = gb.id
        WHERE
            ubg.usuario = l_usuario_id
    ) LOOP
        i := i + 1;
        vgrupos.extend(1);
        vgrupos(i) := rec_grupo.nombre;
    END LOOP;

--Validaci�n del display de los items de objetivos de acuerdo al item, al estado del formulario
--y al modo de consulta (EDICION, CONSULTA)

    IF ( l_modo = kmodoconsulta ) THEN
--Est� en modo consulta, por tanto ning�n bot�n de edici�n o cambio de estado debe funcionar
--Solo se verifica si las columnas de objetivos se visualizan dependiendo del estado
        CASE upper(pitem)
            WHEN 'OBJETIVOS.ALCANZADO' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'OBJETIVOS.PCTCONSIND' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'OBJETIVOS.PCTPOND' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            ELSE
                RETURN 0;
        END CASE;
    ELSIF ( l_modo = kmodoedicion ) THEN
        CASE upper(pitem)
            WHEN 'LISTA_OBJETIVOS_SAVE' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;

                IF ( ( ( vestado_formulario IN (
                    knuevo,
                    ket,
                    kperaj
                ) AND ( vevaluado = vusuario ) ) OR ( vestado_formulario IN (
                    knuevo,
                    ket,
                    kperaj,
                    kconval
                ) AND ( vevaluador = vusuario ) ) ) AND ( vfecha_pago >= SYSDATE ) ) OR ( vfecha_pago >= SYSDATE AND isadmin ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;

            WHEN 'LISTA_OBJETIVOS_ADD' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;

                IF ( ( ( vestado_formulario IN (
                    knuevo,
                    ket,
                    kperaj
                ) AND vevaluado = vusuario ) OR ( ( vestado_formulario IN (
                    knuevo,
                    ket,
                    kperaj,
                    kconval
                ) AND ( vevaluador = vusuario ) ) ) AND vfecha_pago >= SYSDATE ) ) OR ( isadmin AND vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;

            WHEN 'LISTA_OBJETIVOS_DELETE' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;

                IF ( ( ( ( vestado_formulario IN (
                    ket,
                    kperaj
                ) AND vevaluado = vusuario ) OR ( vestado_formulario IN (
                    ket,
                    kperaj,
                    kconval
                ) AND vevaluador = vusuario ) ) AND ( vfecha_pago >= SYSDATE ) ) ) OR ( isadmin AND vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;

           WHEN 'LISTA_OBJETIVOS_VALIDAR_CONCERTACION' THEN
                IF ( ( vestado_formulario IN (
                    ket,
                    kperaj
                ) AND vevaluado = vusuario ) OR ( vestado_formulario IN (
                    ket,
                    kperaj
                ) AND vevaluador = vusuario ) ) AND ( vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'LISTA_OBJETIVOS_APROBAR_CONCERTACION' THEN
                IF ( vestado_formulario IN (
                    kconval
                ) AND ( vusuario IN (
                    vevaluador
                ) ) AND ( vfecha_pago >= SYSDATE ) ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'LISTA_OBJETIVOS_MODIFY' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;

                IF isadmin AND vestado_formulario NOT IN (
                               kfinalizado,
                               kanulado
                ) AND ( vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;

            WHEN 'LISTA_OBJETIVOS_SAVE_RESULTADOS' THEN
                IF vestado_formulario IN (
                    kconap
                ) AND ( vevaluado = vevaluador ) AND ( vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'FORTALEZAS_SAVE' THEN
                IF vestado_formulario IN (
                    kresval
                ) AND vevaluador = vusuario AND ( vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'LISTA_OBJETIVOS_VALIDAR_RESULTADO' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;
                IF vestado_formulario IN ( kconap, kresval ) 
                  AND ( vgestor = vusuario OR isadmin )
                  AND ( VALIDACIONES_GUI.val_crud_formevaobj_fch_aprob(pformulario,V('APP_USER')) ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'COMENTARIOS_SAVE_O' THEN
                IF vestado_formulario NOT IN (
                   kfinalizado,
                   kanulado
                ) AND vevaluado = vusuario AND ( vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'COMENTARIOS_SAVE_R' THEN
                RETURN 0; --Se decidi� en reuni�n 20/09 que no se mostrar�a.
            WHEN 'FINALIZAR' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;                        
                
                -- TODO: Este se esta llamando dentro de val_peso_total_objetivos CALFONSO - 4/09/2020 
                -- l_peso_requerido_obj_cer := VALIDACIONES_GUI.calc_peso_requerido_evaluacion( l_periodo, l_id_target_opr, SNW_CONSTANTES.constante_tipo('OPR_CER') );
                -- select periodo into l_periodo from BARS_FORMEVAOBJ where id = pformulario;
                -- select ID_TARGET_OPR into l_id_target_opr from trabajador where NUMERO_IDENTIFICACION = vevaluado;        
                
                l_peso_planeado_obj_cer := validaciones_gui.val_peso_total_objetivos(pformulario,SNW_CONSTANTES.constante_tipo('CERRADO'));--Validar Ponderaci�n Requerida de Objetivos Cerrados
                
                -- FALTA validar objetivos abiertos
                -- l_peso_planeado_obj_cer := validaciones_gui.val_peso_total_objetivos(pformulario,SNW_CONSTANTES.constante_tipo('CERRADO'));--Validar Ponderaci�n Requerida de Objetivos Cerrados
                
                
                IF vestado_formulario IN (kresval) 
                  AND ( l_peso_planeado_obj_cer )
                   AND ( vgestor = vusuario OR isadmin )
                   AND ( VALIDACIONES_GUI.val_crud_formevaobj_fch_aprob(pformulario,V('APP_USER')) ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;

            WHEN 'OBJETIVOS.ALCANZADO' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'OBJETIVOS.PCTCONSIND' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'OBJETIVOS.PCTPOND' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            ELSE
                RETURN 0;
        END CASE;
    ELSE --Caso no contemplado en el modo formulario
        RETURN 0;
    END IF;
    
EXCEPTION
    WHEN OTHERS THEN
    RETURN 0;
        --raise_application_error(-20000, 'Un error inesperado ocurri� en BARS_VALITEMSOBJ. Por favor, inf�rmelo al administrador de la aplicaci�n. '
          --                              || sqlcode
            --                            || ' - '
              --                          || sqlerrm);
END bars_valitemsobj;

  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."BARS_VALITEMSPDI" 
(pitem IN VARCHAR2, pencabezado IN BARS_ENCPDI.ID%TYPE) RETURN NUMBER
IS
kplaneado CONSTANT VARCHAR2(30) := 'PLANEADO';
kconcertado CONSTANT VARCHAR2(30) := 'CONCERTADO';
kfinalizado CONSTANT VARCHAR2(30) := 'FINALIZADO';
kanulado CONSTANT VARCHAR2(30) := 'ANULADO';
knoaprobado CONSTANT VARCHAR2(30) := 'NO APROBADO';
kadministradores CONSTANT VARCHAR2(30) := 'ADMINISTRADORES';
kmodoconsulta CONSTANT VARCHAR2(30) := 'CONSULTA';
kmodoedicion CONSTANT VARCHAR2(30) := 'EDICION';
ktipoestadopdi CONSTANT VARCHAR2(30) := 'PDI';
vtipo_estado NUMBER(2,0);
vestado_formulario VARCHAR2(30);
vevaluado BARS_FORMEVAOBJ.EVALUADO%TYPE;
vgestor TRABAJADOR.JEFE%TYPE;
vusuario USUARIO.NUMERO_IDENTIFICACION%TYPE;
vaccion tipo.nombre%type;
flag BOOLEAN;
formulario_inexistente EXCEPTION;

--Colecci�n para almacenar los grupos del usuario
TYPE grupo_array IS TABLE OF GRUPO.NOMBRE%TYPE;
vgrupos grupo_array;
i number(2) := 0;

BEGIN
--Determinar usuario
SELECT numero_identificacion into vusuario
  FROM USUARIO
  WHERE upper(user_name) = upper(v('APP_USER'));
--Determinar grupos (i.e. roles) del usuario
vgrupos := grupo_array();
FOR rec_grupo IN ( SELECT upper(trim(gb.nombre)) nombre
                   from USU_GRUPO ubg inner join GRUPO gb on ubg.grupo = gb.id
                   where ubg.usuario = vusuario ) 
                   LOOP
  i := i + 1;
  vgrupos.EXTEND(1);
  vgrupos(i) := rec_grupo.nombre;
END LOOP;

if pencabezado IS NULL then --El formulario se va a crear
 raise formulario_inexistente;
end if;

--Determinar tipo estado PDI
SELECT id into vtipo_estado
  FROM TIPO_ESTADO_BARS
  WHERE upper(nombre) = ktipoestadopdi;
--Determinar estado del formulario PDI
SELECT upper(trim(E.nombre)) into vestado_formulario
  FROM BARS_PDI F inner join ESTADO_BARS E on F.ID_estado = E.id
  WHERE F.id = NVL(pencabezado,0);
--Determinar evaluado y gestor
SELECT evaluado into vevaluado
  FROM BARS_ENCPDI
  WHERE id = NVL(pencabezado,0);
SELECT jefe into vgestor
  FROM TRABAJADOR
  WHERE numero_identificacion = NVL(vevaluado,0);
--Determinar acci�n de desarrollo
SELECT upper(nombre) into vaccion
  FROM tipo 
  WHERE id = ( SELECT ID_accion FROM BARS_PDI WHERE id = pencabezado );

--Validaci�n del display de los items de objetivos de acuerdo al item y al estado del formulario
CASE upper(pitem)
WHEN 'PDI.ESTADO' THEN --No para visualizaci�n sino para read only
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF (flag) then
    RETURN 0;
  else
    RETURN 1;
  end if;
WHEN 'PDI.ESTFINFORMACION' THEN --Este campo es solo para las acciones de formaci�n
  IF ( vaccion LIKE 'FORMACI_N' AND vestado_formulario in (kconcertado,kfinalizado) ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'CREATE' THEN --Aca no deber�a llegar porque el bot�n est� condicionado para que solo aparezca si :P78_ID es NULL, y en ese caso
                   --se lanzar�a excepcion formulario_inexistente
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( pencabezado is null and (flag OR vusuario = v('P78_EVALUADO') ) ) then --Solo administradores y el propio evaluado pueden crear
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'SAVE' THEN
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( vestado_formulario in (kplaneado) and vevaluado = vusuario )
    OR ( flag ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'DELETE' THEN
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( vestado_formulario in (kplaneado) and vevaluado = vusuario ) 
    OR ( flag and vestado_formulario in (kplaneado) ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'CONCERTAR' THEN
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( vestado_formulario in (kplaneado) and ( vgestor = vusuario OR flag ) ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'REPROBAR_CONCERTACION' THEN
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( vestado_formulario in (kplaneado) and ( vgestor = vusuario OR flag ) ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'FINALIZAR' THEN
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( vestado_formulario in (kconcertado) and ( flag ) ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
ELSE
  RETURN 0;
END CASE;

EXCEPTION
  WHEN FORMULARIO_INEXISTENTE THEN
    CASE upper(pitem)
      WHEN 'CREATE' THEN 
        FOR i IN 1..vgrupos.COUNT LOOP
            if vgrupos(i) = kadministradores then
                flag := TRUE;
            end if;
        end loop;
        IF ( pencabezado is null and (flag OR vusuario = v('P78_EVALUADO') ) ) then
            RETURN 1;
        else
            RETURN 0;
        end if;
      WHEN 'NUEVO_PDI' THEN --Bot�n de la p�gina 77 donde nace el proceso de creaci�n 
        FOR i IN 1..vgrupos.COUNT LOOP
            if vgrupos(i) = kadministradores then
                flag := TRUE;
            end if;
        end loop;
        IF ( pencabezado is null and (flag OR vusuario = v('P77_EVALUADO') ) ) then
            RETURN 1;
        else
            RETURN 0;
        end if;
      WHEN 'PDI.ESTADO' /*No display sino readonly*/ THEN RETURN 1; --El estado inicial del formulario se asigna autom�ticamente
      WHEN 'PDI.ESTFINFORMACION' THEN RETURN 0; --A un formulario inexistente no se le muestra este campo
    ELSE RETURN 0;
    END CASE;

  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurri� en BARS_VALITEMSPDI. Por favor, inf�rmelo al administrador de la aplicaci�n. ' 
      || SQLCODE || ' - ' || SQLERRM);

END BARS_VALITEMSPDI;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."BARS_VALPORCENTPOSPRE" ( pposicion IN BARS_POSPRE.id%type, pporcentaje IN NUMBER ) RETURN VARCHAR2
is
retV  VARCHAR2(200);
rmin  NUMBER;
rmax  NUMBER;
idPos NUMBER;
begin
  retV := 'Correcto.';

  select RESTRMIN, RESTRMAX into rmin, rmax from BARS_POSPRE where id = pposicion;

  if( rmin is not null and pporcentaje < rmin ) then
              retV := 'El porcentaje es inferior al minimo.';
  elsif( rmax is not null and pporcentaje > rmax ) then
           retV := 'El porcentaje supera el maximo.';
  end if; 

  return retV;
end;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."DETERMINAR_PAG_FORM_PLANTILLA" (pformulario IN FORMULARIO_360.ID%type,
ptipo IN VARCHAR2)
RETURN NUMBER
IS
vplantilla PLANTILLA_FORM_360.ID%TYPE;
ktipo_seccion CONSTANT VARCHAR2(10) := 'SECCION';
ktipo_resultado CONSTANT VARCHAR2(10) := 'RESULTADO';
BEGIN
--Determinaci�n de la plantilla
SELECT PLANTILLA_FORM_360.ID into vplantilla
from FORMULARIO_360 inner join PLANTILLA_FORM_360 on (FORMULARIO_360.plantilla =
PLANTILLA_FORM_360.id)
where FORMULARIO_360.id = pformulario;
if upper(ptipo) = ktipo_seccion then --Se busca la primera p�gina de la secci�n
RETURN 42;
elsif upper(ptipo) = ktipo_resultado then --Se busca la p�gina del resultado
RETURN 51;
end if;
EXCEPTION
WHEN OTHERS THEN
RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_PAG_FORM_PLANTILLA.' || SQLCODE || ' - ' || SQLERRM);
END DETERMINAR_PAG_FORM_PLANTILLA;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."DETERMINAR_PCT_CONSECUCION" (pformulario IN FORMULARIO_BARS.ID%TYPE, 
  ptipo IN SECCION_FORMULARIO.TIPO%TYPE)
RETURN PCT_CONSECUCION_BARS.PCT_CONSECUCION%TYPE
IS
  vpct_consecucion PCT_CONSECUCION_BARS.PCT_CONSECUCION%TYPE;
  vresultado_encuesta NUMBER;
  vconteo_filas NUMBER;
  vperiodo PERIODO_BARS.ANO%TYPE;
BEGIN
  --Determinar promedio encuesta y normalizar
  vresultado_encuesta := NORMALIZAR_BPR(DETERMINAR_PROMEDIO_ENCUESTA(pformulario, ptipo));


  --Persiste la informacion en FORMULARIO_BARS
  update formulario_bars set media_normalizada = vresultado_encuesta where id = pformulario;

  
  --Determinar pct_consecucion
  vpct_consecucion := OBTENER_PORCENTAJE_CONSECUCION(vresultado_encuesta);

  
  /* ! ! ! Se cambia por:  https://sinerware.atlassian.net/browse/GTH2019-81 ! ! !  -- Estado anterior:
   --Determinar periodo
  select periodo into vperiodo from FORMULARIO_BARS where id = pformulario;
  
  --Determinar pct_consecucion
  SELECT count(*) into vconteo_filas
    from PCT_CONSECUCION_BARS
    where tipo_id = 61 and periodo = vperiodo AND vresultado_encuesta BETWEEN media_comp_inf AND media_comp_sup;
    
  if vconteo_filas = 1 then --Se determin� el porcentaje de consecuci�n  
    SELECT pct_consecucion into vpct_consecucion
      from PCT_CONSECUCION_BARS
      where tipo_id = 61 and  periodo = vperiodo AND vresultado_encuesta BETWEEN media_comp_inf AND media_comp_sup;
  else --No se determin� un porcentaje de consecuci�n
    vpct_consecucion := NULL;
  end if;
    */
  
  
  RETURN vpct_consecucion;
  
EXCEPTION
WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en DETERMINAR_PCT_CONSECUCION. ' || SQLCODE || ' - ' 
     || SQLERRM || ' ');
END;

  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."DETERMINAR_PROMEDIO_ENCUESTA" (pformulario IN FORMULARIO_BARS.ID%TYPE,
  ptipo IN SECCION_FORMULARIO.TIPO%TYPE)
RETURN NUMBER
IS
 vpromedio NUMBER;
BEGIN
 /*SELECT NVL( AVG(resultado),0 ) into vpromedio
   from SECCION_FORMULARIO
   where formulario = pformulario and tipo = ptipo;*/
 --Cambio en procedimiento de c�lculo de promedio (no es de las secciones sino de las respuestas)
 SELECT NVL( AVG(valor),0 ) into vpromedio
   from RESPUESTA
   where seccion_formulario IN (select id from seccion_formulario where formulario = pformulario)
   and valor != 0;
   --Persiste la informacion en FORMULARIO_BARS
   update formulario_bars set media_comportamientos= round(vpromedio,4) where id=pformulario;

 RETURN round(vpromedio,4);
EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en DETERMINAR_PROMEDIO_ENCUESTA. ' || SQLCODE || ' - ' 
     || SQLERRM || ' ');
END;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."DETERMINAR_PROMEDIO_SECCION" 
    (pseccion_formulario IN SECCION_FORMULARIO.ID%TYPE)
RETURN NUMBER
IS
 vpromedio NUMBER;
BEGIN
 SELECT NVL( AVG(valor),0 ) into vpromedio
   from RESPUESTA
   where seccion_formulario = pseccion_formulario and valor != 0;
 RETURN vpromedio;
EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en DETERMINAR_PROMEDIO_SECCION. ' || SQLCODE || ' - '
     || SQLERRM || ' ');
END;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."LIMPIEZA_VARCHAR" 
(
  I_CADENA IN VARCHAR2 
) RETURN VARCHAR2 AS 
BEGIN
  RETURN trim(replace(replace(replace(I_CADENA,chr(13),''),chr(10),''),chr(9),''));
END LIMPIEZA_VARCHAR;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."MOSTRAR_CURSOS" 
(
  i_id_grupo in NUMBER,
  i_rol in  VARCHAR2
) return number 
as
l_visible PDI_GRUPO.VISIBLE%TYPE;
begin

select visible into l_visible from pdi_grupo where id = i_id_grupo;

if(i_rol = 'admin') then 
    return 1;
elsif (l_visible = 'S') then 
    return 1;
else 
    return 0;
end if;
end MOSTRAR_CURSOS;

  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."NORMALIZAR_BPR" 
(X in NUMBER)
return NUMBER
is
o_result NUMBER;
begin
o_result:=0;
if (X between 1 and 3) then o_result:= (4*X)-4;
elsif (X between 3 and 5) then o_result:= X+5;
end if;
return o_result;
end;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."OBTENER_PORCENTAJE_CONSECUCION" ( i_resultado_obtenido in number) return number is 
    l_porcentaje_consecucion NUMBER;
begin

    l_porcentaje_consecucion := 0;

    if (i_resultado_obtenido >= 6) then    
        l_porcentaje_consecucion := (i_resultado_obtenido * 10) + 20;
    end if;

    return ROUND(l_porcentaje_consecucion, 1);

end OBTENER_PORCENTAJE_CONSECUCION;

  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."PONDERACION_TIPO_EVALUACION" 
(i_trabajador in NUMBER,
i_tipo_evaluacion in NUMBER,
i_periodo in NUMBER)
return NUMBER
is
o_ponderacion NUMBER;
begin
select PORCENTAJE_TOTAL into o_ponderacion 
    from restriccion_target right join trabajador on (restriccion_target.id_target=trabajador.id_target_opr) 
    where trabajador.numero_identificacion=i_trabajador and id_tipo_evaluacion=i_tipo_evaluacion and periodo=i_periodo;
return o_ponderacion;
exception
    when no_data_found then
    return 0;
end;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."PORCENTAJE_EVALUACION" 
(I_EVALUACION in NUMBER, I_CONDICION NUMBER)
return NUMBER
is
BEGIN
DECLARE
    L_TOTAL NUMBER;
    L_LISTAS NUMBER;
    L_PORCENTAJE NUMBER; 
    V_ESTADO NUMBER;
BEGIN
SELECT COUNT(*) INTO L_LISTAS FROM RESPUESTA_EVALUACION WHERE EVALUACION =I_EVALUACION;
SELECT COUNT(*) INTO L_TOTAL FROM PREGUNTA WHERE ESTADO=1;
L_PORCENTAJE  :=ROUND(((L_LISTAS/L_TOTAL)*100),0);
CASE 
    WHEN L_PORCENTAJE = 0 THEN
         V_ESTADO := 35;
    WHEN L_PORCENTAJE > 0 AND L_PORCENTAJE < 100 THEN
         V_ESTADO := 36;
    WHEN L_PORCENTAJE = 100 THEN
         V_ESTADO := 37;
END CASE;
IF I_CONDICION = 1 THEN
    UPDATE EVALUACION SET ESTADO = V_ESTADO WHERE ID = I_EVALUACION;
     UPDATE EVALUACION SET PORCENTAJE= L_PORCENTAJE WHERE ID = I_EVALUACION;   
    RETURN L_PORCENTAJE;
END IF;
RETURN L_PORCENTAJE;
END;
EXCEPTION WHEN OTHERS THEN
    dbms_output.put_line(SQLERRM);
    RETURN -1;   
END;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."PRIORIDAD" 
(i_evaluacion in NUMBER)
return NUMBER
is
begin
DECLARE
L_ESTADO NUMBER; 
L_APLICA NUMBER;
BEGIN
SELECT ESTADO, APLICA INTO L_ESTADO, L_APLICA FROM EVALUACION WHERE ID = I_EVALUACION;
IF (L_ESTADO = 37 OR L_APLICA = 0) THEN
    RETURN 100;
END IF;
IF (L_ESTADO = 35 ) THEN
    RETURN ROUND(DBMS_RANDOM.value(low => 1, high => 49),0);
END IF;
IF (L_ESTADO = 36) THEN
    RETURN ROUND(DBMS_RANDOM.value(low => 50, high => 99),0);
END IF;
END;
end;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."SNW_AUTHENTICATION" (
    p_username IN VARCHAR2,
    p_password IN VARCHAR2 )
  RETURN BOOLEAN
IS
  l_user usuario.user_name%type := upper(p_username);
  l_pwd usuario.password%type;
  l_id usuario.id%type;
BEGIN
  SELECT id ,
    password
  INTO l_id,
    l_pwd
  FROM usuario
  WHERE user_name = l_user;
  RETURN l_pwd   = rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(p_password||l_id||l_user), 2 ));
EXCEPTION
WHEN NO_DATA_FOUND THEN
  RETURN false;
END;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."SNW_RANDOM_PASSWORD" return varchar2 as 
l_random_password varchar2(30);
begin
select dbms_random.string('x',6) || lpad(to_char(trunc(dbms_random.value(1,99))),2,'0') into l_random_password from dual;
return l_random_password;
end;



  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."TEST_TRABAJADOR" (ID IN NUMBER )
  RETURN NUMBER
IS
  
BEGIN
    IF ID > 26 THEN
        RETURN 1;
    END IF;

    RETURN 0;
END;





  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."VAL_EXISTE_ENCPDI" 
(
  i_periodo in BARS_ENCPDI.periodo%type,
  i_evaluado in  BARS_ENCPDI.evaluado%type
) return number as
l_conteo NUMBER;
begin

select count(*) into l_conteo from bars_encpdi where periodo=i_periodo and evaluado=i_evaluado;

if (l_conteo=1) then return 1;
elsif (l_conteo>1) then 
raise_application_error(-20000,'Existe m�s de un regitro de PDI.');
else return 0;
end if;
  return null;
end val_existe_encpdi;